# LST

## Installation
* `git clone`
* Lancer `make init`
  * définir les ports apache et adminer ainsi que le mot de passe root mysql, un nom de db, un utilisateur et mot de passe mysql
  * reporter ces valeurs dans le `.env` de Symfony. L'application devrait être dispo sur `localhost:XXX` où `XXX` est le port défini pour apache.


## Données / import
* Données dans `application/data`

* Import via les commandes:
  * `php bin/console app:import-csv` (option `emptydb` pour vider la base)
  * `php bin/console app:import-article`
  * `php bin/console app:import-csv-trad` (langue optionnelle)
  * `php bin/console app:import-fonction`
  * `php bin/console app:import-fonction-collocations`
  * `php bin/console app:import-fonction-collocations`
  * `php bin/console app:import-fonction-adverbs`
  * `php bin/console app:import-routine`

* suppression:
  * `php bin/console app:delete-trad zh`
  * `php bin/console app:delete-fonctions`
  * `php bin/console app:delete-routines`

### Import JSON 
* Suppression de toutes les données (sauf users et articles) et réimport via la commande : `php bin/console app:import-json`
* Le fichier d'import json doit être placé dans `/application/data/json/` et nommé `lstdata.json`
* Les données peuvent être partielles : [cf. documentation exportJSON ](#Export-JSON-XML)


## Cache
Il y a du cache. Relancer les containers dockers suffit à l'invalider. (voir `application/src/Manager/CacheManager.php`)

```
docker-compose stop
docker-compose up -d
```

## Administration
* Commande pour donner les droits à un utilisateurs : `php bin/console app:toggleadmin <mail>`


## Divers
* Dans doctrine/adminer l'objet lexical_entry a une propriété acceptionUniq du type
  Si LEMMA = introduction_2 et N = N
  Alors acceptionUniq = introduction_N_2
  Si LEMMA = avérer_REFL_1 et N = V
  Alors acceptionUniq = avérer_V_REFL_1

* Routine perl (voir `xml-perl-transform/`) pour reconstruire les id (lemma), ajouter attribut 'acceptionUniq' et supprimer les attributs DEF CS SCS -
```
  <text>
         <front>
             <head><LST CAT="N" TYPE="MONO" LEMMA="approche"
                     DEF="Voie souvent hésitante par laquelle on cherche à cerner un problème complexe\."
                     CS="objet_scientifique" SCS="méthode">Approche</LST> linguistique pour
                     <lb/>l'<LST CAT="N" TYPE="MONO" LEMMA="analyse"
                     DEF="Décomposition en éléments\." CS="processus_cognitif" SCS="examen"
                     TERME="OUI">analyse</LST> syntaxique de corpus</head>
             <docAuthor>
```
## Export JSON-XML

- Formulaire d'export aux formats XML ou JSON disponible pour les administrateurs (`lst.demarre-shs.fr/export/options`) 
- Export personnalisable (choix de 6 groupes d'entités + exemples, sources, et langues des traductions)
- [Documentation de l'export au format XML](export-xml-readme.md) : en partie mappé sur LMF.
- [Documentation de l'export au format JSON](export-json-readme.md) : calque de la base de données, avec les relations 1-1 / 1-* factorisées dans des objets enfants de l'objet parent.
- L'export JSON est réimportable [cf Import JSON](#Import-JSON)

## Licence
GNU GENERAL PUBLIC LICENSE V3 (voir le [Guide rapide de la GPLv3](https://www.gnu.org/licenses/quick-guide-gplv3.html))

## Auteurs du code
* Arnaud Bey
* Sylvain Hatier
