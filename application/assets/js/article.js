import {chartHandler} from "./testCharts.js"
import {graphColloc} from "./cytoscape-entry.js"
let entries = document.querySelectorAll("LST")

entries.forEach(function(entry) {
  entry.addEventListener("click", function() {
    let acceptionUniq = entry.getAttribute("acceptionUniq");
    entryToDetails(acceptionUniq);
  });
});

let catBtns = document.querySelectorAll("span[data-cat]")
catBtns.forEach(function(catBtn) {
  catBtn.addEventListener("click", function() {
    let cat = catBtn.getAttribute("data-cat");
    toggleCat(cat);
  });
});

function toggleCat(cat) {
  let entries = document.querySelectorAll('LST[CAT="' + cat + '"]')
  entries.forEach(function(entry) {
    entry.classList.toggle("showLST");
  });
}

function entryToDetails(acceptionUniq) {
  let url = Routing.generate('lexicalentry_details', {});
  $('#lexicalEntry-modal').modal('show')
  document.getElementById("lexicalEntry-modal-body").innerHTML = "<i class='fas fa-spinner fa-spin'></i> chargement en cours..."

  $.ajax({
    method: 'POST',
    url: url,
    data: {
      'acceptionUniq': acceptionUniq
    }
  }).done((results) => {
    populateModal(results).then(() => {
      chartHandler.init()
      graphColloc.init()
      $('[data-toggle="popover"]').popover()
    })
  })
}

function populateModal(results){
  return new Promise((resolve, reject) => {
      document.getElementById("lexicalEntry-modal-body").innerHTML = results.html
      setTimeout(function() {
        resolve('foo')
      }, 10)
    })
}
