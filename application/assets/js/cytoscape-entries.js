import fcose from 'cytoscape-fcose';
cytoscape.use(fcose);
import cola from 'cytoscape-cola';
cytoscape.use( cola );

import cise from 'cytoscape-cise';
cytoscape.use(cise);
// import layoutUtilities from 'cytoscape-layout-utilities'
// cytoscape.use( layoutUtilities )
// import tippy from 'tippy.js';
// cytoscape.use( tippy );

import coseBilkent from 'cytoscape-cose-bilkent';
cytoscape.use(coseBilkent);

import cytoscape from "cytoscape";

export let graphColloc = {
  init: function() {
      var containerCy = document.getElementById('cy')
      if (containerCy) {
        let height = window.innerHeight - (180)
        containerCy.style.height = height + "px"
        let startingElements
        let startingNodes
        var dataJSON = JSON.parse(containerCy.dataset.datas);
        var layout = {
          spacingFactor: 3,
          animate: 'end',
          animationEasing: 'ease-out',
          edgeElasticity: 1,
          fit: true,
          idealEdgeLength: 30,
          name: 'cose-bilkent',
          nodeOverlap: 10,
          tile: false,
          nestingFactor: 1.2,
          nodeRepulsion: 10000,
          nodeSeparation: 10000,
          numIter: 500,
          packComponents: true,
          quality: "default",
          randomize: true,
          tilingPaddingHorizontal: 100,
          tilingPaddingVertical: 100,
          uniformNodeDimensions: true,
          minNodeSpacing: 50,
          avoidOverlap: true,
          avoidOverlapPadding: 50,
          nodeDimensionsIncludeLabels: true,
          gravity: 1,
          gravityRangeCompound: 1,
          // Gravity force (constant) for compounds
          gravityCompound: 1.0,
          // Gravity range (constant)
          gravityRange: 1,
          // Initial cooling factor for incremental layout
          initialEnergyOnIncremental: 0.5

        }


        var cy = cytoscape({
            container: containerCy, // container to render in
            ready: function() {
              startingElements = this.elements();
              startingNodes = this.nodes();
              update(this);
            },
            elements: dataJSON,
            layout: layout,
            style: [ // the stylesheet for the graph
              {
                selector: 'node',
                style: {
                  'background-color': 'data(color)',
                  'label': 'data(text)',
                  'width': 'data(weight)',
                  'height': 'data(weight)'
                }
              },
              {
                selector: '.breadCrumb',
                style: {
                  "background-color": "yellow"
                }
              },
              {
                selector: 'edge',
                style: {
                  'width': 2,
                  'line-color': 'grey',
                  'curve-style': 'bezier',
                  'target-arrow-shape': 'triangle',
                  'target-arrow-color': 'grey',
                }
              }
            ],
          }
        );

        $('.criteria').on('change', function(evt) {
          update(cy);
        });

        cy.on('click', 'node', function(evt){
                var node = evt.target;
                var nodeId = node.data().id;
                var nodeText = node.data('text');
                var containerEdge = document.getElementById('current-edge');
                if (nodeId.match(/^\d+$/g) != null) {
                  var url = Routing.generate('lexicalentry_display', {
                    'id': nodeId
                  });
                  containerEdge.innerHTML = "<a class='text-white' href='"+ url +"'>" + nodeText + "</a>";
                }
                else {
                  containerEdge.innerHTML = nodeText + " (mot non présent dans la base) ";
                }
        });

        cy.on('click', 'edge', function(evt){
          var edge = evt.target;
          var containerEdge = document.getElementById('current-edge');
          var edgeId = edge.data().id;
          var edgeCaption = edge.data('caption');
          var url = Routing.generate('collocation_display', {
            'id': edgeId
          });
          containerEdge.innerHTML = "<a class='text-white' href='"+ url +"'>"  + edgeCaption + "</a>";
        });

        $(document).ready(function(){
          $("#all-structureColloc").click(function(){
            $.each($("input[name='structureColloc']"), function(){
                $(this).prop("checked", true);
            });
            update(cy);
          });
          $("#none-structureColloc").click(function(){
            $.each($("input[name='structureColloc']"), function(){
                $(this).prop("checked", false);
            });
            update(cy);
          });
          $("#all-fl").click(function(){
            $.each($("input[name='fonctionLexicale']"), function(){
                $(this).prop("checked", true);
            });
            update(cy);
          });
          $("#none-fl").click(function(){
            $.each($("input[name='fonctionLexicale']"), function(){
                $(this).prop("checked", false);
            });
            update(cy);
          });
        });


        function update(cy) {
          cy.nodes().remove();
          cy.add(startingElements);

          let minDisc = parseInt($('#disc').val(), 10);
          let minOcc = parseInt($('#occ').val(), 10);
          let minLog = parseInt($('#loglike').val(), 10);
          let structureChecked = new Array();
          $.each($("input[name='structureColloc']:checked"), function(){
              structureChecked.push($(this).val());
          });
          let fonctionChecked = new Array();
          $.each($("input[name='fonctionLexicale']:checked"), function(){
              fonctionChecked.push($(this).val());
          });
          let edgesToHide = cy.edges().filter(function(edge) {
            let occ = edge.data('nbOccurrences');
            let disc = edge.data('nbDisciplines');
            let loglike = edge.data('logLike');
            let structureColloc = edge.data('structureColloc');
            let fonctionLexicale = edge.data('fonctionLexicale');
            return ( occ < minOcc || disc < minDisc || loglike < minLog || (structureChecked.indexOf(structureColloc) == -1)|| (fonctionChecked.indexOf(fonctionLexicale) == -1));
          });
          cy.remove(edgesToHide);

          let orphanNodes = cy.nodes().filter(function(node) {
            let isOrphan = (node.connectedEdges().size() === 0) ? true : false;
            return isOrphan;
          });
          cy.remove(orphanNodes);

          cy.layout(layout).run();
          cy.center();
          cy.fit();
      }
    }
  }
}
graphColloc.init();
