import cytoscape from "cytoscape"
import fcose from 'cytoscape-fcose'
import layoutUtilities from 'cytoscape-layout-utilities'
import tippy from 'tippy.js';
import coseBilkent from 'cytoscape-cose-bilkent';
cytoscape.use(coseBilkent)
import cola from 'cytoscape-cola';
cytoscape.use( cola );
cytoscape.use(fcose)
cytoscape.use( layoutUtilities )
// cytoscape.use( tippy );

export let graphColloc = {
  init: function() {
      var containerCy = document.getElementById('cy')
      if (containerCy) {
        let height = window.innerHeight - (18)
        containerCy.style.height = height + "px"

        var dataJSON = JSON.parse(containerCy.dataset.datas);

        var layoutBilkent = {
          spacingFactor: 3,
          animate: 'end',
          animationEasing: 'ease-out',
          edgeElasticity: 1,
          fit: true,
          idealEdgeLength: 30,
          name: 'cola',
          nodeOverlap: 10,
          tile: false,
          nestingFactor: 1.2,
          nodeRepulsion: 10000,
          nodeSeparation: 10000,
          numIter: 500,
          packComponents: true,
          quality: "default",
          randomize: true,
          tilingPaddingHorizontal: 100,
          tilingPaddingVertical: 100,
          uniformNodeDimensions: true,
          minNodeSpacing: 50,
          avoidOverlap: true,
          avoidOverlapPadding: 50,
          nodeDimensionsIncludeLabels: true,
          gravity: 1,
          gravityRangeCompound: 1,
          // Gravity force (constant) for compounds
          gravityCompound: 1.0,
          // Gravity range (constant)
          gravityRange: 1,
          // Initial cooling factor for incremental layout
          initialEnergyOnIncremental: 0.5

        }

        var layout = {
          name: 'fcose',
          animate: true,
          animationEasing: 'ease-out',
          edgeElasticity: 1,
          fit: true,
          idealEdgeLength: 80,
          initialEnergyOnIncremental: 1,
          nestingFactor: 1,
          nodeDimensionsIncludeLabels: false,
          nodeRepulsion: 299999,
          nodeSeparation: 29999,
          numIter: 4500,
          packComponents: true,
          quality: "proof",
          randomize: true,
          tile: true,
          tilingPaddingHorizontal: 100,
          tilingPaddingVertical: 100,
          uniformNodeDimensions: true,
        };

        var layout2 = {
          name: 'fcose',
          fit: false,
        };

        var cy = cytoscape({
            container: containerCy, // container to render in
            elements: dataJSON,
            layout: layout,
            style: [ // the stylesheet for the graph
              {
                selector: 'node',
                style: {
                  'background-color': 'data(color)',
                  'label': 'data(text)',
                  'width': 'data(weight)',
                  'height': 'data(weight)'
                }
              },
              {
                selector: '.breadCrumb',
                style: {
                  "background-color": "yellow"
                }
              },
              {
                selector: 'edge',
                style: {
                  'width': 2,
                  'line-color': 'grey',
                  'curve-style': 'bezier',
                  'target-arrow-shape': 'triangle',
                  'target-arrow-color': 'grey',
                }
              }
            ],
          }
        );

        var nodesExpanded = new Array;
        cy.on('click', 'node', function(evt){
                var node = evt.target;
                var nodeId = node.data().id;
                if (nodeId.match(/^\d+$/g) != null) {

                  var url = Routing.generate('lexicalentry_colloc_nodes', {
                    'id': nodeId
                  });
                  $.ajax({
                    url: url,
                    type: 'GET',
                    success: function (data) {
                      let currentElement = cy.$( "#"+nodeId );
                      let index = nodesExpanded.indexOf(nodeId);
                      currentElement.toggleClass("breadCrumb");
                      if (index == -1) {
                        nodesExpanded.push(nodeId);
                        var data = JSON.parse(data)
                        let elements = cy.add(data);
                        let collection = cy.elements();
                        collection.layout(layout2).run();
                        cy.center(elements)
                      }
                      else {
                        let heirs = new Array();
                        let nodesIdToRemove = getHeirs(cy, nodeId, heirs)
                        let nodesToRemove = cy.nodes().filter(function( node ){
                          return nodesIdToRemove.indexOf(node.data().id) != -1
                        });
                        nodesToRemove
                        cy.remove(nodesToRemove);
                        nodesExpanded.splice(index, 1);
                      }
                  }

                })
              }

            //ajout test si id est numérique
            var nodeText = node.data('text');
            var containerEdge = document.getElementById('current-edge');
            if (nodeId.match(/^\d+$/g) != null) {
              var url = Routing.generate('lexicalentry_display', {
                'id': nodeId
              });
              containerEdge.innerHTML = "<a href='"+ url +"'>" + nodeText + "</a>";
            }
            else {
              containerEdge.innerHTML = nodeText + " (mot non présent dans la base) ";
            }
        });

        cy.on('click', 'edge', function(evt){
          var edge = evt.target;
          var containerEdge = document.getElementById('current-edge');
          var edgeId = edge.data().id;
          var edgeCaption = edge.data('caption');
          var url = Routing.generate('collocation_display', {
            'id': edgeId
          });
          containerEdge.innerHTML = "<a href='"+ url +"'>" + edgeCaption + "</a>";
        });


    }
  }
}

function getHeirs(cy, id, heirs){
    let nodesToRemove = cy.nodes().filter(function( node ){
      return node.data('parentId') == id;
    });

    nodesToRemove.forEach(function(item){
      let id = item.data().id
      if (heirs.indexOf(id) == -1) {
        heirs.push(id)
        getHeirs(cy, id, heirs)
      }
    });

    return heirs;
}
