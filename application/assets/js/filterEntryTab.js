let input = document.getElementById("rechercheContrainte")

if (input) {
  input.addEventListener("keyup", function() {
    filter(this.value)
  }, false)
}

function filter(value) {
  const entries = document.getElementsByClassName("entry");
  const entriesArray = Array.from(entries)

  entriesArray.forEach(function(entry) {
    const el = entry.querySelector('.valuelemma')
    let cellContent = el.innerHTML
    cellContent += ("synonyms" in el.dataset) ? el.dataset.synonyms : ""
    entry.style.display = (cellContent.indexOf(value) > -1) ? "block" : "none"
  });
}
