document.addEventListener("DOMContentLoaded", function(event) {
  let btns = Array.from(document.getElementsByClassName("fonction-details"));
  if (btns.length > 0) {
    btns.forEach(function(btn) {
      btn.addEventListener("click", function() {
        displayFonctionElements(this)
      }, false);
    });
  } else {
    initEvent();
  }
  
});

function initEvent(){
  $('.popover-dismiss').popover({
    trigger: 'focus'
  })

  let entriesPopover = Array.from(document.getElementsByClassName("entry-popover"));
  entriesPopover.forEach(function(entryPopover) {
    entryPopover.addEventListener("click", function() {
      const id = entryPopover.dataset.entry
      let url = Routing.generate('lexicalentry_exemples', {"id":id});

      $.ajax({
        method: 'POST',
        url: url,
      }).done((results) => {
        entryPopover.dataset.content = results.html
        $(entryPopover).popover('show')
      })
    }, false);
  });

  let collocationsPopover = Array.from(document.getElementsByClassName("collocation-popover"));
  collocationsPopover.forEach(function(collocationPopover) {
    collocationPopover.addEventListener("click", function() {
      const id = collocationPopover.dataset.collocation
      console.log(id)
      let url = Routing.generate('collocation_exemples', {"id":id});

      $.ajax({
        method: 'POST',
        url: url,
      }).done((results) => {
        collocationPopover.dataset.content = results.html
        $(collocationPopover).popover('show')
      })
    }, false);
  });

  let routinePopovers = Array.from(document.getElementsByClassName("routine-popover"));
  routinePopovers.forEach(function(routinePopover) {
    routinePopover.addEventListener("click", function() {
      const id = routinePopover.dataset.routine
      let url = Routing.generate('routine_exemples', {"id":id});

      $.ajax({
        method: 'POST',
        url: url,
      }).done((results) => {
        routinePopover.dataset.content = results.html
        $(routinePopover).popover('show')
      })
    }, false);
  });

}

function displayFonctionElements(btn) {
  let id = btn.dataset.fonction
  let url = Routing.generate('fonction_elements', {"id":id});

  $.ajax({
    method: 'POST',
    url: url,
  }).done((results) => {
    document.getElementById("elementsModalBody").innerHTML = results.html
    initEvent();
    $('#elementsModal').modal('show')
  })
}
