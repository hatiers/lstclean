import {chartHandler} from "./testCharts.js"
import {graphColloc} from "./cytoscape-entry.js"

document.addEventListener('DOMContentLoaded', function() {
  chartHandler.init();
  graphColloc.init();
});
