import bsCustomFileInput from 'bs-custom-file-input'

$(function () {
  $('[data-toggle="popover"]').popover({ delay: { hide: 200 } })
  $('[data-toggle="tooltip"]').tooltip({ delay: { hide: 200 } })
  bsCustomFileInput.init()
})
