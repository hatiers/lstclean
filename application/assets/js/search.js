$(function () {
  const el = document.querySelector('#search_everywhere');
  if(el){
    el.addEventListener('keyup', (event) => {
      doSearch(event.target.value);
    });
  }
})

var delayTimer;

function doSearch(str) {
  if (str.trim().length) {
    clearTimeout(delayTimer);
    delayTimer = setTimeout(function() {
      let url = Routing.generate('search_everywhere');

      $.ajax({
        method: 'POST',
        url: url,
        data: {
          'str': str
        }
      }).done((results) => {
        if (document.querySelector('#search_everywhere').value.trim().length) {
          document.querySelector("#search_results").innerHTML = results.html
        }
      })

    }, 500);
  } else {
    document.querySelector("#search_results").innerHTML = ""
  }
}
