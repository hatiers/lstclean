let btns = Array.from(document.getElementsByClassName("btn-sort"));
btns.forEach(function(btn) {

  btn.addEventListener("click", function() {
    sort(this)
  }, false);
});


function sort(btn) {
  let order = btn.dataset.order;
  var asc = (order == "asc") ? true : false;
  let metrique = btn.dataset.metrique;
  //sortLemmas
  let container = $("#collocations");
  let collocations = $("#collocations .entry.listecollocation");
  collocations.sort(function(a, b) {
    return $(a).data(metrique) < $(b).data(metrique) ? asc ? 1 : -1 : asc ? -1 : 1;
  });
  let divCollocations = Array.from(document.querySelectorAll(".entry.listecollocation"));
  divCollocations.forEach(function(divCollocation) {
    let spanText = divCollocation.querySelector(".display-metrique");
      spanText.innerHTML = (metrique == "alpha") ? "" : divCollocation.getAttribute("data-"+metrique);
  });
  btn.dataset.order = (asc) ? "desc" : "asc";
  $(container).html(collocations);
}
