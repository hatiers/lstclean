let btns = Array.from(document.getElementsByClassName("btn-sort"));
btns.forEach(function(btn) {

  btn.addEventListener("click", function() {
    sort(this)
  }, false);
});


function sort(btn) {
  let order = btn.dataset.order;
  var asc = (order == "asc") ? true : false;
  let metrique = btn.dataset.metrique;
  //sortLemmas
  let container = $("#lemmas");
  let lemmas = $("#lemmas .entry.listelemma");
  lemmas.sort(function(a, b) {
    return $(a).data(metrique) < $(b).data(metrique) ? asc ? 1 : -1 : asc ? -1 : 1;
  });
  let divLemmas = Array.from(document.querySelectorAll(".entry.listelemma"));
  divLemmas.forEach(function(divLemma) {
    let spanText = divLemma.querySelector(".display-metrique");
    spanText.innerHTML = (metrique == "alpha") ? "" : divLemma.getAttribute("data-" + metrique);
  });
  btn.dataset.order = (asc) ? "desc" : "asc";
  $(container).html(lemmas);
  $('[data-toggle="popover"]').popover()
  $('[data-toggle="tooltip"]').tooltip()
}