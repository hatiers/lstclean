import Chart from 'chart.js';

Chart.defaults.global.legend.display = false;

export let chartHandler = {
  init: function() {
    var ctx = document.getElementById('chartMetriques');
    var dataJSON = JSON.parse(ctx.dataset.datas)
    const arrayLabels = Object.keys(dataJSON)
    const arrayMetriques = Object.values(dataJSON)

    var myChart = new Chart(ctx, {
      type: 'horizontalBar',
      data: {
        labels: arrayLabels,
        datasets: [{
          label: 'Distribution',
          data: arrayMetriques,
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(225, 246, 66, 0.2)',
            'rgba(75, 192, 0, 0.2)',
            'rgba(153, 0, 186, 0.2)',
            'rgba(0, 159, 64, 0.2)'
          ],
          borderWidth: 1
        }]
      }
    });
  }
}
