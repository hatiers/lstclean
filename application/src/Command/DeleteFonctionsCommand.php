<?php

namespace App\Command;

use App\Entity\Fonction;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;

class DeleteFonctionsCommand extends Command
{
    protected static $defaultName = 'app:delete-fonctions';
    protected static $defaultDescription = 'Delete all fonctions';
    private $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $fonctions = $this->em->getRepository(Fonction::class)->findAll();
        foreach ($fonctions as $fonction) {
          $this->em->remove($fonction);
        }
        $this->em->flush();

        $io->success('fonctions supprimées, vous pouvez les réimporter avec la commande php bin/console app:import-fonction');

        return 0;
    }
}
