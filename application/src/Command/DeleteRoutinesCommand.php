<?php

namespace App\Command;

use App\Entity\Routine;
use App\Entity\ExempleRoutine;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;

class DeleteRoutinesCommand extends Command
{
    protected static $defaultName = 'app:delete-routines';
    protected static $defaultDescription = 'Delete all routines';
    private $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);


        $exemples = $this->em->getRepository(ExempleRoutine::class)->findAll();
        foreach ($exemples as $exemple) {
          $this->em->remove($exemple);
        }
        $io->success('Exemple routines supprimés');

        $routines = $this->em->getRepository(Routine::class)->findAll();
        foreach ($routines as $routine) {
          $this->em->remove($routine);
        }
        $this->em->flush();

        $io->success('Routines supprimées, vous pouvez les réimporter avec la commande php bin/console app:import-routine');

        return 0;
    }
}
