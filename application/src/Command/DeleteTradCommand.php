<?php

namespace App\Command;

use App\Entity\Language;
use App\Manager\LanguageManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DeleteTradCommand extends Command
{
    private $em;
    private $lm;

    public function __construct(
        EntityManagerInterface $em,
        LanguageManager $lm

    ) {
        $this->em = $em;
        $this->lm = $lm;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:delete-trad')
            ->setDescription('delete LST Trad')
            ->setHelp('Suppression des fichiers LST Trad')
            ->addArgument('language', InputArgument::REQUIRED, 'language');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $language = $input->getArgument('language');
        if ($languageEntity = $this->em->getRepository(Language::class)->findOneByShortName($language)) {
            $this->lm->delete($languageEntity);
            $io->success("Suppression traductions terminée");
        } else {
            $io->success("Pas de langue trouvée.");
        } 
    }
}
