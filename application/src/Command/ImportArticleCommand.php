<?php

namespace App\Command;

// use App\Manager\ArticleManager;
use App\Manager\LexicalEntryManager;
use App\Manager\ArticleManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ImportArticleCommand extends Command
{
    private $em;
    private $params;
    private $lexicalEntryManager;
    // private $articleManager;

    public function __construct(
        EntityManagerInterface $em,
        ParameterBagInterface $params,
        LexicalEntryManager $lexicalEntryManager,
        ArticleManager $articleManager

    ) {
        $this->em = $em;
        $this->params = $params;
        $this->lexicalEntryManager = $lexicalEntryManager;
        $this->articleManager = $articleManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        ->setName('app:import-article')
        ->setDescription('Import Articles annotés déposés dans public/articles')
        ->setHelp('Import fichiers xml des articles annotés présents dans public/articles');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $route = $this->params->get('projectDir');
        $datadir = $route.DIRECTORY_SEPARATOR."public/articles".DIRECTORY_SEPARATOR;
        // list all xml in a directory
        $this->articleManager->importArticlesFromRep($datadir);
    }
}
