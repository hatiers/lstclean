<?php

namespace App\Command;

use App\Manager\CollocationManager;
use App\Manager\ExempleManager;
use App\Manager\ExempleCollocationManager;
use App\Manager\ImportManager;
use App\Manager\LexicalEntryManager;
use App\Manager\MetriquesManager;
use App\Manager\SemanticClassSubClassManager;
use App\Manager\SourceExempleManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ImportCsvCommand extends Command
{
    private $em;
    private $params;
    private $collocationManager;
    private $exempleCollocationManager;
    private $exempleManager;
    private $importManager;
    private $lexicalEntryManager;
    private $semanticClassSubClassManager;
    private $sourceExempleManager;
    private $metriquesManager;

    public function __construct(
        EntityManagerInterface $em,
        ParameterBagInterface $params,
        CollocationManager $collocationManager,
        ExempleManager $exempleManager,
        ExempleCollocationManager $exempleCollocationManager,
        ImportManager $importManager,
        LexicalEntryManager $lexicalEntryManager,
        SemanticClassSubClassManager $semanticClassSubClassManager,
        SourceExempleManager $sourceExempleManager,
        MetriquesManager $metriquesManager

    ) {
        $this->em = $em;
        $this->params = $params;
        $this->collocationManager = $collocationManager;
        $this->exempleCollocationManager = $exempleCollocationManager;
        $this->exempleManager = $exempleManager;
        $this->importManager = $importManager;
        $this->lexicalEntryManager = $lexicalEntryManager;
        $this->semanticClassSubClassManager = $semanticClassSubClassManager;
        $this->sourceExempleManager = $sourceExempleManager;
        $this->metriquesManager = $metriquesManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        ->setName('app:import-csv')
        ->setDescription('Import LST')
        ->setHelp('Import des fichiers LST (entry, collocation, exemple, description source et description classes -tout fichier dans data)')
        ->addArgument('emptydb', InputArgument::OPTIONAL, 'Vidage Base?');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $route = $this->params->get('projectDir');
        $datadir = $route.DIRECTORY_SEPARATOR."data".DIRECTORY_SEPARATOR;

        $emptyOrNot=$input->getArgument('emptydb');

        if ($emptyOrNot == 1) {
            //vidage de la base
            $this->lexicalEntryManager->emptydb();
            $this->collocationManager->emptydb();
            $this->exempleCollocationManager->emptydb();
            $this->semanticClassSubClassManager->emptydb();
            $this->sourceExempleManager->emptydb();
            $this->metriquesManager->emptydb();
        }

        // Entrées
        $filePath = $datadir."LST_mots.csv";
        $lines = file($filePath);
        $this->importManager->parseCSVLexicalEntries($lines);

        // Sources
        $filePath = $datadir."LST_sources.csv";
        $lines = file($filePath);
        $this->sourceExempleManager->parseTXTBiblio($lines);

        // Exemples
        $filePath = $datadir."LST_exemples.csv";
        $lines = file($filePath);
        $this->exempleManager->parseCSVExempleLE($lines);

        // Collocations
        $filePath = $datadir."LST_collocations.csv";
        $lines = file($filePath);
        $this->collocationManager->parseCSVCollocation($lines);

        // Exemples collocations
        $filePath = $datadir."LST_collocations_exemples.csv";
        $lines = file($filePath);
        $this->exempleCollocationManager->parseCSVExempleCollocation($lines);

        // Description classifications
        $filePath = $datadir."LST_classification.csv";
        $lines = file($filePath);
        $this->semanticClassSubClassManager->parseCSVDescriptionClass($lines);

        // Métriques
        $filePath = $datadir."LST_lemma-cat_metriques.csv";
        $lines = file($filePath);
        $this->metriquesManager->parseCSVMetriques($lines);

        $this->em->flush();
    }
}
