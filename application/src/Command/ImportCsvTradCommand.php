<?php

namespace App\Command;

use App\Entity\Language;
use App\Manager\LanguageManager;
use App\Manager\LexicalEntryTradManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Finder\Finder;


class ImportCsvTradCommand extends Command
{
    private $em;
    private $lm;
    private $params;
    private $lexicalEntryTradManager;

    public function __construct(
        EntityManagerInterface $em,
        ParameterBagInterface $params,
        LexicalEntryTradManager $lexicalEntryTradManager,
        LanguageManager $lm

    ) {
        $this->em = $em;
        $this->params = $params;
        $this->lexicalEntryTradManager = $lexicalEntryTradManager;
        $this->lm = $lm;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:import-csv-trad')
            ->setDescription('Import LST Trad')
            ->setHelp('Import des fichiers LST Trad')
            ->addArgument('language', InputArgument::OPTIONAL, 'language');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $route = $this->params->get('projectDir');
        $datadir = $route . DIRECTORY_SEPARATOR . "data" . DIRECTORY_SEPARATOR;
        $language = $input->getArgument('language');

        $finder = new Finder();

        if ($language) {
            $finder->files()->in($datadir)->name("/LST_traductions_" . $language . "\.csv$/");
        } else {
            $finder->files()->in($datadir)->name("/LST_traductions_.{2}\.csv$/");
        }

        foreach ($finder as $file) {
            if (!$language) {
                preg_match('/LST_traductions_(.{2})\.csv$/', $file->getFilename(), $matches);
                $currentLanguage = $matches[1];
            } else {
                $currentLanguage = $language;
            }

            $languageEntity = $this->em->getRepository(Language::class)->findOneByShortName($currentLanguage);
            if ($languageEntity) {
                $io->note('nettoyage des entrées pour la langue -> ' . $currentLanguage . ' ...');
                $io->note('...');
                $this->lm->clean($languageEntity);
                $io->note('nettoyage terminé');
            } else {
                $languageEntity = $this->lm->create($currentLanguage, $currentLanguage, $currentLanguage);
                $io->note('création ' . $currentLanguage);
            }
            
            $io->note('import des entrées pour la langue -> ' . $currentLanguage . ' ...');
            $io->note('...');
            $filePath = $file->getPathname();
            $lines = file($filePath);
            $this->lexicalEntryTradManager->parseCSV($lines, $languageEntity);
            $this->em->flush();
            $io->success('import ' . $currentLanguage . ' terminé');
        }

        $io->success("Import traductions terminé");
    }
}
