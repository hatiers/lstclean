<?php

namespace App\Command;

use App\Manager\FonctionManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportFonctionCollocationsCommand extends Command
{
    private $em;
    private $params;
    private $lexicalEntryTradManager;

    public function __construct(
        EntityManagerInterface $em,
        ParameterBagInterface $params,
        FonctionManager $fonctionManager
    ) {
        $this->em = $em;
        $this->params = $params;
        $this->fonctionManager = $fonctionManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        ->setName('app:import-fonction-collocations')
        ->setDescription('Import LST Fonctions collocations')
        ->setHelp('Import des fichiers LST Fonctions collocations')
        ->addArgument('emptydb', InputArgument::OPTIONAL, 'Vidage Base?');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $route = $this->params->get('projectDir');
        $datadir = $route.DIRECTORY_SEPARATOR."data".DIRECTORY_SEPARATOR;
        $filePath = $datadir."LST_fonctions_collocations.csv";
        $lines = file($filePath);
        $this->fonctionManager->parseCSVCollocations($lines);

        $io->success('ok');
    }
}
