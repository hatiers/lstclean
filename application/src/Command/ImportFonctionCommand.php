<?php

namespace App\Command;

use App\Manager\FonctionManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportFonctionCommand extends Command
{
    private $em;
    private $params;
    private $lexicalEntryTradManager;

    public function __construct(
        EntityManagerInterface $em,
        ParameterBagInterface $params,
        FonctionManager $fonctionManager

    ) {
        $this->em = $em;
        $this->params = $params;
        $this->fonctionManager = $fonctionManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        ->setName('app:import-fonction')
        ->setDescription('Import LST Fonctions')
        ->setHelp('Import des fichiers LST Fonctions')
        ->addArgument('emptydb', InputArgument::OPTIONAL, 'Vidage Base?');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $route = $this->params->get('projectDir');
        $datadir = $route.DIRECTORY_SEPARATOR."data".DIRECTORY_SEPARATOR;
        $filePath = $datadir."LST_fonctions.csv";
        $lines = file($filePath);
        $this->fonctionManager->parseCSV($lines);

        $io->success('ok');

    }
}
