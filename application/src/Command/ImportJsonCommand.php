<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use Symfony\Component\Finder\Finder;

use  App\Manager\jsonImportManager\JsonSemanticClassesManager;
use  App\Manager\jsonImportManager\JsonSourceExempleManager;
use  App\Manager\jsonImportManager\JsonFonctionsManager;
use  App\Manager\jsonImportManager\JsonEntriesManager;
use  App\Manager\jsonImportManager\JsonCollocationsManager;
use  App\Manager\jsonImportManager\JsonRoutinesManager;
use  App\Manager\jsonImportManager\JsonTranslationsManager;
use  App\Manager\jsonImportManager\JsonEncoderDocumentation;


use App\Manager\jsonImportManager\EmptyDbManager;

class ImportJsonCommand extends Command
{
    private $em;
    private $params;
    private $jsonSemanticClassesManager;
    private $jsonFonctionsManager;
    private $jsonEntriesManager;
    private $jsonCollocationsManager;
    private $jsonRoutinesManager;
    private $jsonSourceExempleManager;
    private $jsonTranslationsManager;    
    private $jsonEncoderDocumentation;
    private $emptyDbManager;

    public function __construct(
        EntityManagerInterface $em,
        ParameterBagInterface $params,
        JsonSemanticClassesManager $jsonSemanticClassesManager,
        JsonFonctionsManager $jsonFonctionsManager,
        JsonEntriesManager $jsonEntriesManager,
        JsonSourceExempleManager $jsonSourceExempleManager,
        JsonCollocationsManager $jsonCollocationsManager,
        JsonRoutinesManager $jsonRoutinesManager,
        JsonTranslationsManager $jsonTranslationsManager, 
        EmptyDbManager $emptyDbManager,
        JsonEncoderDocumentation $jsonEncoderDocumentation

    ) {
        $this->em = $em;
        $this->params = $params;

        $this->jsonSemanticClassesManager = $jsonSemanticClassesManager;
        $this->jsonFonctionsManager = $jsonFonctionsManager;
        $this->jsonEntriesManager = $jsonEntriesManager;
        $this->jsonSourceExempleManager = $jsonSourceExempleManager;
        $this->jsonRoutinesManager = $jsonRoutinesManager;
        $this->jsonCollocationsManager = $jsonCollocationsManager;
        $this->jsonTranslationsManager = $jsonTranslationsManager;
        $this->jsonEncoderDocumentation = $jsonEncoderDocumentation;

        $this->emptyDbManager = $emptyDbManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        ->setName('app:import-json')
        ->setDescription('Import JSON LST')
        ->setHelp("Vide la base et réimporte les données du LST à partir du fichier /data/json/lstdata.json");
        // ->addArgument('emptydb', InputArgument::OPTIONAL, 'Vidage Base?');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Je n'arrive pas à faire fonctionner cette option : je commente tout
        // de toute façon, en cas de réimport, il faut vider la base. 

        // $emptyOrNot=$input->getArgument('emptydb');
        // if ($emptyOrNot == 1) {
            // echo " empty   " . "\n" . "\n";
            //vidage de la base
            // $this->emptyDbManager->truncateAllTables();
        // }

        $this->emptyDbManager->truncateAllTables();

        $data = $this->getJsonFile("lstdata.json");

        // le contenu du fichier JSON est modulable lors de l'export (on choisit quelles entités récupérer).
        // donc pour éviter une erreur si un réimport est partiel je met des conditions 
        // IL FAUDRAIT AUSSI MODIFIER emptyDbManager->truncateAllTables() POUR NE PAS SUPPRIMER CE QUE L'ON VEUT CONSERVER.
        if($data->classesSemantiques){
            $this->jsonSemanticClassesManager->importSemanticClasses($data->classesSemantiques);
        }
        if($data->fonctionsRehtoriques){
            $this->jsonFonctionsManager->importFonctions($data->fonctionsRehtoriques);
        }
        if($data->sourcesExemples){
            $this->jsonSourceExempleManager->importSourceExemple($data->sourcesExemples);
        }

        if($data->entreesLexicales){
            $this->jsonEntriesManager->importLemmas($data->entreesLexicales);
            $this->jsonEntriesManager->importLexicalTypes($data->entreesLexicales);
            $this->jsonEntriesManager->importSources($data->entreesLexicales);
            $this->jsonEntriesManager->importLexicalEntries($data->entreesLexicales);

            $this->jsonSemanticClassesManager->linkClassSubClassesToEntries();
        }
        
        if($data->routines){
            $this->jsonRoutinesManager->importRoutines($data->routines);
        }
        if($data->collocations){
            $this->jsonCollocationsManager->importCollocations($data->collocations);
        }
        if($data->translations){
            $this->jsonTranslationsManager->importLanguages($data->translations);
            $this->jsonTranslationsManager->importLemmaTrads($data->translations);
            $this->jsonTranslationsManager->importLexicalEntryTrads($data->translations);
        }
    }

    public function getJsonFile($filename){
    
        $route = $this->params->get('projectDir');
        $datadir = $route.DIRECTORY_SEPARATOR."data".DIRECTORY_SEPARATOR ."json".DIRECTORY_SEPARATOR ;


        $finder = new Finder();
        $finder->files()->in($datadir);

        echo "\n ---- Read " . $filename . "\n";
        $file = $finder->files()->name($filename);
        foreach ($file as $f) {
            $json = $f;
        }

        $file = fopen( $json, "r" );
        $filesize = filesize( $json );
        $filetext = fread( $file, $filesize );
        fclose( $file );

        $data = json_decode($filetext);
        return $data;

    }

}
