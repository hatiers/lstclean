<?php

namespace App\Command;

use App\Manager\RoutineManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportRoutinesCommand extends Command
{
    private $em;
    private $params;
    private $lexicalEntryTradManager;

    public function __construct(
        EntityManagerInterface $em,
        ParameterBagInterface $params,
        RoutineManager $routineManager

    ) {
        $this->em = $em;
        $this->params = $params;
        $this->routineManager = $routineManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        ->setName('app:import-routine')
        ->setDescription('Import LST Routine')
        ->setHelp('Import des fichiers LST Routine');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $route = $this->params->get('projectDir');
        $datadir = $route.DIRECTORY_SEPARATOR."data".DIRECTORY_SEPARATOR;
        $filePath = $datadir."LST_routines.csv";
        $lines = file($filePath);
        $this->routineManager->parseCSV($lines);

        $filePath = $datadir."LST_routines_exemples.csv";
        $lines = file($filePath);
        $this->routineManager->parseCSVExemples($lines);

        $io->success('ok');

    }
}
