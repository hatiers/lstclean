<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\User;


class MakeAdminCommand extends Command
{
  private $em;
  protected static $defaultName = 'app:toggleadmin';

  public function __construct(EntityManagerInterface $em)
  {
      $this->em = $em;
      parent::__construct();
  }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('mail', InputArgument::REQUIRED, 'mail')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $mail = $input->getArgument('mail');

        if ($mail) {
            if ($user = $this->em->getRepository(User::class)->findOneByEmail($mail)) {
                $roles = $user->getRoles();
                if (!in_array("ROLE_ADMIN", $roles)) {
                  $roles[] = "ROLE_ADMIN";
                  $io->success('role admin ajouté');
                } else {
                  $i = 0;
                  foreach ($roles as $role) {
                    if ($role == "ROLE_ADMIN" ) {
                      unset($roles[$i]);
                    }
                    $i++;
                  }
                  $io->success('role admin enlevé');
                }

                $user->setRoles($roles);
                $this->em->persist($user);
                $this->em->flush();

            } else {
                $io->success('user not found');
            }
        }
    }
}
