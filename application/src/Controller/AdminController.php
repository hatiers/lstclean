<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Collocation;
use App\Entity\LexicalEntry;
use App\Entity\LexicalEntryTrad;
use App\Entity\User;
use App\Form\ImportCSVType;
use App\Form\ImportXMLType;
use App\Manager\ArticleManager;
use App\Manager\CollocationManager;
use App\Manager\ExempleCollocationManager;
use App\Manager\ExempleManager;
use App\Manager\ImportManager;
use App\Manager\LexicalEntryManager;
use App\Manager\MetriquesManager;
use App\Manager\SemanticClassSubClassManager;
use App\Manager\SourceExempleManager;
use App\Manager\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/admin", name="admin_")
 */
class AdminController extends AbstractController
{
    private $em;
    private $exempleManager;
    private $collocationManager;
    private $lexicalEntryManager;
    private $exempleCollocationManager;
    private $semanticClassSubClassManager;
    private $sourceExempleManager;
    private $metriquesManager;

    public function __construct(EntityManagerInterface $em, ExempleManager $exempleManager, CollocationManager $collocationManager, LexicalEntryManager $lexicalEntryManager, ExempleCollocationManager $exempleCollocationManager, SemanticClassSubClassManager $semanticClassSubClassManager, SourceExempleManager $sourceExempleManager, MetriquesManager $metriquesManager)
    {
        $this->em                         = $em;
        $this->exempleManager             = $exempleManager;
        $this->collocationManager         = $collocationManager;
        $this->lexicalEntryManager        = $lexicalEntryManager;
        $this->exempleCollocationManager  = $exempleCollocationManager;
        $this->sourceExempleManager       = $sourceExempleManager;
        $this->metriquesManager           = $metriquesManager;
        $this->semanticClassSubClassManager  = $semanticClassSubClassManager;
    }

    /**
     * @Route("/users", name="list_users")
     */
    public function index()
    {
        $users = $this->em->getRepository(User::class)->findAll();

        return $this->render('admin/users.html.twig', [
            "users" => $users
        ]);
    }

    /**
     * @Route("/import/article", name="import_article")
     */
    public function importArticle(Request $request, ArticleManager $articleManager)
    {
        //Lance le buildForm du fichier ImportType.php
        $form = $this->createForm(ImportXMLType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $fileDescriptionClass = $data['articlesXml'];
            //fonction import / gestion de rep avec plusieurs fichiers
            $files = $form->get('articlesXml')->getData();
            foreach ($files as $file) {
                $xmlContent = file_get_contents($file);
                $fileName = $file->getClientOriginalName();
                $lastArticle = $articleManager->importArticlesFromForm($xmlContent, $fileName);
            }
            $this->em->flush();
            $idLastArticle = $lastArticle->getId();
            //Comment renvoyer le plus grand id pour article? (afficher le dernier importé)
            return $this->redirectToRoute('article_display', [
                'id' => $idLastArticle
            ]);
        }
        $articles = $this->em->getRepository(Article::class)->findAll();
        return $this->render('admin/import-article.html.twig', [
            'form' => $form->createView(),
            'articles' => $articles,
        ]);
    }


    /**
     * @Route("/import/csv", name="import_csv")
     */
    public function importCsv(Request $request, ImportManager $importManager, ExempleManager $exempleManager, CollocationManager $collocationManager, LexicalEntryManager $lexicalEntryManager, ExempleCollocationManager $exempleCollocationManager, SemanticClassSubClassManager $semanticClassSubClassManager, SourceExempleManager $sourceExempleManager, MetriquesManager $metriquesManager)
    {
        //Lance le buildForm du fichier ImportType.php
        $form = $this->createForm(ImportCSVType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $checkboxValue = $data['cleandb'];

            if ($checkboxValue) {
                //vidage de la base
                $lexicalEntryManager->emptydb();
                $collocationManager->emptydb();
                $semanticClassSubClassManager->emptydb();
                $sourceExempleManager->emptydb();
                $metriquesManager->emptydb();
            }

            //description source
            $fileDescriptionSource = $data['descriptionBiblio'];
            $lines = file($fileDescriptionSource);
            $sourceExempleManager->parseTXTBiblio($lines);

            $fileLexicalEntries = $data['lexicalentries'];
            $importManager->parseCSVLexicalEntries(file($fileLexicalEntries));

            $fileExempleLexicalEntries = $data['exemplesentries'];
            $exempleManager->parseCSVExempleLE(file($fileExempleLexicalEntries));

            $fileCollocations = $data['collocations'];
            $collocationManager->parseCSVCollocation(file($fileCollocations));

            $fileExempleCollocations = $data['exemplescollocations'];
            $exempleCollocationManager->parseCSVExempleCollocation(file($fileExempleCollocations));

            $fileDescriptionClass = $data['descriptionClass'];
            $semanticClassSubClassManager->parseCSVDescriptionClass(file($fileDescriptionClass));

            $fileDescriptionClass = $data['metriques'];
            $metriquesManager->parseCSVMetriques(file($fileDescriptionClass));

            $this->em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('admin/import-csv.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/user/switch/{id}", name="switch_admin")
     */
    public function switchUser(User $user, UserManager $um)
    {
        $um->switchAdmin($user);
        $this->addFlash('success', "switch admin done");

        return $this->redirectToRoute('admin_list_users');
    }

    /**
     * @Route("/user/{id}", name="delete_user")
     */
    public function deleteUser(User $user)
    {
        $this->em->remove($user);
        $this->em->flush();

        $this->addFlash('success', "utilisateur supprimé");

        return $this->redirectToRoute('admin_list_users');
    }

    /**
     * @Route("/users/export", name="export_users")
     */
    public function exportUsers()
    {
        $encoders = [new CsvEncoder(";")];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $users = $this->em->getRepository(User::class)->findAll();
        if (count($users) > 1) {
            $fileSystem = new Filesystem();
            $root = $this->getParameter("kernel.project_dir") . "/public/";
            $file = "export/" . uniqid() . ".csv";

            $csvContent = $serializer->serialize($users, 'csv');

            $fileSystem->appendToFile($root . $file, $csvContent);
            $this->addFlash('success', "export OK");
        } else {
            $this->addFlash('danger', "no data");

            return $this->redirectToRoute('admin_list_users');
        }

        return $this->render('admin/users.html.twig', [
            "users" => $users,
            "file" => $file
        ]);
    }

    /**
     * @Route("/article/{id}", name="delete_article")
     */
    public function deleteArticle(Article $article)
    {
        $this->em->remove($article);
        $this->em->flush();

        $this->addFlash('success', "Article supprimé");

        return $this->redirectToRoute('admin_import_article');
    }

    /**
     * @Route("/logs/", name="logs")
     */
    public function checkDatas()
    {
        $collocsWoEx = $this->em->getRepository(Collocation::class)->findMissingExamples();
        $collocsWoSource = $this->em->getRepository(Collocation::class)->findMissingSource();
        $entriesWoEx = $this->em->getRepository(LexicalEntry::class)->findMissingExamples();
        $entriesWoSource = $this->em->getRepository(LexicalEntry::class)->findMissingSource();
        $entriesWoMetriques = $this->em->getRepository(LexicalEntry::class)->findMissingMetriques();
        $collocsCyclic = $this->em->getRepository(Collocation::class)->findCycle();
        $tradsWoLstBase = $this->em->getRepository(LexicalEntryTrad::class)->findMissingBase();
        // $entries = $this->lexicalEntryManager->checkDatas();
        // $examplesEntries = $this->exempleManager->checkDatas();
        // $examplesCollocs = $this->exempleCollocationManager->checkDatas();
        // $sourceExample = $this->sourceExampleManager->checkDatas();
        // $metriques = $this->metriquesManager->checkDatas();
        return $this->render('admin/logs.html.twig', [
            "collocsWoEx" => $collocsWoEx,
            "collocsWoSource" => $collocsWoSource,
            "collocsCyclic" => $collocsCyclic,
            "entriesWoEx" => $entriesWoEx,
            "entriesWoSource" => $entriesWoSource,
            "entriesWoMetriques" => $entriesWoMetriques,
            "tradsWoLstBase" => $tradsWoLstBase
        ]);
    }
}
