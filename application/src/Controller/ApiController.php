<?php

namespace App\Controller;

use App\Entity\LexicalEntry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api", name="api_")
 */
class ApiController extends AbstractController
{
  /**
   * @Route("/lexical-entries", name="lexical_entries")
   */
  public function lexicalEntries()
  {
    $entries =  $this->getDoctrine()->getRepository(LexicalEntry::class)->findAll();
    $lexicalEntries = [];

    foreach ($entries as $entry) {
      $lexicalEntry = [];
      $lexicalEntry["id"] = $entry->getId();
      $lexicalEntry["cat"] = $entry->getCat()->getValue();
      $lexicalEntry["acceptionUniq"] = $entry->getAcceptionUniq();
      $lexicalEntry["lemma"] = $entry->getLemma()->getValue();
      $lexicalEntry["lexicalType"] = $entry->getLexicalType()->getValue();
      $lexicalEntry["propMorph"] = $entry->getPropMorph();
      $lexicalEntry["propSynt"] = $entry->getPropSynt();
      $lexicalEntry["semanticClass"] = $entry->getSemanticClass()->getValue();
      $lexicalEntry["semanticSubClass"] = $entry->getSemanticSubClass()->getValue();

      $lexicalEntries[] = $lexicalEntry;
    }

    return new JsonResponse($lexicalEntries);
  }

}
