<?php

namespace App\Controller;

use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/article", name="article_")
 */
class ArticleController extends AbstractController
{
  /**
   * @Route("/display/{id}", name="display", requirements={"id"="\d+"})
   */
  public function display(Article $article)
  {
    return $this->render('article/display.html.twig', [
      'article' => $article,
    ]);
  }

  /**
   * @Route("/partial/{id}", name="partial", requirements={"id"="\d+"}, options={"expose"=true})
   */
  public function partial(Article $article)
  {
    $html = $this->renderView("article/partial.html.twig", ['article' => $article]);

    $data = [];
    $data["html"] = $html;

    return new JsonResponse($data);
  }


  /**
   * @Route("/all", name="all")
   */
  public function displayAll()
  {
    $articles = $this->getDoctrine()->getRepository(Article::class)->findAll();

    return $this->render('article/all.html.twig', [
      'articles' => $articles,
    ]);
  }
}
