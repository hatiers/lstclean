<?php

namespace App\Controller;

use App\Entity\Collocation;
use App\Manager\CacheManager;
use App\Manager\LexicalEntryManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/collocations", name="collocation_")
 */
class CollocationController extends AbstractController
{

    /**
     * @Route("/exemples/{id}", name="exemples", options={"expose"=true})
     */
    public function exemples(Collocation $collocation)
    {
        $html = $this->renderView("collocation/examples.html.twig", ['collocation' => $collocation]);

        $data = [];
        $data["html"] = $html;

        return new JsonResponse($data);
    }

    /**
     * @Route("/all", name="all")
     */
    public function listAll(CacheManager $cm)
    {
        $collocationsCache = $cm->get("collocation.all");
        if ($collocationsCache->isHit()) {
            $collocations = $collocationsCache->get();
        } else {
            $collocations = $this->getDoctrine()->getRepository(Collocation::class)->findBy([], ["value" => "ASC"]);
            $cm->store($collocationsCache, $collocations);
        }

        return $this->render('collocation/index.html.twig', [
            'collocations' => $collocations,
        ]);
    }

    /**
     * @Route("/{id}", name="display", requirements={"id"="\d+"}, options={"expose"=true})
     */
    public function displayCollocation(Collocation $collocation)
    {
        // $collocation = $this->getDoctrine()->getRepository(Collocation::class)->findById($id);
        return $this->render('collocation/display.html.twig', [
            'collocation' => $collocation,
        ]);
    }

    /**
     * @Route("/graph", name="graph")
     */
    public function displayGraphColloc(CacheManager $cm, LexicalEntryManager $entryManager)
    {
        $arrayDatas = $entryManager->getJSONGraphCollocs();
        $datas = json_encode($arrayDatas);
        $fonctionLexicale = $entryManager->edgeToData($arrayDatas, "fonctionLexicale");
        $structureColloc = $entryManager->edgeToData($arrayDatas, "structureColloc");
        return $this->render('graph/graph-colloc.html.twig', [
            'datas' => $datas,
            'fonctionLexicale' => $fonctionLexicale,
            'structureColloc' => $structureColloc
        ]);
    }
}
