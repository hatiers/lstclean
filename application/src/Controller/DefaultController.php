<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        return $this->render('homepage.html.twig');
    }

    /**
     * @Route("/about", name="about")
     */
    public function about()
    {
        return $this->render('about.html.twig');
    }

    /**
     * @Route("/mentions-legales", name="legal_notice")
     */
    public function legalNotice()
    {
        return $this->render('legal.html.twig');
    }

    /**
     * @Route("/_locale/{_locale}", name="locale_change")
     */
    public function localeChange()
    {
        return $this->redirectToRoute('homepage');
    }
}
