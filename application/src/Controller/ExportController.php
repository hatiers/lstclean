<?php

namespace App\Controller;

use App\Entity\Collocation;
use App\Entity\Fonction;
use App\Entity\Language;
use App\Entity\LexicalEntry;
use App\Entity\LexicalEntryTrad;
use App\Entity\SourceExemple;
use App\Manager\ExportFunctions;
use App\Manager\JsonEncoder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Manager\jsonImportManager\JsonEncoderDocumentation;
use App\Form\ExportType;

/**
 * @Route("/export", name="export_")
 */
class ExportController extends AbstractController
{
  private $jsonEncoderDocumentation;
  private $exportFunctions;
  private $jsonEncoder;

  public function __construct(
    ExportFunctions $exportFunctions,
    JsonEncoder $jsonEncoder,
    JsonEncoderDocumentation $jsonEncoderDocumentation
  ) {
    $this->exportFunctions = $exportFunctions;
    $this->jsonEncoder = $jsonEncoder;
    $this->jsonEncoderDocumentation = $jsonEncoderDocumentation;
  }

  /**
   * @Route("/options", name="options", methods={"GET", "POST"})
   */
  public function optionsExport(Request $request): Response
  {

    $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

    $translationLanguages = $this->listTranslationsLanguages();

    $form = $this->createForm(ExportType::class, $translationLanguages);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $passedOptions = $form->getData();

      $format = $passedOptions["output_format"]; // json or xml
      $selectedTrads = $passedOptions["translations_languages"];

      $entitiesToDump = [];

      foreach ($passedOptions as $key => $value) {
        switch ($key) {
          case "entries":
            if ($value) {
              $entriesExempleExport = ($passedOptions['entries_examples']) ? true : false;
              $entitiesToDump["entreesLexicales"] = $this->dumpEntries($format, $entriesExempleExport);
            }
            break;
          case "classes":
            if ($value) {
              $entitiesToDump["classesSemantiques"] = $this->dumpClasses($format);
            }
            break;
          case "collocations":
            if ($value) {
              $collocationsExempleExport = ($passedOptions['collocations_examples']) ? true : false;
              $entitiesToDump["collocations"] = $this->dumpCollocations($format, $collocationsExempleExport);
            }
            break;
          case "translations":
            if ($value) {
              $entitiesToDump["traductions"] = $this->dumpTranslations($format, $selectedTrads);  // AJOUTER LES OPTIONS DE TRADUCTION
            }
            break;
          case "fonctions":
            if ($value) {
              $entitiesToDump["fonctionsRhetoriques"] = $this->dumpFonctions($format);
            }
            break;
          case "routines":
            if ($value) {
              $collocationsExempleExport = ($passedOptions['routines_examples']) ? true : false;
              $entitiesToDump["routines"] = $this->dumpRoutines($format, $collocationsExempleExport);
            }
            break;
          case "examples_sources":
            if ($value) {
              $entitiesToDump["sourcesExemples"] = $this->dumpExemplesSources($format);
            }
            break;
          case "export_documentation":
            if ($value and $format == "json") {
              $entitiesToDump["DOCUMENTATION"] = $this->jsonEncoderDocumentation->docAll();
            }
            break;
        }
      }
      if ($format == 'xml') {
        $this->xmlTemplatesToZip($entitiesToDump);
      } elseif ($format == 'json') {
        $this->jsonArraysToZip($entitiesToDump);
      }

      return $this->redirectToRoute('export_zipsend', [], Response::HTTP_SEE_OTHER);
    }

    return $this->render('admin/export-options.html.twig', ['form' => $form->createView(),]);
  }

  public function listTranslationsLanguages()
  {

    $langList = [];
    $langList['Toutes les langues'] = "all";
    $languages = $this->getDoctrine()->getRepository(Language::class)->findAll();
    foreach ($languages as $language) {
      $langList[$language->getName()] = $language->getShortName();
    }
    return $langList;
  }

  public function dumpEntries($format, $exportExemples)
  {

    $entries = $this->getDoctrine()->getRepository(LexicalEntry::class)->findAll();

    if ($format == "xml") {
      $xml =  $this->renderView(
        'export/entries.xml.twig',
        [
          'entries' => $entries,
          'export_exemples' => $exportExemples
        ]
      );

      $prettyxml = $this->exportFunctions->xmlPrettify($xml);

      return $prettyxml;
    } elseif ($format == "json") {
      $json = $this->jsonEncoder->jsonLexicalEntries($entries);
      return $json;
    }
  }

  public function dumpClasses($format)
  {

    $classes = $this->exportFunctions->retrieveSemanticClasses();

    if ($format == "xml") {

      $xml =  $this->renderView(
        'export/semantic-classes.xml.twig',
        ['classes' => $classes]
      );

      $prettyxml = $this->exportFunctions->xmlPrettify($xml);

      return $prettyxml;
    } elseif ($format == "json") {
      $json = $this->jsonEncoder->jsonSemanticClasses($classes);

      return $json;
    }
  }


  public function dumpCollocations($format, $exportExemples)
  {

    $collocations = $this->getDoctrine()->getRepository(Collocation::class)->findAll();

    if ($format == "xml") {

      $xml =  $this->renderView(
        'export/collocations.xml.twig',
        [
          'collocations' => $collocations,
          'export_exemples' => $exportExemples
        ]
      );
      $prettyxml = $this->exportFunctions->xmlPrettify($xml);

      return $prettyxml;
    } elseif ($format == "json") {
      $json = $this->jsonEncoder->jsonCollocations($collocations);

      return $json;
    }
  }

  public function dumpTranslations($format, $selectedLanguages)
  {
    $selectedTranslations = [];
    if ($selectedLanguages == ["Toutes les langues" => "all"]) {
      $languages = $this->getDoctrine()->getRepository(Language::class)->findAll();
      foreach ($languages as $language) {
        $langCode = $language->getShortName();
        $trads = $this->getDoctrine()->getRepository(LexicalEntryTrad::class)->findByLanguage($langCode);
        $selectedTranslations[$langCode] = $trads;
      }
    } else {
      foreach ($selectedLanguages as $lang) {
        $trads = $this->getDoctrine()->getRepository(LexicalEntryTrad::class)->findByLanguage($lang);
        $selectedTranslations[$lang] = $trads;
      }
    }

    if ($format == "xml") {
      // $translations = $this->getDoctrine()->getRepository(LexicalEntryTrad::class)->findAll();
      $xml =  $this->renderView('export/translations.xml.twig', ['selectedTranslations' => $selectedTranslations]);
      $prettyxml = $this->exportFunctions->xmlPrettify($xml);

      return $prettyxml;
    } elseif ($format == "json") {

      $json = $this->jsonEncoder->jsonTranslations($selectedTranslations);

      return $json;
    }
  }

  public function dumpFonctions($format)
  {

    $fonctions = $this->getDoctrine()->getRepository(Fonction::class)->findAll();

    if ($format == "xml") {

      $xml =  $this->renderView(
        'export/fonctions.xml.twig',
        ['fonctions' => $fonctions,]
      );

      $prettyxml = $this->exportFunctions->xmlPrettify($xml);

      return $prettyxml;
    } elseif ($format == "json") {
      $json = $this->jsonEncoder->jsonFonctions($fonctions);

      return $json;
    }
  }

  public function dumpRoutines($format, $exportExemples)
  {

    $reformatedRoutines = $this->exportFunctions->reformatRoutines();

    if ($format == "xml") {

      $xml =  $this->renderView(
        'export/routines.xml.twig',
        [
          'routines' => $reformatedRoutines['routines'],
          'patronList' => $reformatedRoutines['patronList'],
          'export_exemples' => $exportExemples
        ]
      );

      $prettyxml = $this->exportFunctions->xmlPrettify($xml);

      return $prettyxml;
    } elseif ($format == "json") {
      $json = $this->jsonEncoder->jsonRoutines($reformatedRoutines['routines']);

      return $json;
    }
  }

  public function dumpExemplesSources($format)
  {

    $sources = $this->getDoctrine()->getRepository(SourceExemple::class)->findAll();

    if ($format == "xml") {
      $xml =  $this->renderView('export/examples-sources.xml.twig', ['sources' => $sources]);
      $prettyxml = $this->exportFunctions->xmlPrettify($xml);

      return $prettyxml;
    } elseif ($format == "json") {
      $json = $this->jsonEncoder->jsonExemplesSources($sources);

      return $json;
    }
  }


  /*
    zip the multiple strings returned by the xml templates as multiple files
  */
  public function xmlTemplatesToZip($entitiesToDump)
  {

    if (!extension_loaded('zip')) {
      return false;
    }

    $zip = new \ZipArchive;

    if (!$zip->open("/tmp/lstdata.zip", \ZipArchive::CREATE)) {
      return false;
    }

    foreach ($entitiesToDump as $name => $fileContent) {
      $fileName = $name . '.xml';
      $zip->addFromString($fileName, $fileContent);
    }
    $zip->close();
  }

  /* 
    zip the arrays returned by the Encoder services as a single JSON file
  */
  public function jsonArraysToZip($entitiesToDump)
  {

    if (!extension_loaded('zip')) {
      return false;
    }

    $zip = new \ZipArchive;

    if (!$zip->open("/tmp/lstdata.zip", \ZipArchive::CREATE)) {
      return false;
    }

    $jsonData = json_encode($entitiesToDump);
    $zip->addFromString("lstdata.json", $jsonData);
    // Si un jour on change d'avis et on veut des JSON séparés ... 
    // foreach($entitiesToDump as $name => $fileContent){
    //   $fileName = $name . '.' . $format;
    //   $zip->addFromString($fileName, json_encode($fileContent)); // encode to JSON on the fly
    // }
    $zip->close();
  }


  /**
   * @Route("/zipsend", name="zipsend")
   */
  public function zipSend(): Response
  {
    $response = $this->file("/tmp/lstdata.zip");
    $response->deleteFileAfterSend(true);
    return $response;
  }

  // FOR TEST : TO DEL ? 
  /**
   * @Route("/doc", name="doc", methods={"GET", "POST"})
   */
  public function docExport()
  {

    $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

    $doc = $this->jsonEncoderDocumentation->docAll();
    $this->jsonArraysToZip($doc);
    return $this->redirectToRoute('export_zipsend', [], Response::HTTP_SEE_OTHER);
  }

  // FOR TEST : TO DEL ? 
  /**
   * @Route("/alljson", name="alljson")
   */
  public function dumpAllJson(): Response
  {

    $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

    $json = [];
    $json["entreesLexicales"] = $this->dumpEntries("json", true); // true = with exemples
    $json["classesSemantiques"] = $this->dumpClasses("json");
    $json["collocations"] = $this->dumpCollocations("json", true);
    $json["traductions"] = $this->dumpTranslations("json", "all");
    $json["fonctionsRhetoriques"] = $this->dumpFonctions("json");
    $json["routines"] = $this->dumpRoutines("json", true);
    $json["sourcesExemples"] = $this->dumpExemplesSources("json");

    $json = json_encode($json);

    if (!extension_loaded('zip')) {
      return false;
    }
    $zip = new \ZipArchive;
    if (!$zip->open("/tmp/lstdata.zip", \ZipArchive::CREATE)) {
      return false;
    }
    $zip->addFromString("lst-complete.json", $json);
    $zip->close();

    $response = $this->file("/tmp/lstdata.zip");
    $response->deleteFileAfterSend(true);

    return $response;
  }

  ############################### ALTERNATIVE ###############################


  //   public function dumpEntitiesFiles($entitiesToDump, $extention){

  //     $fileSystem = new Filesystem();
  //     $fileSystem->mkdir('/tmp/dump');

  //     foreach($entitiesToDump as $name => $fileContent){
  //       $fileName = $name . '.' . $extention;
  //       $fileSystem->dumpFile("/tmp/" . $fileName, $fileContent);

  //     }

  //   }

  // /**
  //  * @Route("/zip-dump", name="zip_dump")
  //  */
  //   public function zipDump()/*(Paratext $paratext)*/
  //   {
  //       $source = '/tmp/dump';

  //       if (!extension_loaded('zip') || !file_exists($source)) {
  //           return false;
  //       }
  //       if (file_exists("/tmp/lstdata.zip")) {
  //           unlink("/tmp/lstdata.zip");
  //       }

  //       $zip = new \ZipArchive;
  //       if (!$zip->open("/tmp/lstdata.zip", \ZipArchive::CREATE)) {
  //           return false;
  //       }
  //       if (is_dir($source)) {
  //           $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($source), \RecursiveIteratorIterator::SELF_FIRST);
  //           // dd($files);
  //           foreach ($files as $file) {
  //               if (in_array(substr($file, strrpos($file, '/') + 1), array('.', '..')))
  //                   continue;
  //               $file = realpath($file);
  //               if (is_dir($file)) {
  //                   $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
  //               } else if (is_file($file)) {
  //                   $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
  //               }
  //           }

  //       } else if (is_file($source)) {
  //           $zip->addFromString(basename($source), file_get_contents($source));
  //       }

  //       $zip->close();

  //       $response = $this->file("/tmp/lstdata.zip");
  //       $response->deleteFileAfterSend(true);

  //       return $response;
  //   }

  ###############################  ###############################


}
