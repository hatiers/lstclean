<?php

namespace App\Controller;

use App\Entity\Collocation;
use App\Entity\LexicalEntry;
use App\Entity\Routine;
use App\Manager\CacheManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/expressions", name="expressions_")
 */
class ExpressionController extends AbstractController
{
    /**
     * @Route("/all", name="all")
     */
    public function listAll(CacheManager $cm)
    {
        $collocationsCache = $cm->get("collocation.all");
        if ($collocationsCache->isHit()) {
            $collocations = $collocationsCache->get();
        } else {
            $collocations = $this->getDoctrine()->getRepository(Collocation::class)->findBy([], ["value" => "ASC"]);
            $cm->store($collocationsCache, $collocations);
        }
        $routines = $this->getDoctrine()->getRepository(Routine::class)->findAll();
        $polylexicals = $this->getDoctrine()->getRepository(LexicalEntry::class)->findPolylexicals();

        return $this->render('expression/index.html.twig', [
            'collocations' => $collocations,
            'routines' => $routines,
            'polylexicals' => $polylexicals
        ]);
    }

    /**
     * @Route("/collocations", name="collocations")
     */
    public function listCollocations(CacheManager $cm)
    {
        $collocationsCache = $cm->get("collocation.all");
        if ($collocationsCache->isHit()) {
            $collocations = $collocationsCache->get();
        } else {
            $collocations = $this->getDoctrine()->getRepository(Collocation::class)->findBy([], ["value" => "ASC"]);
            $cm->store($collocationsCache, $collocations);
        }

        return $this->render('expression/collocations.html.twig', [
            'collocations' => $collocations,
        ]);
    }

    /**
     * @Route("/routines", name="routines")
     */
    public function listRoutines()
    {
        $routines = $this->getDoctrine()->getRepository(Routine::class)->findAll();

        return $this->render('expression/routines.html.twig', [
            'routines' => $routines
        ]);
    }

    /**
     * @Route("/polylexicals", name="polylexicals")
     */
    public function listPolylexicals()
    {   
        $polylexicals = $this->getDoctrine()->getRepository(LexicalEntry::class)->findPolylexicals();

        return $this->render('expression/polylexicals.html.twig', [
            'polylexicals' => $polylexicals
        ]);
    }


    /**
     * @Route("/{id}", name="display", requirements={"id"="\d+"}, options={"expose"=true})
     */
    public function displayCollocation(Collocation $collocation)
    {

        return $this->render('collocation/display.html.twig', [
            'collocation' => $collocation,
        ]);
    }
}
