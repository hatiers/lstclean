<?php

namespace App\Controller;

use App\Entity\Fonction;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class FonctionController extends AbstractController
{
  /**
   * @Route("/fonctions", name="fonctions")
   */
  public function index()
  {
    $fonctions = $this->getDoctrine()->getRepository(Fonction::class)->findAll();

    return $this->render('fonction/index.html.twig', [
      'fonctions' => $fonctions
    ]);
  }

  /**
   * @Route("/fonction/generalf/{generalf}/label/{label}", name="generalf_label")
   */
  public function generalfLabel($generalf, $label)
  {
    $fonction = $this->getDoctrine()->getRepository(Fonction::class)->findOneBy(["generalf" => $generalf, "label" => $label]);

    return $this->render('fonction/generalf-label.html.twig', [
      'fonction' => $fonction
    ]);
  }

  /**
   * @Route("/fonction/generalf/{generalf}", name="generalf")
   */
  public function generalf($generalf)
  {
    $fonctions = $this->getDoctrine()->getRepository(Fonction::class)->findByGeneralf($generalf);

    return $this->render('fonction/generalf.html.twig', [
      'fonctions' => $fonctions
    ]);
  }

  /**
   * @Route("/fonction/{id}", name="fonction_elements", options={"expose"=true})
   */
  public function fonctionElements(Fonction $fonction)
  {
    $entries = $fonction->getLexicalEntries();
    $collocations = $fonction->getCollocations();
    $routines = $fonction->getRoutines();

    $html = $this->renderView("fonction/elements.html.twig", [
      'fonction' => $fonction,
      'entries' => $entries,
      'collocations' => $collocations,
      'routines' => $routines
    ]);

    $data = [];
    $data["html"] = $html;

    return new JsonResponse($data);
  }
}
