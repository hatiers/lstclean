<?php

namespace App\Controller;

use App\Entity\Lemma;
use App\Entity\LexicalEntry;
use App\Manager\CacheManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/lemma", name="lemma_")
 */
class LemmaController extends AbstractController
{
  /**
   * @Route("/display/{id}", name="display", requirements={"id"="\d+"})
   */
  public function display(Lemma $lemma)
  {
    $exactEntries = $this->getDoctrine()->getRepository(LexicalEntry::class)->findByLemma($lemma);

    if (count($exactEntries) == 1) {
      $id = $exactEntries[0]->getId();
      return $this->redirectToRoute('lexicalentry_display', [
        'id' => $id
      ]);
    }

    $entries = $this->getDoctrine()->getRepository(LexicalEntry::class)->findByIncludeLemma($lemma->getValue());

    return $this->render('lemma/display.html.twig', [
      'lemma' => $lemma,
      'entries' => $entries
    ]);
  }

  /**
   * @Route("/all", name="all")
   */
  public function displayAllLemma(CacheManager $cm)
  {
    $lemmasCache = $cm->get("lemma.all");
    if ($lemmasCache->isHit()) {
      $lemmas = $lemmasCache->get();
    } else {
      $lemmas = $this->getDoctrine()->getRepository(Lemma::class)->findBy([], ["value" => "ASC"]);
      $cm->store($lemmasCache, $lemmas);
    }

    return $this->render('lemma/index.html.twig', [
      'lemmas' => $lemmas
    ]);
  }
}
