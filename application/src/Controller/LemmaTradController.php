<?php

namespace App\Controller;

use App\Entity\Language;
use App\Entity\LemmaTrad;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class LemmaTradController extends AbstractController
{
  /**
   * @Route("/language/{shortName}", name="trad_language")
   */
  public function tradbyLanguage(Language $language)
  {
    $trads = $this->getDoctrine()->getRepository(LemmaTrad::class)->findBy(['language' => $language], ["value" => "ASC"]);

    return $this->render('lexical-entry-trad/index.html.twig', [
      'language' => $language,
      'trads' => $trads
    ]);
  }

  /**
   * @Route("/lemmatrad/{id}", name="trad_display")
   */
  public function display(LemmaTrad $lemmaTrad)
  {
    return $this->render('lexical-entry-trad/display.html.twig', [
      'lemmaTrad' => $lemmaTrad
    ]);
  }
}
