<?php

namespace App\Controller;

use App\Entity\LexicalEntry;
use App\Manager\CacheManager;
use App\Manager\LexicalEntryManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/lexicalentry", name="lexicalentry_")
 */
class LexicalEntryController extends AbstractController
{
    private $entryManager;

    public function __construct(LexicalEntryManager $entryManager)
    {
        $this->entryManager = $entryManager;
    }

    /**
     * @Route("/display/{id}", name="display", requirements={"id"="\d+"}, options={"expose"=true})
     */
    public function display(LexicalEntry $lexicalEntry, CacheManager $cm)
    {   
        $rankSpec = $this->entryManager->getRankSpec($lexicalEntry);
        $rankFreq = $this->entryManager->getRankFreq($lexicalEntry);
        $datas = $this->entryManager->getJSONGraphColloc($lexicalEntry);

        return $this->render('lexical-entry/display.html.twig', [
            'lexicalEntry' => $lexicalEntry,
            'rankSpec' => $rankSpec,
            'rankFreq' => $rankFreq,
            'datas' => $datas
        ]);
    }

    /**
     * @Route("/colloc-nodes/{id}", name="colloc_nodes", requirements={"id"="\d+"}, options={"expose"=true})
     */
    public function getEdgesFromNode(LexicalEntry $lexicalEntry)
    {
        $json = $this->entryManager->getJSONGraphColloc($lexicalEntry, true);

        return new JsonResponse($json);
    }

    /**
     * @Route("/get-json/{id}", name="json", requirements={"id"="\d+"}, options={"expose"=true})
     */
    public function getJson(LexicalEntry $lexicalEntry)
    {
        $json = $this->entryManager->getJson($lexicalEntry);

        return new JsonResponse(array('data' => $json));
    }

    /**
     * @Route("/details", name="details", options={"expose"=true})
     */
    public function details(Request $request)
    {
        $acceptionUniqString = $request->request->get("acceptionUniq");
        $lexicalEntry = $this->getDoctrine()->getRepository(LexicalEntry::class)->findOneByAcceptionUniq($acceptionUniqString);
        $rankSpec = $this->entryManager->getRankSpec($lexicalEntry);
        $rankFreq = $this->entryManager->getRankFreq($lexicalEntry);
        $jsonData = $this->entryManager->getJSONGraphColloc($lexicalEntry);

        $html = $this->renderView("lexical-entry/partial.html.twig", ['lexicalEntry' => $lexicalEntry, 'rankFreq' => $rankFreq, 'rankSpec' => $rankSpec, 'datas' => $jsonData]);

        $data = [];
        $data["html"] = $html;

        return new JsonResponse($data);
    }

    /**
     * @Route("/exemples/{id}", name="exemples", options={"expose"=true})
     */
    public function exemples(LexicalEntry $lexicalEntry)
    {
        $html = $this->renderView("lexical-entry/examples.html.twig", ['exemples' => $lexicalEntry->getExemples()]);

        $data = [];
        $data["html"] = $html;

        return new JsonResponse($data);
    }

    /**
     * @Route("/graph", name="graph")
     */
    public function displayGraphEntries(CacheManager $cm)
    {
        $datas = $this->entryManager->getJSONGraph();
        return $this->render('graph/graph-entries.html.twig', [
            'datas' => $datas
        ]);
    }
}
