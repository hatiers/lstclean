<?php

namespace App\Controller;

use App\Entity\Routine;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/routine", name="routine_")
 */
class RoutineController extends AbstractController
{
  /**
   * @Route("/{id}", name="display", requirements={"id"="\d+"}, options={"expose"=true})
   */
  public function display(Routine $routine)
  {
    $patronHeader = [];
    $patronData = [];
    $patron = $routine->getPatron();
    $participants = explode('----', $patron);
    foreach ($participants as $participant) {
      $t = explode('&', $participant);
      $patronHeader[] = $t[0];
      $patronData[] = $t[1];
    }

    return $this->render('routine/display.html.twig', [
      'routine' => $routine,
      'patronHeader' => $patronHeader,
      'patronData' => $patronData,
    ]);
  }

  /**
   * @Route("/exemples/{id}", name="exemples", options={"expose"=true})
   */
  public function exemples(Routine $routine)
  {
    $html = $this->renderView("routine/examples.html.twig", ['routine' => $routine]);

    $data = [];
    $data["html"] = $html;

    return new JsonResponse($data);
  }
}
