<?php

namespace App\Controller;

use App\Entity\Collocation;
use App\Entity\LemmaTrad;
use App\Entity\LexicalEntry;
use App\Entity\Routine;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    /**
     * @Route("/search-everywhere", name="search_everywhere", options={"expose"=true})
     */
    public function search(Request $request)
    {
      $str = trim($request->get("str"));

      $lexicalEntries = $this->getDoctrine()->getRepository(LexicalEntry::class)->findByContaining($str);
      $collocations = $this->getDoctrine()->getRepository(Collocation::class)->findByContaining($str);
      $routines = $this->getDoctrine()->getRepository(Routine::class)->findByContaining($str);
      $trads = $this->getDoctrine()->getRepository(LemmaTrad::class)->findByContaining($str);

      $html = $this->renderView("search/results.html.twig", [
        'lexicalEntries' => $lexicalEntries,
        'collocations' => $collocations,
        'routines' => $routines,
        'trads' => $trads
      ]);

      $data = [];
      $data["html"] = $html;

      return new JsonResponse($data);
    }
}
