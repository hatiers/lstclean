<?php

namespace App\Controller;

use App\Entity\SemanticClassSubClass;
use App\Entity\SyntacCat;
use App\Manager\CacheManager;
use App\Manager\LexicalEntryManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SemanticClassController extends AbstractController
{
    /**
       * @Route("/classification", name="classification")
       */
    public function displayClassification(CacheManager $cm)
    {
        $syntacCats = $this->getDoctrine()->getRepository(SyntacCat::class)->findBy([], ["value" => "ASC"]);

        $semanticClassesSubClassesCache = $cm->get("SemanticClassSubClass.all");
        if ($semanticClassesSubClassesCache->isHit()) {
            $semanticClassesSubClasses = $semanticClassesSubClassesCache->get();
        } else {
            $semanticClassesSubClasses = $this->getDoctrine()->getRepository(SemanticClassSubClass::class)->findAndSort();
            $cm->store($semanticClassesSubClassesCache, $semanticClassesSubClasses);
        }
        $syntacCatsCache = $cm->get("syntactCat.all");
        if ($syntacCatsCache->isHit()) {
            $syntacCats = $syntacCatsCache->get();
        } else {
            $syntacCats = $this->getDoctrine()->getRepository(SyntacCat::class)->findBy([], ["value" => "ASC"]);
            $cm->store($syntacCatsCache, $syntacCats);
        }


        return $this->render('semantic-class/index.html.twig', [
            'syntacCats' => $syntacCats,
            'semanticClassesSubClasses' => $semanticClassesSubClasses,
          ]);
    }

    /**
       * @Route("/classification/cat/{id}", name="classification_categorie")
       */
    public function displayClassificationByCategory(SyntacCat $syntacCat, CacheManager $cm)
    {
        $semanticClassesSubClassesCache = $cm->get("classSubClassBysyntactCat".$syntacCat->getId());
        if ($semanticClassesSubClassesCache->isHit()) {
            $semanticClassesSubClasses = $semanticClassesSubClassesCache->get();
        } else {
            $semanticClassesSubClasses = $this->getDoctrine()->getRepository(SemanticClassSubClass::class)->findAndSortByCat($syntacCat);
            $cm->store($semanticClassesSubClassesCache, $semanticClassesSubClasses);
        }

        $syntacCatsCache = $cm->get("syntactCat.all");
        if ($syntacCatsCache->isHit()) {
            $syntacCats = $syntacCatsCache->get();
        } else {
            $syntacCats = $this->getDoctrine()->getRepository(SyntacCat::class)->findBy([], ["value" => "ASC"]);
            $cm->store($syntacCatsCache, $syntacCats);
        }

        $syntacCatCache = $cm->get("syntactCat".$syntacCat->getId());
        if ($syntacCatCache->isHit()) {
            $syntacCat = $syntacCatCache->get();
        } else {
            $cm->store($syntacCatCache, $syntacCat);
        }

        return $this->render('semantic-class/index.html.twig', [
          'semanticClassesSubClasses' => $semanticClassesSubClasses,
          'syntacCats' => $syntacCats,
          'syntacCat' => $syntacCat,
        ]);
    }

    /**
       * @Route("classification/graph", name="classification_graph")
       */
    public function displayGraphClass(LexicalEntryManager $entryManager)
    {
        $datas = $entryManager->getJSONGraphCollocs();

        return $this->render('graph/graph-colloc.html.twig', [
          'datas' => $datas
        ]);
    }
}
