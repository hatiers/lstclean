<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CollocationRepository")
 */
class Collocation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $structureGlobale;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $value;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $propSyntax;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $structureColloc;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lemmaBase;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\LexicalEntry", inversedBy="collocationsBase")
     */
    private $lexicalEntryBase;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lemmaCollocatif;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\LexicalEntry", inversedBy="collocationsCollocatif")
     */
    private $lexicalEntryCollocatif;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fonctionLexicale;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbOccurrences;



    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $logLike;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $frequence;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbDisciplines=0;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ExempleCollocation", mappedBy="collocation", orphanRemoval=true)
     */
    private $exempleCollocations;

    /**
     * @ORM\OneToMany(targetEntity=LexicalEntryTrad::class, mappedBy="collocation")
     */
    private $lexicalEntryTrads;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $complement;

    /**
     * @ORM\ManyToMany(targetEntity=Fonction::class, inversedBy="collocations")
     */
    private $fonction;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $regroupement;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $patronParadigme;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $patronFixe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $partieDiscours;

    public function __construct()
    {
        $this->exempleCollocations = new ArrayCollection();
        $this->lexicalEntryTrads = new ArrayCollection();
        $this->fonction = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStructureGlobale(): ?string
    {
        return $this->structureGlobale;
    }

    public function setStructureGlobale(string $structureGlobale): self
    {
        $this->structureGlobale = $structureGlobale;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getPropSyntax(): ?string
    {
        return $this->propSyntax;
    }

    public function setPropSyntax(?string $propSyntax): self
    {
        $this->propSyntax = $propSyntax;

        return $this;
    }

    public function getStructureColloc(): ?string
    {
        return $this->structureColloc;
    }

    public function setStructureColloc(?string $structureColloc): self
    {
        $this->structureColloc = $structureColloc;

        return $this;
    }

    public function getLemmaBase(): ?string
    {
        return $this->lemmaBase;
    }

    public function setLemmaBase(string $lemmaBase): self
    {
        $this->lemmaBase = $lemmaBase;

        return $this;
    }

    public function getLexicalEntryBase(): ?LexicalEntry
    {
        return $this->lexicalEntryBase;
    }

    public function setLexicalEntryBase(?LexicalEntry $lexicalEntryBase): self
    {
        $this->lexicalEntryBase = $lexicalEntryBase;

        return $this;
    }

    public function getLemmaCollocatif(): ?string
    {
        return $this->lemmaCollocatif;
    }

    public function setLemmaCollocatif(string $lemmaCollocatif): self
    {
        $this->lemmaCollocatif = $lemmaCollocatif;

        return $this;
    }

    public function getLexicalEntryCollocatif(): ?LexicalEntry
    {
        return $this->lexicalEntryCollocatif;
    }

    public function setLexicalEntryCollocatif(?LexicalEntry $lexicalEntryCollocatif): self
    {
        $this->lexicalEntryCollocatif = $lexicalEntryCollocatif;

        return $this;
    }

    public function getFonctionLexicale(): ?string
    {
        return $this->fonctionLexicale;
    }

    public function setFonctionLexicale(?string $fonctionLexicale): self
    {
        $this->fonctionLexicale = $fonctionLexicale;

        return $this;
    }

    public function getNbOccurrences(): ?int
    {
        return $this->nbOccurrences;
    }

    public function setNbOccurrences(?int $nbOccurrences): self
    {
        $this->nbOccurrences = $nbOccurrences;

        return $this;
    }


    public function getLogLike(): ?float
    {
        return $this->logLike;
    }

    public function setLogLike(?float $logLike): self
    {
        $this->logLike = $logLike;

        return $this;
    }

    public function getFrequence(): ?string
    {
        return $this->frequence;
    }

    public function setFrequence(?string $frequence): self
    {
        $this->frequence = $frequence;

        return $this;
    }

    public function getNbDisciplines(): ?int
    {
        return $this->nbDisciplines;
    }

    public function setNbDisciplines(?int $nbDisciplines): self
    {
        $this->nbDisciplines = $nbDisciplines;

        return $this;
    }

    /**
     * @return Collection|ExempleCollocation[]
     */
    public function getExempleCollocations(): Collection
    {
        return $this->exempleCollocations;
    }

    public function addExempleCollocation(ExempleCollocation $exempleCollocation): self
    {
        if (!$this->exempleCollocations->contains($exempleCollocation)) {
            $this->exempleCollocations[] = $exempleCollocation;
            $exempleCollocation->setCollocation($this);
        }

        return $this;
    }

    public function removeExempleCollocation(ExempleCollocation $exempleCollocation): self
    {
        if ($this->exempleCollocations->contains($exempleCollocation)) {
            $this->exempleCollocations->removeElement($exempleCollocation);
            // set the owning side to null (unless already changed)
            if ($exempleCollocation->getCollocation() === $this) {
                $exempleCollocation->setCollocation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, LexicalEntryTrad>
     */
    public function getLexicalEntryTrads(): Collection
    {
        return $this->lexicalEntryTrads;
    }

    public function addLexicalEntryTrad(LexicalEntryTrad $lexicalEntryTrad): self
    {
        if (!$this->lexicalEntryTrads->contains($lexicalEntryTrad)) {
            $this->lexicalEntryTrads[] = $lexicalEntryTrad;
            $lexicalEntryTrad->setCollocation($this);
        }

        return $this;
    }

    public function removeLexicalEntryTrad(LexicalEntryTrad $lexicalEntryTrad): self
    {
        if ($this->lexicalEntryTrads->removeElement($lexicalEntryTrad)) {
            // set the owning side to null (unless already changed)
            if ($lexicalEntryTrad->getCollocation() === $this) {
                $lexicalEntryTrad->setCollocation(null);
            }
        }

        return $this;
    }

    public function getComplement(): ?string
    {
        return $this->complement;
    }

    public function setComplement(?string $complement): self
    {
        $this->complement = $complement;

        return $this;
    }

    /**
     * @return Collection<int, Fonction>
     */
    public function getFonction(): Collection
    {
        return $this->fonction;
    }

    public function addFonction(Fonction $fonction): self
    {
        if (!$this->fonction->contains($fonction)) {
            $this->fonction[] = $fonction;
        }

        return $this;
    }

    public function removeFonction(Fonction $fonction): self
    {
        $this->fonction->removeElement($fonction);

        return $this;
    }

    public function getRegroupement(): ?string
    {
        return $this->regroupement;
    }

    public function setRegroupement(?string $regroupement): self
    {
        $this->regroupement = $regroupement;

        return $this;
    }

    public function getPatronParadigme(): ?string
    {
        return $this->patronParadigme;
    }

    public function setPatronParadigme(?string $patronParadigme): self
    {
        $this->patronParadigme = $patronParadigme;

        return $this;
    }

    public function getPatronFixe(): ?string
    {
        return $this->patronFixe;
    }

    public function setPatronFixe(?string $patronFixe): self
    {
        $this->patronFixe = $patronFixe;

        return $this;
    }

    public function getPartieDiscours(): ?string
    {
        return $this->partieDiscours;
    }

    public function setPartieDiscours(?string $partieDiscours): self
    {
        $this->partieDiscours = $partieDiscours;

        return $this;
    }
}
