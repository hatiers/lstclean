<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExempleRepository")
 */
class Exemple
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\LexicalEntry", inversedBy="exemples")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lexicalEntry;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pivot;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $partieTextuelle;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $contexteGauche;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $contexteDroit;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SourceExemple", inversedBy="exempleLexicalEntry")
     * @ORM\JoinColumn(nullable=true)
     */
    private $sourceExemple;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLexicalEntry(): ?LexicalEntry
    {
        return $this->lexicalEntry;
    }

    public function setLexicalEntry(?LexicalEntry $lexicalEntry): self
    {
        $this->lexicalEntry = $lexicalEntry;

        return $this;
    }


    public function getPivot(): ?string
    {
        return $this->pivot;
    }

    public function setPivot(string $pivot): self
    {
        $this->pivot = $pivot;

        return $this;
    }

    public function getPartieTextuelle(): ?string
    {
        return $this->partieTextuelle;
    }

    public function setPartieTextuelle(string $partieTextuelle): self
    {
        $this->partieTextuelle = $partieTextuelle;

        return $this;
    }

    public function getContexteGauche(): ?string
    {
        return $this->contexteGauche;
    }

    public function setContexteGauche(?string $contexteGauche): self
    {
        $this->contexteGauche = $contexteGauche;

        return $this;
    }

    public function getContexteDroit(): ?string
    {
        return $this->contexteDroit;
    }

    public function setContexteDroit(?string $contexteDroit): self
    {
        $this->contexteDroit = $contexteDroit;

        return $this;
    }

    public function getSourceExemple(): ?SourceExemple
    {
        return $this->sourceExemple;
    }

    public function setSourceExemple(?SourceExemple $sourceExemple): self
    {
        $this->sourceExemple = $sourceExemple;

        return $this;
    }
}
