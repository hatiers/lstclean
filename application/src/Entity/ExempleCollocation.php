<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExempleCollocationRepository")
 */
class ExempleCollocation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $structure;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Collocation", inversedBy="exempleCollocations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $collocation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $contexteGauche;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $contexteDroit;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pivot;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $partieTextuelle;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SourceExemple", inversedBy="exempleCollocations")
     * @ORM\JoinColumn(nullable=true)
     */
    private $sourceExemple;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStructure(): ?string
    {
        return $this->structure;
    }

    public function setStructure(?string $structure): self
    {
        $this->structure = $structure;

        return $this;
    }

    public function getCollocation(): ?Collocation
    {
        return $this->collocation;
    }

    public function setCollocation(?Collocation $collocation): self
    {
        $this->collocation = $collocation;

        return $this;
    }

    public function getContexteGauche(): ?string
    {
        return $this->contexteGauche;
    }

    public function setContexteGauche(?string $contexteGauche): self
    {
        $this->contexteGauche = $contexteGauche;

        return $this;
    }

    public function getContexteDroit(): ?string
    {
        return $this->contexteDroit;
    }

    public function setContexteDroit(?string $contexteDroit): self
    {
        $this->contexteDroit = $contexteDroit;

        return $this;
    }

    public function getPivot(): ?string
    {
        return $this->pivot;
    }

    public function setPivot(string $pivot): self
    {
        $this->pivot = $pivot;

        return $this;
    }

    public function getPartieTextuelle(): ?string
    {
        return $this->partieTextuelle;
    }

    public function setPartieTextuelle(string $partieTextuelle): self
    {
        $this->partieTextuelle = $partieTextuelle;

        return $this;
    }

    public function getSourceExemple(): ?SourceExemple
    {
        return $this->sourceExemple;
    }

    public function setSourceExemple(?SourceExemple $sourceExemple): self
    {
        $this->sourceExemple = $sourceExemple;

        return $this;
    }
}
