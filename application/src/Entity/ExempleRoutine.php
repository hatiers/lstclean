<?php

namespace App\Entity;

use App\Repository\ExempleRoutineRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExempleRoutineRepository::class)
 */
class ExempleRoutine
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Routine::class, inversedBy="exemples")
     * @ORM\JoinColumn(nullable=false)
     */
    private $routine;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity=SourceExemple::class)
     */
    private $source;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $simplicity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoutine(): ?Routine
    {
        return $this->routine;
    }

    public function setRoutine(?Routine $routine): self
    {
        $this->routine = $routine;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getSource(): ?SourceExemple
    {
        return $this->source;
    }

    public function setSource(?SourceExemple $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getSimplicity(): ?string
    {
        return $this->simplicity;
    }

    public function setSimplicity(?string $simplicity): self
    {
        $this->simplicity = $simplicity;

        return $this;
    }
}
