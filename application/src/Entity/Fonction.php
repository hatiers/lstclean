<?php

namespace App\Entity;

use App\Repository\FonctionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FonctionRepository::class)
 */
class Fonction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $generalf;

    /**
     * @ORM\ManyToMany(targetEntity=Collocation::class, mappedBy="fonction")
     */
    private $collocations;

    /**
     * @ORM\OneToMany(targetEntity=LexicalEntry::class, mappedBy="fonction")
     */
    private $lexicalEntries;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $exemple;

    /**
     * @ORM\OneToMany(targetEntity=Routine::class, mappedBy="fonction", orphanRemoval=true)
     */
    private $routines;

    public function __construct()
    {
        $this->collocations = new ArrayCollection();
        $this->lexicalEntries = new ArrayCollection();
        $this->routines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getGeneralf(): ?string
    {
        return $this->generalf;
    }

    public function setGeneralf(string $generalf): self
    {
        $this->generalf = $generalf;

        return $this;
    }

    /**
     * @return Collection<int, Collocation>
     */
    public function getCollocations(): Collection
    {
        return $this->collocations;
    }

    public function addCollocation(Collocation $collocation): self
    {
        if (!$this->collocations->contains($collocation)) {
            $this->collocations[] = $collocation;
            $collocation->addFonction($this);
        }

        return $this;
    }

    public function removeCollocation(Collocation $collocation): self
    {
        if ($this->collocations->removeElement($collocation)) {
            $collocation->removeFonction($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, LexicalEntry>
     */
    public function getLexicalEntries(): Collection
    {
        return $this->lexicalEntries;
    }

    public function addLexicalEntry(LexicalEntry $lexicalEntry): self
    {
        if (!$this->lexicalEntries->contains($lexicalEntry)) {
            $this->lexicalEntries[] = $lexicalEntry;
            $lexicalEntry->setFonction($this);
        }

        return $this;
    }

    public function removeLexicalEntry(LexicalEntry $lexicalEntry): self
    {
        if ($this->lexicalEntries->removeElement($lexicalEntry)) {
            // set the owning side to null (unless already changed)
            if ($lexicalEntry->getFonction() === $this) {
                $lexicalEntry->setFonction(null);
            }
        }

        return $this;
    }

    public function getExemple(): ?string
    {
        return $this->exemple;
    }

    public function setExemple(?string $exemple): self
    {
        $this->exemple = $exemple;

        return $this;
    }

    /**
     * @return Collection<int, Routine>
     */
    public function getRoutines(): Collection
    {
        return $this->routines;
    }

    public function addRoutine(Routine $routine): self
    {
        if (!$this->routines->contains($routine)) {
            $this->routines[] = $routine;
            $routine->setFonction($this);
        }

        return $this;
    }

    public function removeRoutine(Routine $routine): self
    {
        if ($this->routines->removeElement($routine)) {
            // set the owning side to null (unless already changed)
            if ($routine->getFonction() === $this) {
                $routine->setFonction(null);
            }
        }

        return $this;
    }
}
