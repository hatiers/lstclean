<?php

namespace App\Entity;

use App\Repository\LanguageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LanguageRepository::class)
 */
class Language
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $shortName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $translatedName;

    /**
     * @ORM\OneToMany(targetEntity=LemmaTrad::class, mappedBy="language")
     */
    private $lemmaTrads;

    public function __construct()
    {
        $this->lemmaTrads = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    public function setShortName(string $shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }

    public function getTranslatedName(): ?string
    {
        return $this->translatedName;
    }

    public function setTranslatedName(string $translatedName): self
    {
        $this->translatedName = $translatedName;

        return $this;
    }

    /**
     * @return Collection<int, LemmaTrad>
     */
    public function getLemmaTrads(): Collection
    {
        return $this->lemmaTrads;
    }

    public function addLemmaTrad(LemmaTrad $lemmaTrad): self
    {
        if (!$this->lemmaTrads->contains($lemmaTrad)) {
            $this->lemmaTrads[] = $lemmaTrad;
            $lemmaTrad->setLanguage($this);
        }

        return $this;
    }

    public function removeLemmaTrad(LemmaTrad $lemmaTrad): self
    {
        if ($this->lemmaTrads->removeElement($lemmaTrad)) {
            // set the owning side to null (unless already changed)
            if ($lemmaTrad->getLanguage() === $this) {
                $lemmaTrad->setLanguage(null);
            }
        }

        return $this;
    }
}
