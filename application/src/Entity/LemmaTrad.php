<?php

namespace App\Entity;

use App\Repository\LemmaTradRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LemmaTradRepository::class)
 */
class LemmaTrad
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity=Language::class, inversedBy="lemmaTrads")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    /**
     * @ORM\OneToMany(targetEntity=LexicalEntryTrad::class, mappedBy="lemmaTrad", orphanRemoval=true)
     */
    private $lexicalEntryTrads;

    public function __construct()
    {
        $this->lexicalEntryTrads = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return Collection<int, LexicalEntryTrad>
     */
    public function getLexicalEntryTrads(): Collection
    {
        return $this->lexicalEntryTrads;
    }

    public function addLexicalEntryTrad(LexicalEntryTrad $lexicalEntryTrad): self
    {
        if (!$this->lexicalEntryTrads->contains($lexicalEntryTrad)) {
            $this->lexicalEntryTrads[] = $lexicalEntryTrad;
            $lexicalEntryTrad->setLemmaTrad($this);
        }

        return $this;
    }

    public function removeLexicalEntryTrad(LexicalEntryTrad $lexicalEntryTrad): self
    {
        if ($this->lexicalEntryTrads->removeElement($lexicalEntryTrad)) {
            // set the owning side to null (unless already changed)
            if ($lexicalEntryTrad->getLemmaTrad() === $this) {
                $lexicalEntryTrad->setLemmaTrad(null);
            }
        }

        return $this;
    }
}
