<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;


// /**
//  * @ORM\ManyToOne(targetEntity="App\Entity\Lemma", inversedBy="lexicalEntries", cascade={"remove"}, fetch="EAGER")
//  * @MaxDepth(1)
//  */


/**
 * @ORM\Entity(repositoryClass="App\Repository\LexicalEntryRepository")
 */
class LexicalEntry
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $meaning = "";


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SemanticClass", inversedBy="lexicalEntries")
     */
    private $semanticClass;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SemanticSubClass", inversedBy="lexicalEntries")
     */
    private $semanticSubClass;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $acceptionUniq;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SyntacCat", inversedBy="lexicalEntries", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\LexicalType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lexicalType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $propMorph;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $varGraph;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $propSynt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Exemple", mappedBy="lexicalEntry", orphanRemoval=true)
     */
    private $exemples;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Collocation", mappedBy="lexicalEntryBase", orphanRemoval=true)
     */
    private $collocationsBase;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Collocation", mappedBy="lexicalEntryCollocatif", orphanRemoval=true)
     */
    private $collocationsCollocatif;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Source", inversedBy="lexicalEntries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $source;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Metriques", inversedBy="lexicalEntries", fetch="EAGER")
     */
    private $metriques;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SemanticClassSubClass", inversedBy="lexicalEntries")
     */
    private $semanticClassSubClass;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $verbConstruction;

    /**
     * @ORM\OneToMany(targetEntity=LexicalEntryTrad::class, mappedBy="lexicalEntry")
     */
    private $lexicalEntryTrads;

    /**
     * @ORM\ManyToOne(targetEntity=Lemma::class, inversedBy="lexicalEntries", fetch="EAGER", cascade={"remove", "persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $lemma;

    /**
     * @ORM\ManyToOne(targetEntity=Fonction::class, inversedBy="lexicalEntries")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $fonction;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $sujetComplement;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;


    public function __construct()
    {
        $this->exemples = new ArrayCollection();
        $this->collocationsBase = new ArrayCollection();
        $this->collocationsCollocatif = new ArrayCollection();
        $this->lexicalEntryTrads = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMeaning(): ?string
    {
        return $this->meaning;
    }

    public function setMeaning(string $meaning): self
    {
        $this->meaning = $meaning;

        return $this;
    }


    public function getSemanticClass(): ?SemanticClass
    {
        return $this->semanticClass;
    }

    public function setSemanticClass(?SemanticClass $semanticClass): self
    {
        $this->semanticClass = $semanticClass;

        return $this;
    }

    public function getSemanticSubClass(): ?SemanticSubClass
    {
        return $this->semanticSubClass;
    }

    public function setSemanticSubClass(?SemanticSubClass $semanticSubClass): self
    {
        $this->semanticSubClass = $semanticSubClass;

        return $this;
    }

    public function getAcceptionUniq(): ?string
    {
        return $this->acceptionUniq;
    }

    public function setAcceptionUniq(string $acceptionUniq): self
    {
        $this->acceptionUniq = $acceptionUniq;

        return $this;
    }

    public function getCat(): ?SyntacCat
    {
        return $this->cat;
    }

    public function setCat(?SyntacCat $cat): self
    {
        $this->cat = $cat;

        return $this;
    }

    public function getLexicalType(): ?LexicalType
    {
        return $this->lexicalType;
    }

    public function setLexicalType(?LexicalType $lexicalType): self
    {
        $this->lexicalType = $lexicalType;

        return $this;
    }

    public function getPropMorph(): ?string
    {
        return $this->propMorph;
    }

    public function setPropMorph(?string $propMorph): self
    {
        $this->propMorph = $propMorph;

        return $this;
    }

    public function getVarGraph(): ?string
    {
        return $this->varGraph;
    }

    public function setVarGraph(?string $varGraph): self
    {
        $this->varGraph = $varGraph;

        return $this;
    }

    public function getPropSynt(): ?string
    {
        return $this->propSynt;
    }

    public function setPropSynt(?string $propSynt): self
    {
        $this->propSynt = $propSynt;

        return $this;
    }

    /**
     * @return Collection|Exemple[]
     */
    public function getExemples(): Collection
    {
        return $this->exemples;
    }

    public function addExemple(Exemple $exemple): self
    {
        if (!$this->exemples->contains($exemple)) {
            $this->exemples[] = $exemple;
            $exemple->setLexicalEntry($this);
        }

        return $this;
    }

    public function removeExemple(Exemple $exemple): self
    {
        if ($this->exemples->contains($exemple)) {
            $this->exemples->removeElement($exemple);
            // set the owning side to null (unless already changed)
            if ($exemple->getLexicalEntry() === $this) {
                $exemple->setLexicalEntry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Collocation[]
     */
    public function getCollocationsBase(): Collection
    {
        return $this->collocationsBase;
    }

    public function addCollocationsBase(Collocation $collocationsBase): self
    {
        if (!$this->collocationsBase->contains($collocationsBase)) {
            $this->collocationsBase[] = $collocationsBase;
            $collocationsBase->setLexicalEntryBase($this);
        }

        return $this;
    }

    public function removeCollocationsBase(Collocation $collocationsBase): self
    {
        if ($this->collocationsBase->contains($collocationsBase)) {
            $this->collocationsBase->removeElement($collocationsBase);
            // set the owning side to null (unless already changed)
            if ($collocationsBase->getLexicalEntryBase() === $this) {
                $collocationsBase->setLexicalEntryBase(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Collocation[]
     */
    public function getCollocationsCollocatif(): Collection
    {
        return $this->collocationsCollocatif;
    }

    public function addCollocationsCollocatif(Collocation $collocationsCollocatif): self
    {
        if (!$this->collocationsCollocatif->contains($collocationsCollocatif)) {
            $this->collocationsCollocatif[] = $collocationsCollocatif;
            $collocationsCollocatif->setLexicalEntryCollocatif($this);
        }

        return $this;
    }

    public function removeCollocationsCollocatif(Collocation $collocationsCollocatif): self
    {
        if ($this->collocationsCollocatif->contains($collocationsCollocatif)) {
            $this->collocationsCollocatif->removeElement($collocationsCollocatif);
            // set the owning side to null (unless already changed)
            if ($collocationsCollocatif->getLexicalEntryCollocatif() === $this) {
                $collocationsCollocatif->setLexicalEntryCollocatif(null);
            }
        }

        return $this;
    }

    public function getSource(): ?Source
    {
        return $this->source;
    }

    public function setSource(?Source $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getMetriques(): ?Metriques
    {
        return $this->metriques;
    }

    public function setMetriques(?Metriques $metriques): self
    {
        $this->metriques = $metriques;

        return $this;
    }

    public function getSemanticClassSubClass(): ?SemanticClassSubClass
    {
        return $this->semanticClassSubClass;
    }

    public function setSemanticClassSubClass(?SemanticClassSubClass $semanticClassSubClass): self
    {
        $this->semanticClassSubClass = $semanticClassSubClass;

        return $this;
    }

    public function getVerbConstruction(): ?string
    {
        return $this->verbConstruction;
    }

    public function setVerbConstruction(?string $verbConstruction): self
    {
        $this->verbConstruction = $verbConstruction;

        return $this;
    }

    /**
     * @return Collection<int, LexicalEntryTrad>
     */
    public function getLexicalEntryTrads(): Collection
    {
        return $this->lexicalEntryTrads;
    }

    public function addLexicalEntryTrad(LexicalEntryTrad $lexicalEntryTrad): self
    {
        if (!$this->lexicalEntryTrads->contains($lexicalEntryTrad)) {
            $this->lexicalEntryTrads[] = $lexicalEntryTrad;
            $lexicalEntryTrad->setLexicalEntry($this);
        }

        return $this;
    }

    public function removeLexicalEntryTrad(LexicalEntryTrad $lexicalEntryTrad): self
    {
        if ($this->lexicalEntryTrads->removeElement($lexicalEntryTrad)) {
            // set the owning side to null (unless already changed)
            if ($lexicalEntryTrad->getLexicalEntry() === $this) {
                $lexicalEntryTrad->setLexicalEntry(null);
            }
        }

        return $this;
    }

    public function getLemma(): ?Lemma
    {
        return $this->lemma;
    }

    public function setLemma(?Lemma $lemma): self
    {
        $this->lemma = $lemma;

        return $this;
    }

    public function getFonction(): ?Fonction
    {
        return $this->fonction;
    }

    public function setFonction(?Fonction $fonction): self
    {
        $this->fonction = $fonction;

        return $this;
    }

    public function getSujetComplement(): ?string
    {
        return $this->sujetComplement;
    }

    public function setSujetComplement(?string $sujetComplement): self
    {
        $this->sujetComplement = $sujetComplement;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}
