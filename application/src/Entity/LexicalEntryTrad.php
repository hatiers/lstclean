<?php

namespace App\Entity;

use App\Repository\LexicalEntryTradRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LexicalEntryTradRepository::class)
 */
class LexicalEntryTrad
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbOcc;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $synonyms;

    /**
     * @ORM\Column(type="text")
     */
    private $exampleSource;

    /**
     * @ORM\Column(type="text")
     */
    private $exampleCible;


    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity=LemmaTrad::class, inversedBy="lexicalEntryTrads")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $lemmaTrad;

    /**
     * @ORM\ManyToOne(targetEntity=LexicalEntry::class, inversedBy="lexicalEntryTrads")
     */
    private $lexicalEntry;

    /**
     * @ORM\ManyToOne(targetEntity=Collocation::class, inversedBy="lexicalEntryTrads")
     */
    private $collocation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $usageGeneral;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $usageSpecific;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $context;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getNbOcc(): ?int
    {
        return $this->nbOcc;
    }

    public function setNbOcc(?int $nbOcc): self
    {
        $this->nbOcc = $nbOcc;

        return $this;
    }

    public function getSynonyms(): ?string
    {
        return $this->synonyms;
    }

    public function setSynonyms(?string $synonyms): self
    {
        $this->synonyms = $synonyms;

        return $this;
    }

    public function getExampleSource(): ?string
    {
        return $this->exampleSource;
    }

    public function setExampleSource(string $exampleSource): self
    {
        $this->exampleSource = $exampleSource;

        return $this;
    }

    public function getExampleCible(): ?string
    {
        return $this->exampleCible;
    }

    public function setExampleCible(string $exampleCible): self
    {
        $this->exampleCible = $exampleCible;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getLemmaTrad(): ?LemmaTrad
    {
        return $this->lemmaTrad;
    }

    public function setLemmaTrad(?LemmaTrad $lemmaTrad): self
    {
        $this->lemmaTrad = $lemmaTrad;

        return $this;
    }

    public function getLexicalEntry(): ?LexicalEntry
    {
        return $this->lexicalEntry;
    }

    public function setLexicalEntry(?LexicalEntry $lexicalEntry): self
    {
        $this->lexicalEntry = $lexicalEntry;

        return $this;
    }

    public function getCollocation(): ?Collocation
    {
        return $this->collocation;
    }

    public function setCollocation(?Collocation $collocation): self
    {
        $this->collocation = $collocation;

        return $this;
    }

    public function getUsageGeneral(): ?string
    {
        return $this->usageGeneral;
    }

    public function setUsageGeneral(?string $usageGeneral): self
    {
        $this->usageGeneral = $usageGeneral;

        return $this;
    }

    public function getUsageSpecific(): ?string
    {
        return $this->usageSpecific;
    }

    public function setUsageSpecific(?string $usageSpecific): self
    {
        $this->usageSpecific = $usageSpecific;

        return $this;
    }

    public function getContext(): ?string
    {
        return $this->context;
    }

    public function setContext(?string $context): self
    {
        $this->context = $context;

        return $this;
    }
}
