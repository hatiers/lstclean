<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MetriquesRepository")
 */
class Metriques
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;



    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $ratio;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $ccbym;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $cabym;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cafreq;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ngrammes;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $repartitionDisciplines;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $repartitionTranches;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LexicalEntry", mappedBy="metriques", orphanRemoval=true)
     */
    private $lexicalEntries;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $disciplinesSpec;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $freqAnthropo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $freqEco;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $freqGeo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $freqHistoire;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $freqLing;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $freqPsycho;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $freqSciedu;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $freqSciepo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $freqScinfo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $freqSocio;

    public function __construct()
    {
        $this->lexicalEntries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getRatio(): ?float
    {
        return $this->ratio;
    }

    public function setRatio(?float $ratio): self
    {
        $this->ratio = $ratio;

        return $this;
    }

    public function getCcbym(): ?float
    {
        return $this->ccbym;
    }

    public function setCcbym(?float $ccbym): self
    {
        $this->ccbym = $ccbym;

        return $this;
    }

    public function getCabym(): ?float
    {
        return $this->cabym;
    }

    public function setCabym(?float $cabym): self
    {
        $this->cabym = $cabym;

        return $this;
    }

    public function getCafreq(): ?int
    {
        return $this->cafreq;
    }

    public function setCafreq(?int $cafreq): self
    {
        $this->cafreq = $cafreq;

        return $this;
    }

    public function getNgrammes(): ?int
    {
        return $this->ngrammes;
    }

    public function setNgrammes(?int $ngrammes): self
    {
        $this->ngrammes = $ngrammes;

        return $this;
    }

    public function getRepartitionDisciplines(): ?int
    {
        return $this->repartitionDisciplines;
    }

    public function setRepartitionDisciplines(?int $repartitionDisciplines): self
    {
        $this->repartitionDisciplines = $repartitionDisciplines;

        return $this;
    }

    public function getRepartitionTranches(): ?int
    {
        return $this->repartitionTranches;
    }

    public function setRepartitionTranches(?int $repartitionTranches): self
    {
        $this->repartitionTranches = $repartitionTranches;

        return $this;
    }

    /**
     * @return Collection|LexicalEntry[]
     */
    public function getLexicalEntries(): Collection
    {
        return $this->lexicalEntries;
    }

    public function addLexicalEntry(LexicalEntry $lexicalEntry): self
    {
        if (!$this->lexicalEntries->contains($lexicalEntry)) {
            $this->lexicalEntries[] = $lexicalEntry;
            $lexicalEntry->setMetriques($this);
        }

        return $this;
    }

    public function removeLexicalEntry(LexicalEntry $lexicalEntry): self
    {
        if ($this->lexicalEntries->contains($lexicalEntry)) {
            $this->lexicalEntries->removeElement($lexicalEntry);
            // set the owning side to null (unless already changed)
            if ($lexicalEntry->getMetriques() === $this) {
                $lexicalEntry->setMetriques(null);
            }
        }

        return $this;
    }

    public function getDisciplinesSpec(): ?int
    {
        return $this->disciplinesSpec;
    }

    public function setDisciplinesSpec(?int $disciplinesSpec): self
    {
        $this->disciplinesSpec = $disciplinesSpec;

        return $this;
    }

    public function getFreqAnthropo(): ?int
    {
        return $this->freqAnthropo;
    }

    public function setFreqAnthropo(?int $freqAnthropo): self
    {
        $this->freqAnthropo = $freqAnthropo;

        return $this;
    }

    public function getFreqEco(): ?int
    {
        return $this->freqEco;
    }

    public function setFreqEco(?int $freqEco): self
    {
        $this->freqEco = $freqEco;

        return $this;
    }

    public function getFreqGeo(): ?int
    {
        return $this->freqGeo;
    }

    public function setFreqGeo(?int $freqGeo): self
    {
        $this->freqGeo = $freqGeo;

        return $this;
    }

    public function getFreqHistoire(): ?int
    {
        return $this->freqHistoire;
    }

    public function setFreqHistoire(?int $freqHistoire): self
    {
        $this->freqHistoire = $freqHistoire;

        return $this;
    }

    public function getFreqLing(): ?int
    {
        return $this->freqLing;
    }

    public function setFreqLing(?int $freqLing): self
    {
        $this->freqLing = $freqLing;

        return $this;
    }

    public function getFreqPsycho(): ?int
    {
        return $this->freqPsycho;
    }

    public function setFreqPsycho(?int $freqPsycho): self
    {
        $this->freqPsycho = $freqPsycho;

        return $this;
    }

    public function getFreqSciedu(): ?int
    {
        return $this->freqSciedu;
    }

    public function setFreqSciedu(?int $freqSciedu): self
    {
        $this->freqSciedu = $freqSciedu;

        return $this;
    }

    public function getFreqSciepo(): ?int
    {
        return $this->freqSciepo;
    }

    public function setFreqSciepo(?int $freqSciepo): self
    {
        $this->freqSciepo = $freqSciepo;

        return $this;
    }

    public function getFreqScinfo(): ?int
    {
        return $this->freqScinfo;
    }

    public function setFreqScinfo(?int $freqScinfo): self
    {
        $this->freqScinfo = $freqScinfo;

        return $this;
    }

    public function getFreqSocio(): ?int
    {
        return $this->freqSocio;
    }

    public function setFreqSocio(?int $freqSocio): self
    {
        $this->freqSocio = $freqSocio;

        return $this;
    }
}
