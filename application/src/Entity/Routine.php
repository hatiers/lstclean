<?php

namespace App\Entity;

use App\Repository\RoutineRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RoutineRepository::class)
 */
class Routine
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=Fonction::class, inversedBy="routines")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fonction;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subSubType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subType;

    /**
     * @ORM\Column(type="text")
     */
    private $modele;

    /**
     * @ORM\Column(type="text")
     */
    private $realisation;

    /**
     * @ORM\Column(type="integer")
     */
    private $occurrence;

    /**
     * @ORM\Column(type="text")
     */
    private $patron;

    /**
     * @ORM\OneToMany(targetEntity=ExempleRoutine::class, mappedBy="routine", orphanRemoval=true)
     */
    private $exemples;

    public function __construct()
    {
        $this->exemples = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getFonction(): ?Fonction
    {
        return $this->fonction;
    }

    public function setFonction(?Fonction $fonction): self
    {
        $this->fonction = $fonction;

        return $this;
    }

    public function getSubSubType(): ?string
    {
        return $this->subSubType;
    }

    public function setSubSubType(?string $subSubType): self
    {
        $this->subSubType = $subSubType;

        return $this;
    }

    public function getSubType(): ?string
    {
        return $this->subType;
    }

    public function setSubType(?string $subType): self
    {
        $this->subType = $subType;

        return $this;
    }

    public function getModele(): ?string
    {
        return $this->modele;
    }

    public function setModele(string $modele): self
    {
        $this->modele = $modele;

        return $this;
    }

    public function getRealisation(): ?string
    {
        return $this->realisation;
    }

    public function setRealisation(string $realisation): self
    {
        $this->realisation = $realisation;

        return $this;
    }

    public function getOccurrence(): ?int
    {
        return $this->occurrence;
    }

    public function setOccurrence(int $occurrence): self
    {
        $this->occurrence = $occurrence;

        return $this;
    }

    public function getPatron(): ?string
    {
        return $this->patron;
    }

    public function setPatron(string $patron): self
    {
        $this->patron = $patron;

        return $this;
    }

    /**
     * @return Collection<int, ExempleRoutine>
     */
    public function getExemples(): Collection
    {
        return $this->exemples;
    }

    public function addExemple(ExempleRoutine $exemple): self
    {
        if (!$this->exemples->contains($exemple)) {
            $this->exemples[] = $exemple;
            $exemple->setRoutine($this);
        }

        return $this;
    }

    public function removeExemple(ExempleRoutine $exemple): self
    {
        if ($this->exemples->removeElement($exemple)) {
            // set the owning side to null (unless already changed)
            if ($exemple->getRoutine() === $this) {
                $exemple->setRoutine(null);
            }
        }

        return $this;
    }
}
