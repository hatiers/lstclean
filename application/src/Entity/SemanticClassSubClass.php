<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SemanticClassSubClassRepository")
 */
class SemanticClassSubClass
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SemanticClass", inversedBy="semanticClassSubClasses",  fetch="EAGER"))
     * @ORM\JoinColumn(nullable=false)
     */
    private $semanticClass;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SemanticSubClass", inversedBy="semanticClassSubClasses",  fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $semanticSubClass;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionClass;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $testAppartClass;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionSubClass;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $testAppartSubClass;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SyntacCat", inversedBy="semanticClassSubClasses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $syntacCatClassSubClass;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LexicalEntry", mappedBy="semanticClassSubClass", fetch="EAGER")
     */
    private $lexicalEntries;

    public function __construct()
    {
        $this->lexicalEntries = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSemanticClass(): ?SemanticClass
    {
        return $this->semanticClass;
    }

    public function setSemanticClass(?SemanticClass $semanticClass): self
    {
        $this->semanticClass = $semanticClass;

        return $this;
    }

    public function getSemanticSubClass(): ?SemanticSubClass
    {
        return $this->semanticSubClass;
    }

    public function setSemanticSubClass(?SemanticSubClass $semanticSubClass): self
    {
        $this->semanticSubClass = $semanticSubClass;

        return $this;
    }

    public function getDescriptionClass(): ?string
    {
        return $this->descriptionClass;
    }

    public function setDescriptionClass(?string $descriptionClass): self
    {
        $this->descriptionClass = $descriptionClass;

        return $this;
    }

    public function getTestAppartClass(): ?string
    {
        return $this->testAppartClass;
    }

    public function setTestAppartClass(?string $testAppartClass): self
    {
        $this->testAppartClass = $testAppartClass;

        return $this;
    }

    public function getDescriptionSubClass(): ?string
    {
        return $this->descriptionSubClass;
    }

    public function setDescriptionSubClass(?string $descriptionSubClass): self
    {
        $this->descriptionSubClass = $descriptionSubClass;

        return $this;
    }

    public function getTestAppartSubClass(): ?string
    {
        return $this->testAppartSubClass;
    }

    public function setTestAppartSubClass(?string $testAppartSubClass): self
    {
        $this->testAppartSubClass = $testAppartSubClass;

        return $this;
    }

    public function getSyntacCatClassSubClass(): ?SyntacCat
    {
        return $this->syntacCatClassSubClass;
    }

    public function setSyntacCatClassSubClass(?SyntacCat $syntacCatClassSubClass): self
    {
        $this->syntacCatClassSubClass = $syntacCatClassSubClass;

        return $this;
    }

    /**
     * @return Collection|LexicalEntry[]
     */
    public function getLexicalEntries(): Collection
    {
        return $this->lexicalEntries;
    }

    public function addLexicalEntry(LexicalEntry $lexicalEntry): self
    {
        if (!$this->lexicalEntries->contains($lexicalEntry)) {
            $this->lexicalEntries[] = $lexicalEntry;
            $lexicalEntry->setSemanticClassSubClass($this);
        }

        return $this;
    }

    public function removeLexicalEntry(LexicalEntry $lexicalEntry): self
    {
        if ($this->lexicalEntries->contains($lexicalEntry)) {
            $this->lexicalEntries->removeElement($lexicalEntry);
            // set the owning side to null (unless already changed)
            if ($lexicalEntry->getSemanticClassSubClass() === $this) {
                $lexicalEntry->setSemanticClassSubClass(null);
            }
        }

        return $this;
    }
}
