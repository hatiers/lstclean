<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SourceRepository")
 */
class Source
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LexicalEntry", mappedBy="source", orphanRemoval=true)
     */
    private $lexicalEntries;

    public function __construct()
    {
        $this->lexicalEntries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return Collection|LexicalEntry[]
     */
    public function getLexicalEntries(): Collection
    {
        return $this->lexicalEntries;
    }

    public function addLexicalEntry(LexicalEntry $lexicalEntry): self
    {
        if (!$this->lexicalEntries->contains($lexicalEntry)) {
            $this->lexicalEntries[] = $lexicalEntry;
            $lexicalEntry->setSource($this);
        }

        return $this;
    }

    public function removeLexicalEntry(LexicalEntry $lexicalEntry): self
    {
        if ($this->lexicalEntries->contains($lexicalEntry)) {
            $this->lexicalEntries->removeElement($lexicalEntry);
            // set the owning side to null (unless already changed)
            if ($lexicalEntry->getSource() === $this) {
                $lexicalEntry->setSource(null);
            }
        }

        return $this;
    }
}
