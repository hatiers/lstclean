<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SourceExempleRepository")
 */
class SourceExemple
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Exemple", mappedBy="sourceExemple", orphanRemoval=true)
     */
    private $exempleLexicalEntry;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ExempleCollocation", mappedBy="sourceExemple", orphanRemoval=true)
     */
    private $exempleCollocations;

    /**
     * @ORM\Column(type="text")
     */
    private $descriptionBiblio;

    public function __construct()
    {
        $this->exempleLexicalEntry = new ArrayCollection();
        $this->exempleCollocations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return Collection|Exemple[]
     */
    public function getExempleLexicalEntry(): Collection
    {
        return $this->exempleLexicalEntry;
    }

    public function addExempleLexicalEntry(Exemple $exempleLexicalEntry): self
    {
        if (!$this->exempleLexicalEntry->contains($exempleLexicalEntry)) {
            $this->exempleLexicalEntry[] = $exempleLexicalEntry;
            $exempleLexicalEntry->setSourceExemple($this);
        }

        return $this;
    }

    public function removeExempleLexicalEntry(Exemple $exempleLexicalEntry): self
    {
        if ($this->exempleLexicalEntry->contains($exempleLexicalEntry)) {
            $this->exempleLexicalEntry->removeElement($exempleLexicalEntry);
            // set the owning side to null (unless already changed)
            if ($exempleLexicalEntry->getSourceExemple() === $this) {
                $exempleLexicalEntry->setSourceExemple(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ExempleCollocation[]
     */
    public function getExempleCollocations(): Collection
    {
        return $this->exempleCollocations;
    }

    public function addExempleCollocation(ExempleCollocation $exempleCollocation): self
    {
        if (!$this->exempleCollocations->contains($exempleCollocation)) {
            $this->exempleCollocations[] = $exempleCollocation;
            $exempleCollocation->setSourceExemple($this);
        }

        return $this;
    }

    public function removeExempleCollocation(ExempleCollocation $exempleCollocation): self
    {
        if ($this->exempleCollocations->contains($exempleCollocation)) {
            $this->exempleCollocations->removeElement($exempleCollocation);
            // set the owning side to null (unless already changed)
            if ($exempleCollocation->getSourceExemple() === $this) {
                $exempleCollocation->setSourceExemple(null);
            }
        }

        return $this;
    }

    public function getDescriptionBiblio(): ?string
    {
        return $this->descriptionBiblio;
    }

    public function setDescriptionBiblio(string $descriptionBiblio): self
    {
        $this->descriptionBiblio = $descriptionBiblio;

        return $this;
    }
}
