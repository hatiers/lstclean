<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SyntacCatRepository")
 */
class SyntacCat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LexicalEntry", mappedBy="cat", orphanRemoval=true)
     */
    private $lexicalEntries;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SemanticClassSubClass", mappedBy="syntacCatClassSubClass")
     */
    private $semanticClassSubClasses;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $valueLong;

    public function __construct()
    {
        $this->lexicalEntries = new ArrayCollection();
        $this->semanticClassSubClasses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|LexicalEntry[]
     */
    public function getLexicalEntries(): Collection
    {
        return $this->lexicalEntries;
    }

    public function addLexicalEntry(LexicalEntry $lexicalEntry): self
    {
        if (!$this->lexicalEntries->contains($lexicalEntry)) {
            $this->lexicalEntries[] = $lexicalEntry;
            $lexicalEntry->setCat($this);
        }

        return $this;
    }

    public function removeLexicalEntry(LexicalEntry $lexicalEntry): self
    {
        if ($this->lexicalEntries->contains($lexicalEntry)) {
            $this->lexicalEntries->removeElement($lexicalEntry);
            // set the owning side to null (unless already changed)
            if ($lexicalEntry->getCat() === $this) {
                $lexicalEntry->setCat(null);
            }
        }

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return Collection|SemanticClassSubClass[]
     */
    public function getSemanticClassSubClasses(): Collection
    {
        return $this->semanticClassSubClasses;
    }

    public function addSemanticClassSubClass(SemanticClassSubClass $semanticClassSubClass): self
    {
        if (!$this->semanticClassSubClasses->contains($semanticClassSubClass)) {
            $this->semanticClassSubClasses[] = $semanticClassSubClass;
            $semanticClassSubClass->setSyntacCatClassSubClass($this);
        }

        return $this;
    }

    public function removeSemanticClassSubClass(SemanticClassSubClass $semanticClassSubClass): self
    {
        if ($this->semanticClassSubClasses->contains($semanticClassSubClass)) {
            $this->semanticClassSubClasses->removeElement($semanticClassSubClass);
            // set the owning side to null (unless already changed)
            if ($semanticClassSubClass->getSyntacCatClassSubClass() === $this) {
                $semanticClassSubClass->setSyntacCatClassSubClass(null);
            }
        }

        return $this;
    }

    public function getValueLong(): ?string
    {
        return $this->valueLong;
    }

    public function setValueLong(string $valueLong): self
    {
        $this->valueLong = $valueLong;

        return $this;
    }
}
