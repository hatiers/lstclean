<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ExportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
        ->add('output_format', ChoiceType::class, [
            'label' => "Format",
            'expanded' => true,
            'multiple' => false,
            'choices' => [ "xml" => "xml", "json" => "json" ],
            'required' => true,
        ])
        ->add('entries', CheckboxType::class, [
            'label'    => "Exporter les entrées lexicales",
            'required' => false,
        ])
        ->add('entries_examples', CheckboxType::class, [
            'label'    => "Ajouter les exemples des entrées lexicales",
            'required' => false,
        ])
        ->add('classes', CheckboxType::class, [
            'label'    => 'Exporter les classes sémantiques',
            'required' => false,
        ])
        ->add('collocations', CheckboxType::class, [
            'label'    => 'Exporter les collocations',
            'required' => false,
        ])
        ->add('collocations_examples', CheckboxType::class, [
            'label'    => 'Ajouter les exemples des collocations',
            'required' => false,
        ])
        ->add('translations', CheckboxType::class, [
            'label'    => 'Exporter les traductions',
            'required' => false,
        ])
        ->add('translations', CheckboxType::class, [
            'label'    => 'Exporter les traductions',
            'required' => false,
        ])
        ->add('translations_languages', ChoiceType::class, [
            'label' => "Filtrer les traductions par langues",
            'multiple' => true,
            'choices' => $options["data"],
            'data' => ['Toutes les langues' => 'all'], // par défaut, toutes les lanuges sont sélectionneées (et évite l'erreur d'une valeur nulle)
        ])
        ->add('fonctions', CheckboxType::class, [
            'label'    => 'Exporter les fonctions rhétoriques',
            'required' => false,
        ])
        ->add('routines', CheckboxType::class, [
            'label'    => 'Exporter les routines',
            'required' => false,
        ])
        ->add('routines_examples', CheckboxType::class, [
            'label'    => 'Ajouter les exemples des routines',
            'required' => false,
        ])
        ->add('examples_sources', CheckboxType::class, [
            'label'    => 'Exporter les sources des exemples',
            'required' => false,
        ])
        ->add('export_documentation', CheckboxType::class, [
            'label'    => "Ajouter la documentation du modèle à l'export (JSON seulement)",
            'required' => false,
        ])
        ->add('save', SubmitType::class, [
            'label' => 'Exporter',
        ]);
    }
}
