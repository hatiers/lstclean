<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ImportCSVType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('lexicalentries', FileType::class, [
            'label' => 'CSV Entrées'
          ])

          ->add('exemplesentries', FileType::class, [
            'label' => 'CSV Exemples Entrées'
          ])

          ->add('collocations', FileType::class, [
            'label' => 'CSV Collocation'
          ])
          ->add('exemplescollocations', FileType::class, [
            'label' => 'CSV Exemples Collocation'
          ])
          ->add('descriptionClass', FileType::class, [
            'label' => 'CSV Description Classification'
          ])
          ->add('descriptionBiblio', FileType::class, [
            'label' => 'CSV Description Bibliographie'
          ])
          ->add('metriques', FileType::class, [
            'label' => 'CSV Metriques'
          ])
          ->add('cleandb', CheckboxType::class, [
              'label'    => 'Vider la base',
              'required' => false,
          ])
          ->add('save', SubmitType::class, [
              'label' => 'importer',
          ]);
    }
}
