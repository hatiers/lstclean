<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ImportXMLType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('articlesXml', FileType::class, [
            'label' => 'Article(s) XML ',
            'multiple' => true,
            'attr' => ['accept' => 'application/xml'],
          ])
          ->add('save', SubmitType::class, [
              'label' => 'importer',
          ]);
    }
}
