<?php

namespace App\Manager;

use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;

class ArticleManager
{
    private $em;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->em = $entityManager;
    }

    public function importArticlesFromRep($path)
    {
        if ($dh = opendir($path)) {
            while (($file = readdir($dh)) !== false) {
                $extension =  substr(strrchr($file, '.'), 1);
                if ($extension == "xml") {
                    print $file . "\n";
                    $article = new Article();
                    $article->setName($file);
                    $article->setContent(file_get_contents($path . DIRECTORY_SEPARATOR . $file));
                    $this->em->persist($article);
                }
            }
            closedir($dh);
            $this->em->flush();
        }
        return;
    }

    public function importArticlesFromForm($content, $fileName)
    {
        $extension =  substr(strrchr($fileName, '.'), 1);
        if ($extension == "xml") {
            print $fileName . "\n";
            $article = new Article();
            $article->setName($fileName);
            $article->setContent($content);
            $this->em->persist($article);
            $this->em->flush();
        }
        return $article;
    }
}
