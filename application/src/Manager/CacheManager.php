<?php

namespace App\Manager;

use Symfony\Component\Cache\Adapter\ApcuAdapter;

class CacheManager
{
    private $cache;

    public function __construct()
    {
        $this->cache = new ApcuAdapter();
    }

    public function clear($key)
    {
        $this->cache->deleteItem($key);

        return;
    }

    public function get($key)
    {
        return $this->cache->getItem($key);
    }

    public function store($item, $value)
    {
        $item->set($value);
        $this->cache->save($item);

        return;
    }
}
