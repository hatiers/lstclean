<?php

namespace App\Manager;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Collocation;
use App\Entity\LexicalEntry;

class CollocationManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function parseCSVCollocation($lines)
    {
        $cpt = 0;
        foreach ($lines as $line) {
            if ($cpt !== 0) {
                $collocation = new Collocation();
                $tab = explode("\t", $line);
                $structureGlobale = $tab[0];
                $value = $tab[1];
                $propSyntax = $tab[2];
                $structureColloc = $tab[3];
                $lemmaBase = $tab[4];
                $lexicalEntryBase = $tab[5];
                $lemmaCollocatif = $tab[6];
                $lexicalEntryCollocatif = $tab[7];
                $fonctionLexicale = $tab[8];
                $nbOccurrences = $tab[9];
                $nbDisciplines = $tab[10];
                $logLike = $tab[11];
                $frequence = $tab[12];


                $collocation->setStructureGlobale($structureGlobale);
                $collocation->setValue($value);
                $collocation->setPropSyntax($propSyntax);
                $collocation->setStructureColloc($structureColloc);
                $collocation->setLemmaBase($lemmaBase);
                $collocation->setLemmaCollocatif($lemmaCollocatif);

                $currentLexicalEntry = $this->em->getRepository(LexicalEntry::class)->findOneByAcceptionUniq($lexicalEntryBase);
                if ($currentLexicalEntry) {
                    $collocation->setLexicalEntryBase($currentLexicalEntry);
                }
                $currentLexicalEntry = $this->em->getRepository(LexicalEntry::class)->findOneByAcceptionUniq($lexicalEntryCollocatif);
                if ($currentLexicalEntry) {
                    $collocation->setLexicalEntryCollocatif($currentLexicalEntry);
                }

                $collocation->setFonctionLexicale($fonctionLexicale);
                $collocation->setNbOccurrences((int)$nbOccurrences);
                $collocation->setNbDisciplines((int)$nbDisciplines);
                $collocation->setLogLike((float)$logLike);
                $collocation->setFrequence($frequence);


                $this->em->persist($collocation);
            }
            $cpt++;
        }
        $this->em->flush();
        return;
    }
    public function emptydb()
    {
        $collocations = $this->em->getRepository(Collocation::class)->findAll();
        foreach ($collocations as $collocation) {
            // code...
            $this->em->remove($collocation);
        }
        $this->em->flush();
    }

    //Vérification des données
    public function checkDatas()
    {
        $collocsIssue = [];
        return $collocsIssue;
    }
}
