<?php

namespace App\Manager;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\ExempleCollocation;
use App\Entity\Collocation;
use App\Entity\SourceExemple;

class ExempleCollocationManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function parseCSVExempleCollocation($lines)
    {
        $cpt = 0;
        foreach ($lines as $line) {
            if ($cpt !== 0) {
                $exempleCollocation = new ExempleCollocation();
                $tab = explode("\t", $line);
                $structure = $tab[0];
                $collocation = $tab[1];
                //$collocationBis=$tab[2];
                $contexteGauche = $tab[3];
                $pivot = $tab[4];
                $contexteDroit = $tab[5];
                $source = $tab[6];
                $partieTextuelle = $tab[7];

                $exempleCollocation->setStructure($structure);
                $exempleCollocation->setContexteGauche($contexteGauche);
                $exempleCollocation->setPivot($pivot);
                $exempleCollocation->setContexteDroit($contexteDroit);
                $exempleCollocation->setPartieTextuelle($partieTextuelle);

                //reprendre ici
                $currentCollocation = $this->em->getRepository(Collocation::class)->findOneByValue($collocation);
                $currentSourceExemple = $this->em->getRepository(SourceExemple::class)->findOneByValue($source);
                // echo("String à persister ".$collocation."  ".$source);
                // if ($currentSourceExemple && $currentCollocation) {
                if ($currentCollocation) {
                    $exempleCollocation->setCollocation($currentCollocation);
                    $exempleCollocation->setSourceExemple($currentSourceExemple);
                    $this->em->persist($exempleCollocation);
                }
            }
            $cpt++;
        }

        $this->em->flush();

        return;
    }

    public function emptydb()
    {
        $exemplesCollocations = $this->em->getRepository(ExempleCollocation::class)->findAll();
        foreach ($exemplesCollocations as $exempleCollocation) {
            // code...
            $this->em->remove($exempleCollocation);
        }
        $this->em->flush();
    }
}
