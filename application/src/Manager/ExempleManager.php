<?php

namespace App\Manager;

use App\Entity\Exemple;
use App\Entity\LexicalEntry;
use App\Entity\SourceExemple;
use Doctrine\ORM\EntityManagerInterface;

class ExempleManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function create(
        LexicalEntry $lexicalentry,
        SourceExemple $sourceExemple = null,
        $contexteG,
        $pivot,
        $contexteD,
        $partieTextuelle

    ) {
        $exemple = new Exemple();

        $exemple->setContexteGauche($contexteG);
        $exemple->setPivot($pivot);
        $exemple->setContexteDroit($contexteD);
        $exemple->setSourceExemple($sourceExemple);
        $exemple->setPartieTextuelle($partieTextuelle);
        $exemple->setLexicalEntry($lexicalentry);

        $this->em->persist($exemple);

        return;
    }
    public function parseCSVExempleLE($lines)
    {
        $cpt=0;
        foreach ($lines as $line) {
            if ($cpt !== 0) {
                $tab = explode("\t", $line);
                $acceptionValue=$tab[3];
                $contexteG=$tab[4];
                $pivot=$tab[5];
                $contexteD=$tab[6];
                $sourceExempleValue=$tab[7];
                $partieTextuelle=$tab[8];

                //récupération de l'objet lexical entry en fonction de sa propriété acceptionUniq
                $lexicalentry = $this->em->getRepository(LexicalEntry::class)->findOneByAcceptionUniq($acceptionValue);
                $sourceExemple= $this->em->getRepository(SourceExemple::class)->findOneByvalue($sourceExempleValue);

                if ($lexicalentry) {
                    if (!$sourceExemple) {
                        $this->create($lexicalentry, null, $contexteG, $pivot, $contexteD, $partieTextuelle);
                    } else {
                        $this->create($lexicalentry, $sourceExemple, $contexteG, $pivot, $contexteD, $partieTextuelle);
                    }
                    //persistance des données pour l'entité Exemple
                }
            }
            $cpt++;
        }
        $this->em->flush();

        return;
    }
}
