<?php

namespace App\Manager;

use App\Entity\Routine;
use App\Repository\SemanticClassSubClassRepository;
use Doctrine\ORM\EntityManagerInterface;
use \DOMDocument;

class ExportFunctions
{

  private $em;
  private $exportClassesRepository;

  public function __construct(EntityManagerInterface $em, SemanticClassSubClassRepository $exportSemanticClasses)
  {
    $this->em = $em;
    $this->exportClassesRepository = $exportSemanticClasses;
  }

  /* 
    Retourne un array hiérarchisé des classes et sous-classes sémantiques
    à partir des requêtes SQL natives programmées dans App\Repository\ExportSemanticClasses'
  */
  public function retrieveSemanticClasses()
  {
    $classes = $this->exportClassesRepository->ListAllClasses();
    $reconstructedClasses = [];

    foreach ($classes as $class) {
      $jsonDescr = json_decode($class['classDescription']);
      $class['classDescription'] = $jsonDescr;
      $subclassesList = explode(',', $class['subclasses']);
      $class["subclasses"] = [];
      foreach ($subclassesList as $subclassId) {
        //$subclass = $this->getDoctrine()->getRepository(SemanticClassSubClass::class)->describeSubclass($subclassId);
        $subclass = $this->exportClassesRepository->describeSubclass($subclassId);
        $subJsonDescr = json_decode($subclass['subclassDescription']);
        $subclass['subclassDescription'] = $subJsonDescr;
        $class["subclasses"][] = $subclass;
      }
      $reconstructedClasses[] = $class;
    }

    return $reconstructedClasses;
  }

  /*
    Retourne un array contenant les routines reformatées  
    en dépliant leurs patrons (ce sont de pseudos tableurs avec des séparateurs de lignes et de colonnes stockés en base)
  */
  public function reformatRoutines()
  {
    $routines = $this->em->getRepository(Routine::class)->findAll();

    $patronList = [];
    foreach ($routines as $routine) {
      $patronHeader = [];
      $patronData = [];
      $patron = $routine->getPatron();
      $participants = explode('----', $patron);
      foreach ($participants as $participant) {
        $t = explode('&', $participant);
        $patronHeader[] = $t[0];
        $patronData[] = $t[1];
      }
      $patronList[$routine->getId()]["header"] = $patronHeader;
      $patronList[$routine->getId()]["data"] = $patronData;
    }

    return ['routines' => $routines, 'patronList' => $patronList];
  }

  /*
    Corrige l'indentation d'une sortie XML en utilisant la classe PHP DOMDocument
   */
  public function xmlPrettify($xml)
  {
    libxml_use_internal_errors(true);
    $dom = new DOMDocument();
    $dom->preserveWhiteSpace = false;
    $dom->loadXML($xml, /*LIBXML_HTML_NOIMPLIED, */ LIBXML_NOBLANKS);
    $dom->formatOutput = true;
    $prettyXml = $dom->saveXML($dom->documentElement);

    return $prettyXml;
  }
}
