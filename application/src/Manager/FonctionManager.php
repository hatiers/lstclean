<?php

namespace App\Manager;

use App\Entity\Collocation;
use App\Entity\Fonction;
use App\Entity\LexicalEntry;
use Doctrine\ORM\EntityManagerInterface;

class FonctionManager
{
  private $em;

  public function __construct(EntityManagerInterface $em)
  {
    $this->em = $em;
  }

  public function parseCSV($lines)
  {
    $cpt = 0;
    foreach ($lines as $line) {
      $tab = explode("\t", $line);
      $generalf = trim($tab[0]);
      $label = trim($tab[1]);
      $exemple = (isset($tab[2])) ? trim($tab[2]) : null;

      $fonction = $this->em->getRepository(Fonction::class)->findOneBy(["label" => $label, "generalf" => $generalf]);

      if (!$fonction) {
        $fonction = new Fonction();
      }
      $fonction->setLabel($label);
      $fonction->setGeneralf($generalf);
      $fonction->setExemple($exemple);
      $this->em->persist($fonction);
    }
    $this->em->flush();

    return;
  }

  public function parseCSVAdverbs($lines)
  {
    $cpt = 0;
    foreach ($lines as $line) {
      if ($cpt !== 0) {
        $tab = explode("\t", $line);
        $acceptionUniq = $tab[0];
        $label = $tab[1];
        $generalf = $tab[2];
        $sujetComplement = $tab[3];
        $comment = $tab[4];

        // essayer récup lexical avec acceoptionUniq
        $lexicalEntry = $this->em->getRepository(LexicalEntry::class)->findOneByAcceptionUniq($acceptionUniq);
        if ($lexicalEntry) {
          // essayer récup fonction avec label et generalf
          $fonction = $this->em->getRepository(Fonction::class)->findOneBy(["label" => $label, "generalf" => $generalf]);
          if ($fonction) {
            $lexicalEntry->setFonction($fonction);
            $lexicalEntry->setSujetComplement($sujetComplement);
            $lexicalEntry->setComment($comment);

            $this->em->persist($fonction);
          }
        }
      }
      $cpt++;
    }
    $this->em->flush();
  }

  public function parseCSVCollocations($lines)
  {
    $cpt = 0;
    foreach ($lines as $line) {
      if ($cpt !== 0) {
        $tab = explode("\t", $line);
        $collocationValue = $tab[0];
        $complement = $tab[1];
        $labels = explode("&", $tab[3]);
        $generalfs = explode("&", $tab[4]);
        $partieDiscours = $tab[5];
        $regroupement = $tab[6];
        $patronParadigme = $tab[7];
        $patronFixe = $tab[8];

        $collocation = $this->em->getRepository(Collocation::class)->findOneByValue($collocationValue);
        if ($collocation) {
          $collocation->setComplement($complement ? $complement : null);
          $collocation->setPartieDiscours($partieDiscours ? $partieDiscours : null);
          $collocation->setRegroupement($regroupement ? $regroupement : null);
          $collocation->setPatronParadigme($patronParadigme ? $patronParadigme : null);
          $collocation->setPatronFixe($patronFixe ? $patronFixe : null);

          $indexLabel = 0;
          foreach ($labels as $label) {
            $generalf = $generalfs[$indexLabel];
            $fonction = $this->em->getRepository(Fonction::class)->findOneBy(["label" => $label, "generalf" => $generalf]);
            if ($fonction) {
              $collocation->addFonction($fonction);
            } else {
              // print "fonction [" . $label . " - ". $generalf  . "] not found \n";
            }
            $indexLabel++;
          }
          $this->em->persist($collocation);
        } else {
          // print "collocation [". $collocationValue . "] not found \n";
        }
      }
      $cpt++;
    }
    $this->em->flush();
  }
}
