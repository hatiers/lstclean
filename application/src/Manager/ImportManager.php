<?php

namespace App\Manager;

use App\Manager\LexicalEntryManager;
use App\Manager\PropertyManager;
use Doctrine\ORM\EntityManagerInterface;

class ImportManager
{
    private $em;
    private $propertyManager;
    private $entryManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        PropertyManager $propertyManager,
        LexicalEntryManager $entryManager
    ) {
        $this->em = $entityManager;
        $this->propertyManager = $propertyManager;
        $this->entryManager = $entryManager;
    }


    public function parseCSVLexicalEntries($arrayLines)
    {
        $cpt = 0;
        foreach ($arrayLines as $line) {
            if ($cpt !== 0) {
                //récupérer champ 3, 5, 9, 10, 11, 12
                $tab = explode("\t", $line);
                $catValue = $tab[0];
                $typeValue = $tab[1];
                $lemmaValue = $tab[2];
                $acceptionValue = $tab[4];
                $propMorph = $tab[5];
                $varGraph = $tab[6];
                $propSynt = $tab[7];
                $meaning = $tab[8];
                $sourceValue = $tab[9];
                $semanticClassValue = $tab[10];
                $semanticSubClassValue = $tab[11];
                $verbConstruction = $tab[13];

                $lemma = $this->propertyManager->getLemmaOrCreate($lemmaValue);
                $cat = $this->propertyManager->getCatOrCreate($catValue);
                $lexicalType = $this->propertyManager->getLexicalTypeOrCreate($typeValue);
                $source = $this->propertyManager->getSourceOrCreate($sourceValue);
                $semanticClass = $this->propertyManager->getSClassOrCreate($semanticClassValue);
                $semanticSubClass = $this->propertyManager->getSubClassOrCreate($semanticSubClassValue);

                $this->entryManager->create($cat, $lexicalType, $lemma, $acceptionValue, $propMorph, $varGraph, $propSynt, $meaning, $source, $semanticClass, $semanticSubClass, $verbConstruction);
                $this->em->flush();
            }
            $cpt++;
        }
        return;
    }
}
