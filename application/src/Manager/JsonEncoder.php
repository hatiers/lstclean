<?php
namespace App\Manager;

use Symfony\Contracts\Translation\TranslatorInterface;

class JsonEncoder {

  private $translator;

  public function __construct(TranslatorInterface $translator)
  {
      $this->translator = $translator;
  }

// QUESTION : REMETTRE CELA DANS CHAQUE JSON/MANAGER : POUR ALLEGER ?

  public function jsonLexicalEntries($entries)
  {
    // $entries =  $this->getDoctrine()->getRepository(LexicalEntry::class)->findAll();
    // $lexicalEntries['lexicalEntries'] = [];
    $lexicalEntries= [];

    foreach ($entries as $entry) {
      $lexicalEntry = [];
      // $lexicalEntry["id"] = $entry->getId();
      $lexicalEntry["cat"] = $entry->getCat()->getValue();
      $lexicalEntry["acceptionUniq"] = $entry->getAcceptionUniq();
      $lexicalEntry["lemma"] = $entry->getLemma()->getValue();
      $lexicalEntry["varGraph"] =  ($entry->getVargraph() == "") ?  null : $entry->getVargraph();
      $lexicalEntry["lexicalType"] = $entry->getLexicalType()->getValue();

      $lexicalEntry["fonctionId"] = ($entry->getFonction()) ? $entry->getFonction()->getLabel() : null ;

      $lexicalEntry["propMorph"] = ($entry->getPropMorph() == "") ? null : $entry->getPropMorph();
      $lexicalEntry["propSynt"] =  ($entry->getPropSynt() == "") ?  null : $entry->getPropSynt();
      $lexicalEntry["verbConstruction"] =  ($entry->getVerbConstruction() == "" or $entry->getVerbConstruction() == "\n") ?  null : trim($entry->getVerbConstruction());
      $lexicalEntry["sujetComplement"] =  ($entry->getSujetComplement() == "") ?  null : $entry->getSujetComplement();

      $lexicalEntry["semanticClass"] = $entry->getSemanticClass()->getValue();
      $lexicalEntry["semanticSubClass"] = $entry->getSemanticSubClass()->getValue();

      $lexicalEntry["meaning"] =  ($entry->getMeaning() == "") ?  null : $entry->getMeaning();
      $lexicalEntry["source"] =  ($entry->getSource()) ?  $entry->getSource()->getValue() : null ;
      $lexicalEntry["varGraph"] =  ($entry->getVargraph() == "") ?  null : $entry->getVargraph();
      $lexicalEntry["comment"] =  ($entry->getComment() == "" or $entry->getComment() == "\n") ?  null : trim($entry->getComment());

      if ($entry->getMetriques()){
        $metrics = $entry->getMetriques();
        $stats = [];

        // $stats["id"] = $metrics->getId();
        $stats["ratio"] = $metrics->getRatio();
        $stats["ccbym"] = $metrics->getCcbym();
        $stats["cabym"] = $metrics->getCabym();
        $stats["cafreq"] = $metrics->getCafreq();
        $stats["ngrammes"] = $metrics->getNgrammes();
        $stats["repartitionDisciplines"] = $metrics->getRepartitionDisciplines();
        $stats["repartitionTranches"] = $metrics->getRepartitionTranches();
        $stats["disciplinesSpec"] = $metrics->getDisciplinesSpec();
        $stats["freqParDisciplines"] = [];
        $stats["freqParDisciplines"]["anthropo"] = $metrics->getFreqAnthropo();
        $stats["freqParDisciplines"]["eco"] = $metrics->getFreqEco();
        $stats["freqParDisciplines"]["geo"] = $metrics->getFreqGeo();
        $stats["freqParDisciplines"]["histoire"] = $metrics->getFreqHistoire();
        $stats["freqParDisciplines"]["ling"] = $metrics->getFreqLing();
        $stats["freqParDisciplines"]["psycho"] = $metrics->getFreqPsycho();
        $stats["freqParDisciplines"]["sciedu"] = $metrics->getFreqSciedu();
        $stats["freqParDisciplines"]["sciepo"] = $metrics->getFreqSciepo();
        $stats["freqParDisciplines"]["scinfo"] = $metrics->getFreqScinfo();
        $stats["freqParDisciplines"]["socio"] = $metrics->getFreqSocio();
        $lexicalEntry["metrics"] = $stats ; 
      }

      if($entry->getExemples()->isEmpty()){
        $lexicalEntry["exemples"] = null ;
      }
      else {
        $lexicalEntry["exemples"] = [];
        foreach($entry->getExemples() as $ex){
          $exemple = [];
          // $exemple["id"] = $ex->getId();
          $exemple["source"] = ($ex->getSourceExemple()) ? $ex->getSourceExemple()->getValue() : null ;
          $exemple["contexteGauche"] =  ($ex->getContexteGauche()) ? $ex->getContexteGauche() : null ;
          $exemple["pivot"] = ($ex->getPivot()) ? $ex->getPivot() : null ;
          $exemple["contexteDroit"] = ($ex->getContexteDroit()) ? $ex->getContexteDroit() : null ;
          $exemple["partieTextuelle"] = ($ex->getPartieTextuelle()) ? trim($ex->getPartieTextuelle()) : null ;
          $lexicalEntry["exemples"][] = $exemple ;
        }
      }
     $lexicalEntries[] = $lexicalEntry;
    }
      // return json_encode($lexicalEntries);
      return $lexicalEntries;
  }


  /**
   * @Route("/collocations", name="collocations")
   */
  public function jsonCollocations($collocs){

    // $collocs =  $this->getDoctrine()->getRepository(Collocation::class)->findAll();
    // $collocations['collocations'] = [];
    $collocations = [];

    foreach($collocs as $col ){

      $collocation = [];
      // $collocation["id"] = $col->getId();
      $collocation["structureGlobale"] = $col->getStructureGlobale();
      $collocation["value"] = $col->getValue();
      $collocation["structureColloc"] = $col->getStructureColloc();
      $collocation["propSyntax"] = ($col->getPropSyntax() == "" or $col->getPropSyntax() == "\n" ) ? null : $col->getPropSyntax() ;
      $collocation["fonctionLexicale"] = ($col->getFonctionLexicale() == "" or $col->getFonctionLexicale() == "\n" ) ? null : $col->getFonctionLexicale() ;

      $collocation["lexicalEntryBaseId"] = ($col->getLexicalEntryBase()) ? $col->getLexicalEntryBase()->getAcceptionUniq() : null ;
      $collocation["lexicalEntryCollocatifId"] = ($col->getLexicalEntryCollocatif()) ? $col->getLexicalEntryCollocatif()->getAcceptionUniq() : null ;

      if($col->getFonction()->isEmpty()){
        $collocation["fonctions"] = null ;
      }
      else {
        $collocation["fonctions"] = [];
        foreach($col->getFonction() as $fonction){
          $collocation["fonctions"][] = $fonction->getLabel(); // OU GET LABEL ??? 
        }
      }

      $collocation["lemmaBase"] = ($col->getLemmaBase()) ? $col->getLemmaBase() : null ;
      $collocation["lemmaCollocatif"] = ($col->getLemmaCollocatif()) ? $col->getLemmaCollocatif() : null ;
      $collocation["complement"] = ($col->getComplement()) ? $col->getComplement() : null ;
      // $collocation["regroupement"] = ($col->getRegroupement()) ? $col->getRegroupement() : null ;  // Considéré comme à supprimer
      $collocation["patronParadigme"] = ($col->getPatronParadigme()) ? $col->getPatronParadigme() : null ;
      $collocation["patronFixe"] = ($col->getPatronFixe() == "" or $col->getPatronFixe() == "\n" ) ? null : trim($col->getPatronFixe()) ;
      $collocation["partieDiscours"] = ($col->getPartieDiscours()) ? $col->getPartieDiscours() : null ;

      $collocation["metrics"] = [];
      $collocation["metrics"]["nbOccurrences"] = $col->getNbOccurrences(); 
      $collocation["metrics"]["logLike"] = $col->getLogLike();
      $collocation["metrics"]["frequence"] = ($col->getFrequence() == "" or $col->getFrequence() == "\n" ) ? null : trim($col->getFrequence()) ;
      $collocation["metrics"]["nbDisciplines"] = $col->getNbDisciplines(); 

      if($col->getExempleCollocations()->isEmpty()){
        $collocation["exemples"] = null ;
      }
      else {
        $collocation["exemples"] = [];
        foreach($col->getExempleCollocations() as $ex){
          $exemple = [];
          // $exemple["id"] = $ex->getId();
          $exemple["source"] = ($ex->getSourceExemple()) ? $ex->getSourceExemple()->getValue() : null ;
          $exemple["structure"] = ($ex->getStructure()) ? $ex->getStructure() : null ;
          $exemple["contexteGauche"] =  ($ex->getContexteGauche()) ? $ex->getContexteGauche() : null ;
          $exemple["pivot"] = ($ex->getPivot()) ? $ex->getPivot() : null ;
          $exemple["contexteDroit"] = ($ex->getContexteDroit()) ? $ex->getContexteDroit() : null ;
          $exemple["partieTextuelle"] = ($ex->getPartieTextuelle()) ? trim($ex->getPartieTextuelle()) : null ;
          $collocation["exemples"][] = $exemple ;
        }
      }
      // $collocations['collocations'][] = $collocation;
      $collocations[] = $collocation;
  }
  //return json_encode($collocations);
  return $collocations;

}

  /**
   * @Route("/traductions", name="traductions")
   */
  public function jsonTranslations($selectedTranslations){

    foreach($selectedTranslations as $langCode => $trads){

      $lexicalTranslations = [];
      $lexicalTranslations[$langCode] = [];

      foreach ($trads as $trad) {
        $lexicalTranslation = [];
        // $lexicalTranslation['id'] = $trad->getId();
        $lexicalTranslation['nbOcc'] = $trad->getNbOcc();
        $lexicalTranslation['lemmaTrad'] = ($trad->getLemmaTrad()) ? $trad->getLemmaTrad()->getValue() : null ;
        $lexicalTranslation['lexicalEntryId'] = ($trad->getLexicalEntry()) ? $trad->getLexicalEntry()->getAcceptionUniq() : null ;
        // JAMAIS UTILISÉE ... ALors je laisse dans le code ou pas ? 
        $lexicalTranslation['collocationId'] = ($trad->getCollocation()) ? $trad->getCollocation()->getId() : null ;
        $lexicalTranslation['synonyms'] = ($trad->getSynonyms() != "" and $trad->getSynonyms() != "\n" ) ? $trad->getSynonyms() : null ;
        $lexicalTranslation['exempleSource'] = ($trad->getExampleSource() != "" and $trad->getExampleSource() != "\n" ) ? $trad->getExampleSource() : null ;
        $lexicalTranslation['exempleCible'] = ($trad->getExampleCible() != "" and $trad->getExampleCible() != "\n" ) ? $trad->getExampleCible() : null ;
        $lexicalTranslation['comment'] = ($trad->getComment() != "" and $trad->getComment() != "\n" ) ? $trad->getComment() : null ;
        $lexicalTranslation['usageGeneral'] = ($trad->getUsageGeneral() != "" and $trad->getUsageGeneral() != "\n" ) ? $trad->getUsageGeneral() : null ;
        $lexicalTranslation['usageSpecifique'] = ($trad->getusageSpecific() != "" and $trad->getusageSpecific() != "\n" ) ? $trad->getusageSpecific() : null ;
        $lexicalTranslation['contexte'] = ($trad->getContext() != "" and $trad->getContext() != "\n" ) ? $trad->getContext() : null ;

        $lexicalTranslations[$langCode][] = $lexicalTranslation;
      }
    }
    return $lexicalTranslations;
  }

  public function jsonFonctions($funcs){

    // $funcs =  $this->getDoctrine()->getRepository(Fonction::class)->findAll();

    // translator->trans() avec 4eme argument fixant le Locale sur 'fr', s'assure de récupérer les définitions/descriptions des fonctions stockées 
    // comme des chaînes de traduction dans /src/translations/messages.fr.yml

    $genFonctions = [];
    $lastF = null;
    foreach($funcs as $genFonction){
      if($lastF != $genFonction->getGeneralf()){
        $gen = [];
        $gen["id"] = $genFonction->getGeneralf();
        $gen["definition"] = $this->translator->trans($genFonction->getGeneralf() . "_complet", [], null, 'fr');
        $gen["fonctionsSpe"] = []; 

        $genFonctions[] = $gen;
        $lastF = $genFonction->getGeneralf();
      }
    }
    $cnt = 0;
    foreach($genFonctions as $f){
    
      $f["fonctionsSpe"] = [];
      foreach($funcs as $speFonction){
        if($speFonction->getGeneralf() == $f["id"]){

          $spe = [];
          // $spe["internalId"] = $speFonction->getId(); // je ne sais pas si je conserverais ... 
          $spe["id"] = $speFonction->getLabel();
          $spe["definition"] = $this->translator->trans($speFonction->getLabel() . "_desc", [], null, 'fr');
          $spe["exemple"] = $speFonction->getExemple();
          $genFonctions[$cnt]["fonctionsSpe"][] = $spe; 
        }
      }
      $cnt += 1;
    }
    // return json_encode($genFonctions);
    return $genFonctions;
  }

  public function jsonRoutines($routs){

    // $routs =  $this->getDoctrine()->getRepository(Routine::class)->findAll();
    // $routines['routines'] = [];
    $routines = [];

    foreach ($routs as $r) {
      $routine = [];
      // $routine['id'] = $r->getId();
      $routine["code"] = $r->getCode();
      $routine["type"] = ($r->getType()) ? $r->getType() : null ;
      $routine["subType"] = ($r->getSubType() != "" and $r->getSubType() != "\n") ? $r->getSubType() : null ;
      $routine["subSubType"] = ($r->getSubSubType() != "" and $r->getSubSubType() != "\n") ? $r->getSubSubType() : null ;

      $routine["fonctionId"] = ($r->getFonction()) ? $r->getFonction()->getLabel() : null ;

      $routine["modele"] = ($r->getModele() != "" and $r->getModele() != "\n") ? $r->getModele() : null ;
      $routine["realisation"] = ($r->getRealisation() != "" and $r->getRealisation() != "\n") ? $r->getRealisation() : null ;
      $routine["occurrences"] = ($r->getOccurrence()) ? $r->getOccurrence() : null ;
      $routine["patron"] = ($r->getPatron() != "" and $r->getPatron() != "\n") ? $r->getPatron() : null ;

      if ($r->getExemples()->isEmpty()){
        $routine["exemples"] = null;
      }
      else {
        $routine["exemples"] = [];
        foreach($r->getExemples() as $ex){
            $exemple = [];
            // $exemple['id'] = $ex->getId(); 
            $exemple['source'] = ($ex->getSource()) ? $ex->getSource()->getValue() : null; 
            $exemple['type'] =  ($ex->getType()) ? $ex->getType(): null; 
            $exemple['content'] =  $ex->getContent(); 
            $exemple['simplicity'] = ($ex->getSimplicity() != "" and $ex->getSimplicity() != "\n") ? $ex->getSimplicity() : null ; 
            $routine["exemples"][] = $exemple;
        }
      }
      // $routines['routines'][] = $routine;
      $routines[] = $routine;
    }
//    return json_encode($routines);
    return $routines;
  }

  public function jsonSemanticClasses($classes) {

    //$classes =$this->exportFunctions->retrieveSemanticClasses();
    // $semClasses['classesSemantiques'] = [];
    $semClasses = [];
    
    foreach($classes as $class){
      $semClass = [];
      $semClass["id"] = $class["value"];

      $semClass["definition"] = [];
      foreach($class["classDescription"] as $descr) {
        $description["cat"] = $descr->syntac;
        $description["description"] =($descr->desc != "" and $descr->desc != "\n") ? trim($descr->desc) : null ; 
        $description["critereTest"] = ($descr->test != "" and $descr->test != "\n") ? trim($descr->test) : null ; 
        $semClass["definition"][] = $description;
      }

      $semClass["sousClassesSemantiques"] = [];

      foreach($class["subclasses"] as $sub) {

        $subSemClass = [];
        $subSemClass["id"] = $sub["value"];
        $subSemClass["definition"] = [];

        foreach($sub["subclassDescription"] as $subDescr) {
          $subDescription["cat"] = $subDescr->syntac;
          $subDescription["description"] = ($subDescr->desc != "" and $subDescr->desc != "\n") ? trim($subDescr->desc) : null ;
          $subDescription["critereTest"] = ($subDescr->test != "" and $subDescr->test != "\n") ? trim($subDescr->test) : null ;
          $subSemClass["definition"][] = $subDescription;
        }
        $semClass["sousClassesSemantiques"][] = $subSemClass;
      }
      // $semClasses['classesSemantiques'][] = $semClass;
      $semClasses[] = $semClass;
    }
    //return json_encode($semClasses);
    return $semClasses;
  }

  public function jsonExemplesSources($exSrcs) {

    //$exSrcs =  $this->getDoctrine()->getRepository(SourceExemple::class)->findAll();
    // $exemplesSources["sourcesExemples"] = [];
    $exemplesSources = [];
    
    foreach($exSrcs as $src){
      $source = [];
      // $source["internalId"] = $src->getId();
      $source["id"] = $src->getValue();
      $source["descriptionBiblio"] = $src->getDescriptionBiblio();
      //$exemplesSources["sourcesExemples"][] = $source;
      $exemplesSources[] = $source;
    }
    // return json_encode($exemplesSources);
    return $exemplesSources;
  }

  // Syntac_Cat ?? 
  // LexicalType ?? 
  // Source ?? 
  // Language ?? 

}