<?php

namespace App\Manager;

use App\Entity\Language;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\LemmaTrad;
use App\Entity\LexicalEntryTrad;

class LanguageManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function create($name, $shortName, $translatedName)
    {
        $language = new Language;
        $language->setName($name);
        $language->setShortName($shortName);
        $language->setTranslatedName($translatedName);

        $this->em->persist($language);
        $this->em->flush();

        return $language;
    }

    public function clean(Language $language){
        $lemmaTrads = $this->em->getRepository(LemmaTrad::class)->findByLanguage($language);
        foreach ($lemmaTrads as $lemmaTrad) {
            $this->em->remove($lemmaTrad);
        }

        $this->em->flush();
    }

    public function delete(Language $language){
        
        $this->clean($language);
        $this->em->remove($language);

        $this->em->flush();
    }
}
