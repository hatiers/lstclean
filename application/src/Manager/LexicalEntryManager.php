<?php

namespace App\Manager;

use App\Entity\Lemma;
use App\Entity\LexicalEntry;
use App\Entity\LexicalType;
use App\Entity\SemanticClass;
use App\Entity\SemanticSubClass;
use App\Entity\Source;
use App\Entity\SyntacCat;
use App\Manager\CacheManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class LexicalEntryManager
{
    private $em;
    private $cm;

    public function __construct(EntityManagerInterface $em, CacheManager $cm)
    {
        $this->em = $em;
        $this->cm = $cm;
    }

    public function getRankFreq(LexicalEntry $lexicalEntry)
    {
        $rankFreqCache = $this->cm->get("rank-freq-" . $lexicalEntry->getId());

        if ($rankFreqCache->isHit()) {
            $result = $rankFreqCache->get();
        } else {
            $currentMetriques = $lexicalEntry->getMetriques();
            if ($currentMetriques) {
                $currentCaFreq  = $currentMetriques->getCafreq();
                $totalLexicalEntries    = count($this->em->getRepository(LexicalEntry::class)->findAll());
                $underFreqLexicalEntry = $this->em->getRepository(LexicalEntry::class)->findByUnderCaFreq($currentCaFreq);
                $noMetrics = $this->em->getRepository(LexicalEntry::class)->findByNoMetrics();
                
                $rankLexicalEntryFreq  = ($underFreqLexicalEntry + $noMetrics) / $totalLexicalEntries * 100;

                $result = round($rankLexicalEntryFreq, 2);
            } else {
                $result = 0;
            }

            $this->cm->store($rankFreqCache, $result);
        }

        return $result;
    }

    public function getRankSpec(LexicalEntry $lexicalEntry)
    {
        $rankSpecCache = $this->cm->get("rank-spec-" . $lexicalEntry->getId());

        if ($rankSpecCache->isHit()) {
            $result = $rankSpecCache->get();
        } else {
            $currentMetriques = $lexicalEntry->getMetriques();
            if ($currentMetriques) {
                $currentRatio     = $currentMetriques->getRatio();
                $totalLexicalEntries    = count($this->em->getRepository(LexicalEntry::class)->findAll());
                $underRatioLexicalEntry = $this->em->getRepository(LexicalEntry::class)->findByUnderRatio($currentRatio);
                $noMetrics = $this->em->getRepository(LexicalEntry::class)->findByNoMetrics();
                $rankLexicalEntryRatio  = ($underRatioLexicalEntry + $noMetrics) / $totalLexicalEntries * 100;
                $result = round($rankLexicalEntryRatio, 2);
            } else {
                $result = 0;
            }

            $this->cm->store($rankSpecCache, $result);
        }

        return $result;
    }

    public function getJSONGraphColloc(LexicalEntry $lexicalEntry, $fromAjax = false)
    {
        $maxWeight = 50;
        $nodesArray = [];
        $edgesArray = [];
        $collocationsBase = $lexicalEntry->getCollocationsBase();
        $collocationsCollocatif =  $lexicalEntry->getCollocationsCollocatif();
        $lexicalEntryId = $lexicalEntry->getId();
        $suffix = $fromAjax ? "-ajax" : "";
        $lexicalEntryCache = $this->cm->get("graph-lexicalEntry." . $lexicalEntryId . $suffix);
        if ($lexicalEntryCache->isHit()) {
            $datas = $lexicalEntryCache->get();
        } else {
            // $weightNode = ($lexicalEntry->getMetriques()) ? $lexicalEntry->getMetriques()->getCafreq() : 50 ;
            $this->createNode($lexicalEntryId, $lexicalEntry->getLemma()->getValue(), "green", 50, $nodesArray, $datas, null, $fromAjax);

            foreach ($collocationsBase as $base) {
                $text = $base->getLemmaCollocatif();
                $caption = $base->getStructureGlobale();
                $edgeId = $base->getId();
                $id = ($base->getlexicalEntryCollocatif()) ? $base->getlexicalEntryCollocatif()->getId() : $text;
                $weightNode = ($base->getlexicalEntryCollocatif() && $base->getlexicalEntryCollocatif()->getMetriques()) ? $base->getlexicalEntryCollocatif()->getMetriques()->getCafreq() : 1;
                $weightnode = $base->getNbOccurrences();
                $weightnode = ($weightnode > $maxWeight) ? $maxWeight : $weightnode;
                $label = $base->getStructureGlobale();
                $colorNode = ($fromAjax) ? "grey"  : "red";
                $this->createNode($id, $text, $colorNode, $weightnode, $nodesArray, $datas, $lexicalEntryId, $fromAjax);
                $this->createEdge($edgeId, $id, $lexicalEntryId, 2, $label, $caption, $edgesArray, $datas);
            }

            foreach ($collocationsCollocatif as $collocatif) {
                $text = $collocatif->getLemmaBase();
                $caption = $collocatif->getStructureGlobale();
                $edgeId = $collocatif->getId();
                $id = ($collocatif->getlexicalEntryBase()) ? $collocatif->getlexicalEntryBase()->getId() : $text;
                $weightNode = ($collocatif->getlexicalEntryBase() && $collocatif->getlexicalEntryBase()->getMetriques()) ? $collocatif->getlexicalEntryBase()->getMetriques()->getCafreq() : 1;
                $weightnode = $collocatif->getNbOccurrences();
                $weightnode = ($weightnode > $maxWeight) ? $maxWeight : $weightnode;
                $label = $collocatif->getStructureGlobale();
                $colorNode = ($fromAjax) ? "grey" : "blue";
                $this->createNode($id, $text, $colorNode, $weightnode, $nodesArray, $datas, $lexicalEntryId, $fromAjax);
                $this->createEdge($edgeId, $lexicalEntryId, $id, 2, $label, $caption, $edgesArray, $datas);
            }
            $this->cm->store($lexicalEntryCache, $datas);
        }
        return json_encode($datas);
    }



    public function getJSONGraphCollocs()
    {
        $maxWeight = 50;
        $nodesArray = [];
        $edgesArray = [];
        $entries = $this->em->getRepository(LexicalEntry::class)->findAll();
        $lexicalEntryCache = $this->cm->get("graph-lexicalEntry.all");
        if ($lexicalEntryCache->isHit()) {
            $datas = $lexicalEntryCache->get();
        } else {
            foreach ($entries as $lexicalEntry) {
                $collocationsBase = $lexicalEntry->getCollocationsBase();
                $collocationsCollocatif =  $lexicalEntry->getCollocationsCollocatif();
                $lexicalEntryId = $lexicalEntry->getId();
                if (!$collocationsBase->isEmpty() || !$collocationsCollocatif->isEmpty()) {
                    $this->createNode($lexicalEntryId, $lexicalEntry->getLemma()->getValue(), "green", 50, $nodesArray, $datas, null, false);
                }

                foreach ($collocationsBase as $base) {
                    $structureColloc = $base->getStructureColloc();
                    $nbOccurrences = $base->getNbOccurrences();
                    $logLike = $base->getLogLike();
                    $nbDisciplines = $base->getNbDisciplines();
                    $fonctionLexicale = $base->getFonctionLexicale();
                    $text = $base->getLemmaCollocatif();
                    $caption = $base->getStructureGlobale();
                    $edgeId = $base->getId();
                    $id = ($base->getlexicalEntryCollocatif()) ? $base->getlexicalEntryCollocatif()->getId() : $text;
                    $weightEdge = $base->getNbOccurrences();
                    $weightEdge = ($weightEdge > $maxWeight) ? $maxWeight : $weightEdge;
                    $label = $base->getStructureGlobale();
                    $colorNode = "grey";
                    $this->createNode($id, $text, $colorNode, $weightEdge, $nodesArray, $datas, "", false);
                    $this->createEdgeColloc($edgeId, $id, $lexicalEntryId, 2, $label, $caption, $structureColloc, $nbOccurrences, $logLike, $fonctionLexicale, $nbDisciplines, $edgesArray, $datas);
                }

                foreach ($collocationsCollocatif as $collocatif) {
                    $structureColloc = $collocatif->getStructureColloc();
                    $nbOccurrences = $collocatif->getNbOccurrences();
                    $nbDisciplines = $collocatif->getNbDisciplines();
                    $logLike = $collocatif->getLogLike();
                    $fonctionLexicale = $collocatif->getFonctionLexicale();
                    $text = $collocatif->getLemmaBase();
                    $caption = $collocatif->getStructureGlobale();
                    $edgeId = $collocatif->getId();
                    $id = ($collocatif->getlexicalEntryBase()) ? $collocatif->getlexicalEntryBase()->getId() : $text;
                    $weightEdge = $collocatif->getNbOccurrences();
                    $weightEdge = ($weightEdge > $maxWeight) ? $maxWeight : $weightEdge;
                    $label = $collocatif->getStructureGlobale();
                    $colorNode = "grey";
                    $this->createNode($id, $text, $colorNode, $weightEdge, $nodesArray, $datas, "", false);
                    $this->createEdgeColloc($edgeId, $lexicalEntryId, $id, 2, $label, $caption, $structureColloc, $nbOccurrences, $logLike, $fonctionLexicale, $nbDisciplines, $edgesArray, $datas);
                }
                $this->cm->store($lexicalEntryCache, $datas);
            }
        }

        return $datas;
    }



    public function getJSONGraph()
    {
        $maxWeight = 50;
        $entries = $this->em->getRepository(LexicalEntry::class)->findEntryConnected();
        $nodesArray = [];
        $datas = [];
        foreach ($entries as $entry) {
            $text = $entry->getLemma()->getValue();
            $nodeId = $entry->getId();
            $weightNode = ($entry->getMetriques()) ? $entry->getMetriques()->getRatio() : 1;
            $weightNode = ($weightNode > $maxWeight) ? $maxWeight : $weightNode;
            $colorNode = "grey";
            $this->createNode($nodeId, $text, $colorNode, $weightNode, $nodesArray, $datas, "", false);
        }

        return json_encode($datas);
    }

    private function createNode($id, $text, $color, $weight, &$nodesArray, &$datas, $parent, $fromAjax)
    {
        if (!in_array($id, $nodesArray)) {
            $node = ['group' => "nodes", "data" => ["weight" => $weight, "color" => $color, "id" => $id, "text" =>  $text]];
            if ($fromAjax) {
                $node["data"]["parentId"] = $parent;
            }
            $datas[] = $node;
            $nodesArray[] = $id;
        }

        return;
    }

    private function createEdgeColloc($id, $target, $source, $weight, $label, $caption, $structureColloc, $nbOccurrences, $logLike, $fonctionLexicale, $nbDisciplines, &$edgesArray, &$datas)
    {
        if (!in_array($id, $edgesArray)) {
            $datas[] = ['group' => "edges", "data" => ["source" => $source, "target" => $target, "weight" => $weight, "label" => $label, "caption" => $caption, "id" => $id, "structureColloc" => $structureColloc, "nbOccurrences" => $nbOccurrences, "logLike" => $logLike, "fonctionLexicale" => $fonctionLexicale, "nbDisciplines" => $nbDisciplines]];
            $edgesArray[] = $id;
        }
        return;
    }

    private function createEdge($id, $target, $source, $weight, $label, $caption, &$edgesArray, &$datas)
    {
        if (!in_array($id, $edgesArray)) {
            $datas[] = ['group' => "edges", "data" => ["source" => $source, "target" => $target, "weight" => $weight, "label" => $label, "caption" => $caption, "id" => $id]];
            $edgesArray[] = $id;
        }
        return;
    }

    public function edgeToData($datas, $field)
    {
        $values = [];
        foreach ($datas as $data) {
            if (array_key_exists($field, $data["data"])) {
                if ((!in_array($data["data"][$field], $values)) && ($data["data"][$field] != "")) {
                    $values[] = $data["data"][$field];
                }
            }
        }
        return $values;
    }


    public function getJSON(LexicalEntry $lexicalEntry)
    {
        $encoders = [new JsonEncoder()];
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceHandler(function ($object, string $format = null, array $context = []) {
            return $object->getAcceptionUniq();
        });
        $serializer = new Serializer([$normalizer], $encoders);
        $json =  $serializer->serialize($lexicalEntry, 'json');

        return $json;
    }

    public function create(
        SyntacCat $cat,
        LexicalType $type,
        Lemma $lemma,
        $acceptionValue,
        $propMorph,
        $varGraph,
        $propSynt,
        $meaning,
        Source $source,
        SemanticClass $semanticClass,
        SemanticSubClass $semanticSubClass,
        $verbConstruction
    ) {
        $entry = new LexicalEntry();

        $entry->setCat($cat);
        $entry->setLexicalType($type);
        $entry->setLemma($lemma);
        $entry->setAcceptionUniq($acceptionValue);
        $entry->setPropMorph($propMorph);
        $entry->setVarGraph($varGraph);
        $entry->setPropSynt($propSynt);
        $entry->setMeaning($meaning);
        $entry->setSource($source);
        $entry->setSemanticClass($semanticClass);
        $entry->setSemanticSubClass($semanticSubClass);
        $entry->setVerbConstruction($verbConstruction);

        $this->em->persist($entry);

        return $entry;
    }
    public function emptydb()
    {
        $lexicalEntries = $this->em->getRepository(LexicalEntry::class)->findAll();
        foreach ($lexicalEntries as $lexicalEntry) {
            $this->em->remove($lexicalEntry);
        }
        $this->em->flush();
    }
}
