<?php

namespace App\Manager;

use App\Entity\Collocation;
use App\Entity\Language;
use App\Entity\LemmaTrad;
use App\Entity\LexicalEntry;
use App\Entity\LexicalEntryTrad;
use Doctrine\ORM\EntityManagerInterface;

class LexicalEntryTradManager
{
  private $em;

  public function __construct(EntityManagerInterface $em)
  {
    $this->em = $em;
  }

  public function parseCSV($lines, Language $language)
  {
    $cpt = 0;
    foreach ($lines as $line) {
      if ($cpt !== 0) {
        $tab = explode("\t", $line);

        $value = $tab[0];
        $nbOcc = $tab[1];
        $exampleCible = $tab[2];
        $targetEntity = $tab[3];
        $exampleSource = $tab[4];
        $context = $tab[5];
        $comment = $tab[6];
        $usageSpecific = $tab[7];
        $usageGeneral = $tab[8];
        $synonyms = $tab[9];

        $lemmaTrad = $this->em->getRepository(LemmaTrad::class)->findOneBy(["value" => $value, "language" => $language]);
        if (!$lemmaTrad) {
          $lemmaTrad = new LemmaTrad;
          $lemmaTrad->setValue($value);
          $lemmaTrad->setLanguage($language);

          $this->em->persist($lemmaTrad);
        }

        $lexicalEntryTrad = new LexicalEntryTrad;
        $lexicalEntryTrad->setNbOcc(intval($nbOcc));
        $lexicalEntryTrad->setExampleCible($exampleCible);
        $lexicalEntryTrad->setExampleSource($exampleSource);
        $lexicalEntryTrad->setUsageSpecific($usageSpecific);
        $lexicalEntryTrad->setUsageGeneral($usageGeneral);
        $lexicalEntryTrad->setSynonyms($synonyms);
        $lexicalEntryTrad->setContext($context);
        $lexicalEntryTrad->setComment($comment);

        if ($collocation = $this->em->getRepository(Collocation::class)->findOneByValue($targetEntity)) {
          $lexicalEntryTrad->setCollocation($collocation);
        } elseif ($lexicalEntry = $this->em->getRepository(LexicalEntry::class)->findOneByAcceptionUniq($targetEntity)) {
          $lexicalEntryTrad->setLexicalEntry($lexicalEntry);
        }

        $this->em->persist($lexicalEntryTrad);
        $lemmaTrad->addLexicalEntryTrad($lexicalEntryTrad);
        $this->em->persist($lemmaTrad);

        $this->em->flush();
      }
      $cpt++;
    }

    return;
  }
}
