<?php

namespace App\Manager;

class LocaleManager
{
    public function normalize($str)
    {
        return str_replace(
            ["à", "â", "ç", "è", "é", "ê", "ë"],
            ["a", "a", "c", "e", "e", "e", "e"],
            $str
        );
    }

    public function getFirstLetter($str)
    {
        return mb_substr($str, 0, 1);
    }
}
