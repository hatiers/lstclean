<?php

namespace App\Manager;

use App\Entity\Lemma;
use App\Entity\LexicalEntry;
use App\Entity\Metriques;
use App\Entity\SyntacCat;
use Doctrine\ORM\EntityManagerInterface;

class MetriquesManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function parseCSVMetriques($lines)
    {
        $cpt = 0;
        foreach ($lines as $line) {
            if ($cpt !== 0) {
                $currentMetrique = new Metriques();
                $tab = explode("\t", $line);
                $lemmaString = $tab[0];
                $syntactCatString = $tab[1];
                $ratio = $tab[2];
                $ccbym = $tab[3];
                $cabym = $tab[4];
                $cafreq = $tab[5];
                $ngrammes = $tab[6];
                $disciplines = $tab[7];
                $tranches = $tab[8];
                $disciplinesSpec = $tab[9];
                $freqAnthropo = $tab[10];
                $freqEco = $tab[11];
                $freqGeo = $tab[12];
                $freqHistoire = $tab[13];
                $freqLing = $tab[14];
                $freqPsycho = $tab[15];
                $freqSciedu = $tab[16];
                $freqSciepo = $tab[17];
                $freqScinfo = $tab[18];
                $freqSocio = $tab[19];
                $freqSocio = preg_replace("/\s/", "", $freqSocio);

                $currentMetrique->setRatio($ratio);
                $currentMetrique->setCcbym($ccbym);
                $currentMetrique->setCabym($cabym);
                $currentMetrique->setCafreq($cafreq);
                $currentMetrique->setNgrammes($ngrammes);
                $currentMetrique->setRepartitionDisciplines($disciplines);
                $currentMetrique->setRepartitionTranches($tranches);

                $currentMetrique->setDisciplinesSpec($disciplinesSpec);
                $currentMetrique->setFreqAnthropo($freqAnthropo);
                $currentMetrique->setFreqEco($freqEco);
                $currentMetrique->setFreqGeo($freqGeo);
                $currentMetrique->setFreqHistoire($freqHistoire);
                $currentMetrique->setFreqLing($freqLing);
                $currentMetrique->setFreqPsycho($freqPsycho);
                $currentMetrique->setFreqSciedu($freqSciedu);
                $currentMetrique->setFreqSciepo($freqSciepo);
                $currentMetrique->setFreqScinfo($freqScinfo);
                $currentMetrique->setFreqSocio($freqSocio);

                $currentLemma = $this->em->getRepository(Lemma::class)->findOneByValue($lemmaString);

                $currentSyntactCat = $this->em->getRepository(SyntacCat::class)->findOneByValue($syntactCatString);

                $currentLexicalEntries = $this->em->getRepository(LexicalEntry::class)->findBy(['lemma' => $currentLemma, 'cat' => $currentSyntactCat]);

                foreach ($currentLexicalEntries as $currentLexicalEntry) {
                    $currentLexicalEntry->setMetriques($currentMetrique);
                    $this->em->persist($currentLexicalEntry);
                }
                $this->em->persist($currentMetrique);
            }
            $cpt++;
        }
        $this->em->flush();
        return;
    }

    public function emptydb()
    {
        $arrayMetriques = $this->em->getRepository(Metriques::class)->findAll();
        foreach ($arrayMetriques as $unitMetriques) {
            $this->em->remove($unitMetriques);
        }
        $this->em->flush();
    }
}
