<?php

namespace App\Manager;

use App\Entity\Lemma;
use App\Entity\LexicalType;
use App\Entity\SemanticClass;
use App\Entity\SemanticSubClass;
use App\Entity\Source;
use App\Entity\SyntacCat;
use Doctrine\ORM\EntityManagerInterface;

class PropertyManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getLemmaOrCreate($value)
    {
        $lemma = $this->em->getRepository(Lemma::class)->findOneByValue($value);

        if (!$lemma) {
            $lemma = new Lemma();
            $lemma->setValue($value);
            $this->em->persist($lemma);
        }

        return $lemma;
    }


    public function getCatOrCreate($value)
    {
        $cat = $this->em->getRepository(SyntacCat::class)->findOneByValue($value);

        if (!$cat) {
            $cat = new SyntacCat();
            $cat->setValue($value);
            $this->em->persist($cat);
        }

        return $cat;
    }


    public function getLexicalTypeOrCreate($value)
    {
        $lexicaltype = $this->em->getRepository(LexicalType::class)->findOneByValue($value);

        if (!$lexicaltype) {
            $lexicaltype = new LexicalType();
            $lexicaltype->setValue($value);
            $this->em->persist($lexicaltype);
        }

        return $lexicaltype;
    }

    public function getSourceOrCreate($value)
    {
        $source = $this->em->getRepository(Source::class)->findOneByValue($value);

        if (!$source) {
            $source = new Source();
            $source->setValue($value);
            $this->em->persist($source);
        }

        return $source;
    }

    public function getSClassOrCreate($value)
    {
        $semanticClass = $this->em->getRepository(SemanticClass::class)->findOneByValue($value);

        if (!$semanticClass) {
            $semanticClass = new SemanticClass();
            $semanticClass->setValue($value);
            $this->em->persist($semanticClass);
        }

        return $semanticClass;
    }

    public function getSubClassOrCreate($value)
    {
        $semanticSubClass = $this->em->getRepository(SemanticSubClass::class)->findOneByValue($value);

        if (!$semanticSubClass) {
            $semanticSubClass = new SemanticSubClass();
            $semanticSubClass->setValue($value);
            $this->em->persist($semanticSubClass);
        }

        return $semanticSubClass;
    }
}
