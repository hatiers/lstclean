<?php

namespace App\Manager;

use App\Entity\ExempleRoutine;
use App\Entity\Fonction;
use App\Entity\Routine;
use App\Entity\SourceExemple;
use Doctrine\ORM\EntityManagerInterface;

class RoutineManager
{
  private $em;

  public function __construct(EntityManagerInterface $em)
  {
    $this->em = $em;
  }

  public function parseCSV($lines)
  {
    $cpt = 0;
    foreach ($lines as $line) {
      if ($cpt !== 0) {
        $tab = explode("\t", $line);
        $code = trim($tab[0]);
        $generalf = trim($tab[1]);
        $label = trim($tab[2]);
        $fonction = $this->em->getRepository(Fonction::class)->findOneBy(["label" => $label, "generalf" => $generalf]);
        $type = trim($tab[3]);
        $subType = trim($tab[4]);
        $subSubtype = trim($tab[5]);
        $modele = trim($tab[7]);
        $realisation = trim($tab[8]);
        $occurrence = trim($tab[9]);

        $max = count($tab);
        $patronArray = [];
        for ($i = 10; $i <= $max - 1; $i++) {
          if (trim($tab[$i]) != "") {
            $patronArray[] = trim($tab[$i]);
          }
        }
        $patron = implode($patronArray, "----");

        $routine = $this->em->getRepository(Routine::class)->findOneByCode($code);

        if (!$routine) {
          $routine = new Routine();
          $routine->setCode($code);
        }

        $routine->setType($type);
        $routine->setSubType($subType);
        $routine->setSubSubType($subSubtype);
        $routine->setModele($modele);
        $routine->setRealisation($realisation);
        $routine->setOccurrence($occurrence);
        $routine->setPatron($patron);
        $routine->setFonction($fonction);

        $this->em->persist($routine);
      }
      $cpt++;
    }
    $this->em->flush();

    return;
  }

  public function parseCSVExemples($lines)
  {
    $cpt = 0;
    foreach ($lines as $line) {
      if ($cpt !== 0) {
        $tab = explode("\t", $line);
        $code = trim($tab[0]);
        $type = trim($tab[1]);
        $content = isset($tab[2]) ? trim($tab[2]) : null;
        $sourceValue = isset($tab[5]) ? trim($tab[5]) : null;
        $source = $this->em->getRepository(SourceExemple::class)->findOneByValue($sourceValue);
        $simplicity = isset($tab[6]) ? trim($tab[6]) : null;

        $routine = $this->em->getRepository(Routine::class)->findOneByCode($code);
        if ($routine && $content) {
          $exemple = new ExempleRoutine;
          $exemple->setRoutine($routine);
          $exemple->setSource($source);
          $exemple->setType($type);
          $exemple->setContent($content);
          $exemple->setSimplicity($simplicity);

          $this->em->persist($exemple);
        }
      }
      $cpt++;
    }
    $this->em->flush();
  }
}
