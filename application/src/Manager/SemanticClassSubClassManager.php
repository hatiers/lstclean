<?php

namespace App\Manager;

use App\Entity\LexicalEntry;
use App\Entity\SemanticClass;
use App\Entity\SemanticClassSubClass;
use App\Entity\SemanticSubClass;
use App\Entity\SyntacCat;
use Doctrine\ORM\EntityManagerInterface;

class SemanticClassSubClassManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function parseCSVDescriptionClass($lines)
    {
        $cpt=0;
        foreach ($lines as $line) {
            if ($cpt !== 0) {
                $semanticClassSubClass = new SemanticClassSubClass();
                $tab=explode("\t", $line);
                $semanticClassString=$tab[0];
                $semantiSubClassString=$tab[1];
                $semanticClassDesc=$tab[2];
                $semanticClassTest=$tab[3];
                $semanticSubClassDesc=$tab[4];
                $semanticSubClassTest=$tab[5];
                $semanticSubClassCatString=$tab[6];


                $semanticClassSubClass->setDescriptionClass($semanticClassDesc);
                $semanticClassSubClass->setTestAppartClass($semanticClassTest);

                $currentSemanticClass = $this->em->getRepository(SemanticClass::class)->findOneByValue($semanticClassString);
                $semanticClassSubClass->setSemanticClass($currentSemanticClass);

                $currentSemanticSubClass = $this->em->getRepository(SemanticSubClass::class)->findOneByValue($semantiSubClassString);
                $semanticClassSubClass->setSemanticSubClass($currentSemanticSubClass);

                $currentSyntacCat = $this->em->getRepository(SyntacCat::class)->findOneByValue(trim($semanticSubClassCatString));
                $semanticClassSubClass->setSyntacCatClassSubClass($currentSyntacCat);

                $semanticClassSubClass->setDescriptionSubClass($semanticSubClassDesc);
                $semanticClassSubClass->setTestAppartSubClass($semanticSubClassTest);

                //ajout de la relation pour les $lexicalEntries

                $lexicalEntries = $this->em->getRepository(LexicalEntry::class)->findLexicalEntryByClassSubClassSyntactCat($currentSemanticClass, $currentSemanticSubClass, $currentSyntacCat);
                foreach ($lexicalEntries as $lexicalEntry) {
                    //echo("Passage REPO LE \n");
                    $lexicalEntry->setSemanticClassSubClass($semanticClassSubClass);
                    $this->em->persist($lexicalEntry);
                    $semanticClassSubClass->addLexicalEntry($lexicalEntry);
                }

                // besoin de vérifier que la classe et la sous-classe sont effectivement présentes en base
                if ($currentSemanticSubClass && $currentSemanticClass && $currentSyntacCat) {
                    $this->em->persist($currentSemanticClass);
                    $this->em->persist($semanticClassSubClass);
                } else {
                    echo("BLEME Classe SC inexistante : C: ".$semanticClassString." SC: ".$semantiSubClassString." CAT: ".$semanticSubClassCatString."\n");
                }
            }
            $cpt++;
        }
        $this->em->flush();
        return;
    }
    public function emptydb()
    {
        $semanticClassSubClasses= $this->em->getRepository(SemanticClassSubClass::class)->findAll();
        foreach ($semanticClassSubClasses as $semanticClassSubClass) {
            // code...
            $this->em->remove($semanticClassSubClass);
        }
        $this->em->flush();
    }
}
