<?php

namespace App\Manager;

use App\Entity\SourceExemple;
use Doctrine\ORM\EntityManagerInterface;

class SourceExempleManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function parseTXTBiblio($lines)
    {
        foreach ($lines as $line) {
            if (!preg_match("/^\s*$/", $line)) {
                if (preg_match("/[^\t]+\t[^\t]+$/", $line)) {
                    $sourceExemple = new SourceExemple();
                    $tab=explode("\t", $line);
                    $sourceExempleBiblioString=$tab[0];
                    $sourceExempleValueString=trim($tab[1]);
                    $sourceExemple->setDescriptionBiblio($sourceExempleBiblioString);
                    $sourceExemple->setValue($sourceExempleValueString);
                    $this->em->persist($sourceExemple);
                }
            }
        }
        $this->em->flush();
        return;
    }
    public function emptydb()
    {
        $sourceExemples= $this->em->getRepository(SourceExemple::class)->findAll();
        foreach ($sourceExemples as $sourceExemple) {
            $this->em->remove($sourceExemple);
        }
        $this->em->flush();
    }
}
