<?php

namespace App\Manager;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class UserManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function switchAdmin(User $user)
    {
        $roles = $user->getRoles();
        if (!in_array("ROLE_ADMIN", $roles)) {
            $roles[] = "ROLE_ADMIN";
        } else {
            $i = 0;
            foreach ($roles as $role) {
                if ($role == "ROLE_ADMIN") {
                    unset($roles[$i]);
                }
                $i++;
            }
        }

        $user->setRoles($roles);
        $this->em->persist($user);
        $this->em->flush();

        return;
    }
}
