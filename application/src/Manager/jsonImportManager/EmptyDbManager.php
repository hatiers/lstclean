<?php 

namespace App\Manager\jsonImportManager;

use App\Entity\Collocation;
use App\Entity\Exemple;
use App\Entity\ExempleCollocation;
use App\Entity\ExempleRoutine;
use App\Entity\Fonction;
use App\Entity\Language;
use App\Entity\Lemma;
use App\Entity\LemmaTrad;
use App\Entity\LexicalEntry;
use App\Entity\LexicalEntryTrad;
use App\Entity\LexicalType;
use App\Entity\Metriques;
use App\Entity\Routine;
use App\Entity\SemanticClass;
use App\Entity\SemanticClassSubClass;
use App\Entity\SemanticSubClass;
use App\Entity\Source;
use App\Entity\SourceExemple;
use App\Entity\SyntacCat;

use Doctrine\ORM\EntityManagerInterface;

class EmptyDbManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    // supprime toutes les entités sauf User et Article
    public function truncateAllTables(){

        $allClasses = [
            Collocation::class,
            ExempleRoutine::class,
            Routine::class,
            Exemple::class,
            ExempleCollocation::class,
            Fonction::class,
            LexicalEntryTrad::class,
            LexicalEntry::class,
            Source::class,
            LemmaTrad::class,
            Lemma::class,
            Language::class,
            LexicalType::class,
            Metriques::class,
            SemanticClassSubClass::class,
            SemanticSubClass::class,
            SemanticClass::class,
            SourceExemple::class,
            SyntacCat::class
        ];

        foreach ($allClasses as $class){
            echo "delete " . $class . "\n" . "\n" ;
            $entities = $this->em->getRepository($class)->findAll();
            // dd($entities);

            foreach ($entities as $entity) {
                $this->em->remove($entity);
            }
            $this->em->flush();
        }
        $this->em->clear();
        return ;

    }

}