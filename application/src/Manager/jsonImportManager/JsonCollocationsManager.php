<?php

namespace App\Manager\jsonImportManager;

use App\Entity\Collocation;
use App\Entity\ExempleCollocation;
use App\Entity\Fonction;
use App\Entity\LexicalEntry;
use App\Entity\SourceExemple;

use Doctrine\ORM\EntityManagerInterface;

class JsonCollocationsManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function importCollocations($collocations)
    {
        echo "import Collocations, Exemples" . "\n". "\n";

        foreach($collocations as $col){

            $collocation = New Collocation();

            $collocation->setStructureGlobale($col->structureGlobale);
            $collocation->setValue($col->value);
            $collocation->setStructureColloc($col->structureColloc);
            $collocation->setPropSyntax($col->propSyntax);
            $collocation->setFonctionLexicale($col->fonctionLexicale);

            $collocation->setLemmaBase($col->lemmaBase);
            $collocation->setLemmaCollocatif($col->lemmaCollocatif);
            $collocation->setComplement($col->complement);

            $collocation->setPatronParadigme($col->patronParadigme);
            $collocation->setPatronFixe($col->patronFixe);
            $collocation->setPartieDiscours($col->partieDiscours);

            $collocation->setNbOccurrences($col->metrics->nbOccurrences);
            $collocation->setLogLike($col->metrics->logLike);
            $collocation->setFrequence($col->metrics->frequence);
            $collocation->setNbDisciplines($col->metrics->nbDisciplines);

            if($col->lexicalEntryBaseId != null){
                $entryBase = $this->em->getRepository(LexicalEntry::class)->findOneByAcceptionUniq($col->lexicalEntryBaseId);
                $collocation->setLexicalEntryBase($entryBase);
            }

            if($col->lexicalEntryCollocatifId != null){
                $entryCollocatif = $this->em->getRepository(LexicalEntry::class)->findOneByAcceptionUniq($col->lexicalEntryCollocatifId);
                $collocation->setLexicalEntryCollocatif($entryCollocatif);
            }

            if(isset($col->exemples)){
                foreach($col->exemples as $ex){
                    $exemple = new ExempleCollocation();
        
                    $exemple->setStructure($ex->structure);
                    $exemple->setContexteGauche($ex->contexteGauche);
                    $exemple->setPivot($ex->pivot);
                    $exemple->setContexteDroit($ex->contexteDroit);
        
                    $sourceExemple= $this->em->getRepository(SourceExemple::class)->findOneByvalue($ex->source);
                    $exemple->setSourceExemple($sourceExemple);
                    $exemple->setPartieTextuelle($ex->partieTextuelle);

                    $exemple->setCollocation($collocation);
                    $this->em->persist($exemple);
                }
            }

            if(isset($col->fonctions)){
                foreach($col->fonctions as $funcLabel){
                    $fonction = $this->em->getRepository(Fonction::class)->findOneByLabel($funcLabel);
                    $collocation->addFonction($fonction);
                }
            }
            $this->em->persist($collocation);
        }
        $this->em->flush();
        $this->em->clear();
    }

}