<?php 

namespace App\Manager\jsonImportManager;

class JsonEncoderDocumentation {

public function docAll(){

    $docJson = [
      "entreesLexicales" =>  $this->docJsonLexicalEntries(),
      "collocations" =>  $this->docJsonCollocations(),
      "traductions" =>  $this->docJsonTranslations(),
      "fonctionsRhetoriques" =>  $this->docJsonFonctions(),
      "routines" => $this->docJsonRoutines(),
      "classesSemantiques" =>  $this->docJsonSemanticClasses(),
      "sourcesExemples" =>   $this->docJsonExampleSource()
    ];
    return $docJson;
}

public function docJsonLexicalEntries(){

    $lexicalEntries= [];
    $lexicalEntry = [];
    $lexicalEntry["acceptionUniq"] = [
                                    "type" => "Clef primaire", 
                                    "required" => true ,
                                    "description" => "lexicalEntry.AcceptionUniq sert d'identifiant unique. Construit sur le modèle : lemme_categorie_numeroOptionnel'",
                                    ];
    $lexicalEntry["cat"] = [  
                                "type" => "Clef étrangère de SyntacCat (SyntacCat.value)" , 
                                "required" => true ,
                                "description" => "Identifiant d'une catégorie syntaxique (A : adjectif, ADV : adverbe, N : nom ou V : verbe)",
                                "comment" => "Lors de l'import, un premier passage sur le JSON génère les entités CatégorieSyntaxique (qui n'ont qu'un id et une value) à partir d'un tri des valeurs distincte de cette propriété)."
                                ];
    $lexicalEntry["lemma"] = [  
                                "type" => "Clef étrangère de Lemma (Lemma.value)", 
                                "required" => true ,
                                "description" => "A la fois Valeur (string) et identifiant du Lemma associé à la LexicalEntry",
                                "comment" => "Lors de l'import, un premier passage sur le JSON génère les entités Lemma (qui n'ont qu'un id et une value) à partir d'un tri des valeurs distincte de cette propriété)."
                                ];


    $lexicalEntry["lexicalType"] = [  
                                    "type" => "Clef étrangère de LexicalType (LexicalType.value)",
                                    "required" => true ,
                                    "description" => "A la fois Valeur (string) et identifiant du LexicalType associé à la LexicalEntry (2 valeurs : Monolexical / Polylexical)",
                                    "comment" => "Lors de l'import, un premier passage sur le JSON génère les entités lexicalType (qui n'ont qu'un id et une value) à partir d'un tri des valeurs distincte de cette propriété)."
                                    ];

    $lexicalEntry["semanticClass"] =[
                                        "type" => "Clef étrangère de SemanticClass (SemanticClass.value)",
                                        "required" => true ,
                                        "description" => "associe l'entrée à une classe sémantique",
                                        "comment" => "SemanticClass.value sert de clef. Les lexicalEntry sont aussi associées à une instance de SemanticSubClassSubClass (dont une instance est générée lors de l'import), en croisant cat + semanticClass + SemanticSubClass" ,
                                        ];
    $lexicalEntry["semanticSubClass"] = [
                                        "type" => "Clef étrangère de SemanticSubClass (SemanticSubClass.value)",
                                        "required" => true ,
                                        "description" => "associe l'entrée à une sous classe sémantique",
                                        "comment" => "SemanticClass.value sert de clef. Les lexicalEntry sont aussi associées à une instance de SemanticSubClassSubClass (dont une instance est générée lors de l'import), en croisant cat + semanticClass + SemanticSubClass",
                                        ];

    $lexicalEntry["fonctionId"] = [
                                    "type" => "Clef étrangère de Fonction (fonction.label)",
                                    "description" => "associe l'entrée à une fonction rhétorique",
                                    "required" => false ,
                                    "comment" => "fonction.label sert de clef vers une instance de 'fonctionSpe' dans ce JSON",
                                    ];

    $lexicalEntry["varGraph"] = ["type" => "propriété", "required" => false , "description" => "?????" ];
    $lexicalEntry["propMorph"] = ["type" => "propriété", "required" => false , "description" => "?????" ];
    $lexicalEntry["propSynt"] =  ["type" => "propriété", "required" => false , "description" => "?????" ];
    $lexicalEntry["verbConstruction"] = ["type" => "propriété", "required" => false , "description" => "?????" ];
    $lexicalEntry["sujetComplement"] = ["type" => "propriété", "required" => false , "description" => "?????" ];
    $lexicalEntry["meaning"] = ["type" => "propriété", "required" => false , "description" => "?????" ];
    $lexicalEntry["source"] = ["type" => "propriété", "required" => false , "description" => "?????" ];
    $lexicalEntry["comment"] = ["type" => "propriété", "required" => false , "description" => "?????" ];

    $stats = [];
    $stats["DESCRIPTION"] = [
        "type" => "Entité liée à LexicalEntry (One to One)",
        "description" => "statisiques de la lexicalEntry",
        "required" => false ,
        "comment" => "l'entité Metrique est générée et reliée à la LexicalEntry qui la contient lors de l'import",
    ];

    $stats["ratio"] = ["type" => "propriété", "required" => true , 
                        "description" => "résultat du ratio de la fréquence relative (par million de mots) dans le corpus d'analyse (CA) sur le corpus de constraste (CC)" 
                        ];
    $stats["ccbym"] = ["type" => "propriété", "required" => true , 
                        "description" => "fréquence relative (par million de mot) dans le corpus de contraste" 
                        ];
    $stats["cabym"] = ["type" => "propriété", "required" => true , 
                        "description" => "fréquence relative (par million de mot) dans le corpus d'analyse" 
                        ];
    $stats["cafreq"] = ["type" => "propriété", "required" => true , 
                        "description" => "fréquence absolue (nombre d'occurrences) dans le corpus d'analyse" 
                        ];
    $stats["ngrammes"] = ["type" => "propriété", "required" => true , 
                        "description" => "nombre d'apparition du lemme dans un n-gramme validé manuellement comme expression polylexicale contigüe (ex: 'point' dans 'point de vue') afin de soustraire ce nombre d'occurrences à la fréquence totale." 
                        ];
    $stats["repartitionDisciplines"] = ["type" => "propriété", "required" => true , 
                        "description" => "nombre de disciplines différentes dans lesquelles le lemme apparaît" 
                        ];
    $stats["repartitionTranches"] = ["type" => "propriété", "required" => true , 
                        "description" => "nombre de tranches différentes du corpus d'analyse (divisé en 100 tranches) dans lesquelles le lemme apparaît}</distribTranches" 
                        ];
    $stats["disciplinesSpec"] = ["type" => "propriété", "required" => true , 
                        "description" => "nombre de disciplines pour lesquelles la fréquence relative du lemme est supérieure par rapport au corpus de contraste" 
                        ];
    $stats["freqParDisciplines"] = [];
    $stats["freqParDisciplines"]["description"] = "fréquence absolue du lemme dans le sous-corpus disciplinaire";
    $stats["freqParDisciplines"]["anthropo"] = ["type" => "propriété", "required" => true , 
                                                "description" => "fréquence absolue dans le sous-corpus d'anthropologie" 
                                            ];
    $stats["freqParDisciplines"]["eco"] = ["type" => "propriété", "required" => true , 
                                                "description" => "etc" 
                                            ];
    $stats["freqParDisciplines"]["geo"] = ["type" => "propriété", "required" => true , 
                                                "description" => "etc" 
                                            ];
    $stats["freqParDisciplines"]["histoire"] = ["type" => "propriété", "required" => true , 
                                                "description" => "etc" 
                                            ];
    $stats["freqParDisciplines"]["ling"] = ["type" => "propriété", "required" => true , 
                                                "description" => "etc" 
                                            ];
    $stats["freqParDisciplines"]["psycho"] = ["type" => "propriété", "required" => true , 
                                                "description" => "etc" 
                                            ];
    $stats["freqParDisciplines"]["sciedu"] = ["type" => "propriété", "required" => true , 
                                                "description" => "etc" 
                                            ];
    $stats["freqParDisciplines"]["sciepo"] = ["type" => "propriété", "required" => true , 
                                                "description" => "etc" 
                                            ];
    $stats["freqParDisciplines"]["scinfo"] = ["type" => "propriété", "required" => true , 
                                                "description" => "etc" 
                                            ];
    $stats["freqParDisciplines"]["socio"] = ["type" => "propriété", "required" => true , 
                                                "description" => "etc" 
                                            ];
    $lexicalEntry["metrics"] =  $stats;
    $lexicalEntry["exemples"] = [];
    $exemple = [];
    $exemple["DESRIPTION"] =  [
        "type" => "Entité liée à LexicalEntry (One LexicalEntry to Many Exemples )",
        "description" => "exemples de la lexicalEntry",
        "required" => false ,
        "comment" => "chaque entité Exemple est générée et reliée à la LexicalEntry qui la contient lors de l'import",
    ];
    $exemple["source"] = [   "type" => "Clef étrangère (string) de SourceExemple",
                             "required" => false , 
                            "description" => "sourceExemple.code sert de clef" 
                         ];
    $exemple["contexteGauche"] =    ["type" => "propriété", "required" => true ];
    $exemple["pivot"] =             ["type" => "propriété", "required" => true ];
    $exemple["contexteDroit"] =     ["type" => "propriété", "required" => true ];
    $exemple["partieTextuelle"] =   ["type" => "propriété", "required" => false , "description" => "?????" ];  ;
    $lexicalEntry["exemples"][] =   $exemple ;
    $lexicalEntries["LexicalEntry"] = $lexicalEntry;

    return  $lexicalEntries;
    }
############################### COLLOCATION ############################### 

public function docJsonCollocations(){

    $collocations = [];
    $collocation = [];
    // $collocation["id"] = $col->getId();
    $collocation["value"] = [   "type" => "Clef primaire", 
                                "required" => true, 
                                "description" => "forme typique de la collocation (identique à structureGlobale) + éventuel numéro si plusieurs collocations partagent la même forme typique :  ex 'non négligeable 2' "];    
    
    $collocation["structureGlobale"] =  ["type" => "propriété", "required" => true , "description" => "?????" ];

    $collocation["structureColloc"] =   ["type" => "propriété", "required" => true , "description" => "????? structure syntaxique ?" ];
    $collocation["propSyntax"] =        ["type" => "propriété", "required" => false , "description" => "?????" ];
    $collocation["fonctionLexicale"] =  ["type" => "propriété", "required" => false , "description" => "?????" ];

    $collocation["lexicalEntryBaseId"] =  [ "type" => "Clef étrangère de LexicalEntry (LexicalEntry.acceptionUniq)", 
                                            "required" => false, 
                                            "description" => "Lien vers la lexicalEntry formant la base de la Collocation",
                                            "comment" => "Si pas de LexicalEntry associée, le lemmaBase existe néanmoins (mais n'est pas décrit outre mesure dans les données)"
                                            ];    
    $collocation["lexicalEntryCollocatifId"] =  [   "type" => "Clef étrangère de LexicalEntry (LexicalEntry.acceptionUniq)", 
                                                    "required" => false, 
                                                    "description" => "Lien vers la lexicalEntry formant le Collocatif",
                                                    "comment" => "Si pas de LexicalEntry associée, le lemmaCollocatif existe néanmoins (mais n'est pas décrit outre mesure dans les données)"
                                                ];    

    $collocation["lemmaBase"] =  ["type" => "propriété", "required" => true , "description" => "Lemme formant la base de la collocation" ];
    $collocation["lemmaCollocatif"] =  ["type" => "propriété", "required" => true , "description" => "Lemme formant le collocatif de la collocation" ];

    $collocation["fonctions"] = []; 
    $collocation["fonctions"]["DESCRIPTION"] =  [
                                                "type" => "Liste de clefs étrangères de Fonction (fonction.label)",
                                                "description" => "associe la collocation à une ou plusieurs fonction rhétorique",
                                                "required" => false ,
                                                "comment" => "fonction.label sert de clef vers une instance de 'fonctionSpe' dans ce JSON",
                                                ];
    $collocation["fonctions"]["Exemple de valeurs"] = [ "methodes","analyses" ,"miseenevidence"];



    $collocation["complement"] = ["type" => "propriété", "required" => false , "description" => "?????" ];
    $collocation["patronParadigme"] = ["type" => "propriété", "required" => false , "description" => "?????" ];
    $collocation["patronFixe"] = ["type" => "propriété", "required" => false , "description" => "?????" ];
    $collocation["partieDiscours"] = ["type" => "propriété", "required" => false , "description" => "?????" ];

    $collocation["metrics"] = [];

    $collocation["metrics"]["DESCRIPTION"] = [
                                            "comment" => "propriétés directes de Collocations (ne forment pas une entité indépendante, mais mises à part pour clarté du JSON)"
                                        ];

    $collocation["metrics"]["nbOccurrences"] = "fréquence absolue (nombre d'occurrences) dans le corpus d'analyse";
    $collocation["metrics"]["logLike"] = "Log-likelihood : probabilité que les deux mots (base et collocatif) sont utilisés ensemble plus souvent que le hasard.";
    $collocation["metrics"]["frequence"] = "fréquence d'appariton de la collocation dans le corpus d'analyse (divisé en 100 tranches). Exprimée par les valeurs : MF, F, TF. ";
    $collocation["metrics"]["nbDisciplines"] = "nombre de disciplines différentes dans lesquelles la collocation apparaît"; 


    $collocation["exemples"] = [];
    $exemple = [];

    $exemple["DESRIPTION"] =  [
        "type" => "Entité liée à Collocation (One Collocation to Many Exemples )",
        "description" => "exemples de la Collocation",
        "required" => false ,
        "comment" => "chaque entité Exemple est générée et reliée à la Collocation qui la contient lors de l'import",
    ];
    // $exemple["id"] = $ex->getId();
    $exemple["source"] = [   "type" => "Clef étrangère (string) de SourceExemple",
                             "required" => false , 
                            "description" => "sourceExemple.code sert de clef" 
                         ];
    $exemple["structure"] = ["type" => "propriété", "required" => true, "comment" => "identique à Collocation.structureColloc " ];
    $exemple["contexteGauche"] =    ["type" => "propriété", "required" => false ];
    $exemple["pivot"] =             ["type" => "propriété", "required" => true ];
    $exemple["contexteDroit"] =     ["type" => "propriété", "required" => true ];
    $exemple["partieTextuelle"] =   ["type" => "propriété", "required" => false , "description" => "?????" ];  ;
    $collocation["exemples"][] = $exemple ;

    $collocations["collocation"] = $collocation;

    return $collocations;
}

############################### TRANSLATIONS ############################### 

public function docJsonTranslations(){
    

    $lexicalTranslations["LangCode"] = [];

    $lexicalTranslations["LangCode"]["DESCRIPTION"] = "Toutes les traductions sont regroupées dans un array sous un ID correspondant au code ISO de la langue";
    $lexicalTranslation = [];
    // $lexicalTranslation['id'] = $trad->getId();
    $lexicalTranslation['nbOcc'] =  ["type" => "propriété", "required" => true, "comment" => "jamais null, mais peut-être de 0" ];

    $lexicalTranslation['lemmaTrad'] = [  
                                        "type" => "Clef étrangère de LemmaTrad (LemmaTrad.value)", 
                                        "required" => true ,
                                        "description" => "A la fois Valeur (string) et identifiant du Lemma associé à la LexicalEntryTRad",
                                        "comment" => "Relation One lemmaTrad to Many LexicalEntryTrad Lors de l'import, un premier passage sur le JSON génère les entités LemmaTrad (qui n'ont qu'un id et une value, et une langue qui est celle de l'objet parent) à partir d'un tri des valeurs distincte de cette propriété)."
                                        ];

    $lexicalTranslation['lexicalEntryId'] =  [  
                                            "type" => "Clef étrangère de la LexicalEntry traduite (LexicalEntry.acceptionUniq)", 
                                            "required" => false,
                                            "comment" => "relation One LexicalEntry to One LexicalEntryTrad"
                                            ];



    $lexicalTranslation['collocationId'] =   [  
                                            "type" => "Clef étrangère de la Collocation traduite (Collocation.value)", 
                                            "required" => false,
                                            "comment" => "relation One Collocation to One LexicalEntryTrad. Janv 2024 : aucune utilisation"
                                            ];

    $lexicalTranslation['synonyms'] = ["type" => "propriété", "required" => false, "comment" => "peut contenir des valeurs mutliples séparées par des ','" ];
    $lexicalTranslation['exempleSource'] = ["type" => "propriété", "required" => false];
    $lexicalTranslation['exempleCible'] =  ["type" => "propriété", "required" => false];
    $lexicalTranslation['comment'] =  ["type" => "propriété", "required" => false];
    $lexicalTranslation['usageGeneral'] = ["type" => "propriété", "required" => false];
    $lexicalTranslation['usageSpecifique'] =  ["type" => "propriété", "required" => false];
    $lexicalTranslation['contexte'] =  ["type" => "propriété", "required" => false];


    $lexicalTranslations["LangCode"]["LexicalEntryTrad"] = $lexicalTranslation;

    return $lexicalTranslations;

    }
    ############################### FONCTIONS ############################### 


    public function docJsonFonctions(){

    $genFonctions = [];
    $genFonctions["fonctions"] = [];

    $gen = [];
    $gen["id"] = [  "type" => "Clef primaire (fonction.generalF)", 
                    "required" => true, 
                    "comment" => "Dans la BDD du LST, fonctionGénérale et fonctionSpécifique sont réunies sur une même ligne : la clef de fonction.generalF est répétée pour chaque fonctionSpe (fonction.generalF , fonction.label). Les définitions de fonctionGénérale et fonctionSpécifique ne sont pas stockées en base mais comme des messages du système de traduction, appelées à partir des ID de fonctionGénérale et fonctionSpécifique"
                ];    

    $gen["definition"] =  [  "type" => "propriété", 
                            "required" => true, 
                            "comment" => "Attention, n'est pas stocké en base, est stocké comme la chaîne de traduction associée à fonction.generalF"];    
    $gen["fonctionsSpe"] = []; 
    
    $spe = [];
    $spe["id"] =  [ "type" => "Clef primaire (fonction.label)", 
                    "required" => true, 
                    "comment" => "Dans la BDD du LST, fonctionGénérale et fonctionSpécifique sont réunies sur une même ligne : la clef de fonction.generalF est répétée pour chaque fonctionSpe (fonction.generalF , fonction.label). Les définitions de fonctionGénérale et fonctionSpécifique ne sont pas stockées en base mais comme des messages du système de traduction, appelées à partir des ID de fonctionGénérale et fonctionSpécifique"
                    ];    

    $spe["definition"] = [  "type" => "propriété", 
    "required" => true, 
    "comment" => "Attention, n'est pas stocké en base, est stocké comme la chaîne de traduction associée à fonction.generalF"
                        ];    
    $spe["exemple"] = ["type" => "propriété", "required" => true];

    $gen["fonctionsSpe"][] = $spe; 
    $genFonctions["fonctions"] = $gen;


    return $genFonctions;

    }
 
############################### ROUTINES ############################### 

public function docJsonRoutines(){

    $routines = [];
    $routine = [];
    $routine["code"] = ["type" => "Clef primaire (routine.code)", 
                        "required" => true, 
                        ];    
      
    $routine["type"] = ["type" => "propriété", "required" => true];
    $routine["subType"] = ["type" => "propriété", "required" => false];
    $routine["subSubType"] = ["type" => "propriété", "required" => false];

    $routine["fonctionId"] = ["type" => "Clef étrangère de Fonction (fonction.label)",
                            "description" => "associe la routine à une fonction rhétorique",
                            "required" => true ,
                            "comment" => "fonction.label sert de clef vers une instance de 'fonctionSpe' dans ce JSON",
                            ];

    $routine["modele"] = ["type" => "propriété", "required" => true];
    $routine["realisation"] = ["type" => "propriété", "required" => true, "comment" => "plusieurs realisations sont concaténées avec & comme séparateur." ];
    $routine["occurrences"] =  ["type" => "propriété", "required" => true, "comment" => "fréquence absolue (nombre d'occurrences) dans le corpus d'analyse"]; 
    $routine["patron"] = ["type" => "propriété", "required" => false];

    $routine["exemples"] = [];
    $exemple = [];


    $exemple["DESRIPTION"] =  [
        "type" => "Entité liée à Routine (One Routine to Many Exemples )",
        "description" => "exemples de la Routine",
        "required" => false ,
        "comment" => "chaque entité Exemple est générée et reliée à la Routine qui la contient lors de l'import",
    ];
    $exemple["source"] = [   "type" => "Clef étrangère (string) de SourceExemple",
                             "required" => false , 
                            "description" => "sourceExemple.code sert de clef" 
                         ];
    $exemple["type"] = ["type" => "propriété", "required" => true ];
    $exemple["content"] = ["type" => "propriété", "required" => true ];
    $exemple["simplicity"] = ["type" => "propriété", "required" => false ];
    $routine["exemples"][] = $exemple;
    
    $routines["routines"] = $routine;
    
    return $routines ;
    }
############################### SEMANTIC CLASSES ############################### 

public function docJsonSemanticClasses(){

    $semClasses = [];
    
    $semClass = [];
    $semClass["id"] = ["type" => "Clef primaire (semanticClass.value)", 
                        "required" => true 
                        ];    

    $semClass["definition"] = [];
    $semClass["definition"]["EXPLICATION"] = "Une même classe sémantique peut avoir plusieurs définitions : une par Catégorie Syntaxique associée (SyntacCat). Dans la BDD dy LST,  semanticClass et semanticSubClass n'ont qu'une .value et un .id : les defintions, catégories synaxiques et critere de test, qui vont de pair, sont tous stockés dans la table semanticClassSubClass."; 
    $description["cat"] = ["type" => "Clef secondaire de SyntacCat (SyntacCat.value)", 
                            "required" => true,
                            "comment" => "Provient de la table semanticClassSubClass"
                            ];    
    $description["description"] = ["type" => "propriété", "required" => true, "comment" => "Provient de la table semanticClassSubClass"];
    $description["critereTest"] = ["type" => "propriété", "required" => true, "comment" => "Provient de la table semanticClassSubClass"];
    $semClass["definition"][] = $description;

    $semClass["sousClassesSemantiques"] = [];
    $subSemClass = [];
    $subSemClass["id"] = ["type" => "Clef primaire (semanticSubClass.value)", 
                        "required" => true 
                        ];   
    $subSemClass["definition"] = [];
    $subSemClass["definition"]["EXPLICATION"] = "Une même sous-classe sémantique peut avoir plusieurs définitions : une par Catégorie Syntaxique associée (SyntacCat). Dans la BDD dy LST,  semanticClass et semanticSubClass n'ont qu'une .value et un .id : les defintions, catégories synaxiques et critere de test, qui vont de pair, sont tous stockés dans la table semanticClassSubClass."; 

    $subDescription["cat"] = ["type" => "Clef secondaire de SyntacCat (SyntacCat.value)", 
                            "required" => true,
                            "comment" => "Provient de la table semanticClassSubClass"
                            ];    
    $subDescription["description"] = ["type" => "propriété", "required" => true, "comment" => "Provient de la table semanticClassSubClass"];
    $subDescription["critereTest"] = ["type" => "propriété", "required" => true, "comment" => "Provient de la table semanticClassSubClass"];
    $subSemClass["definition"][] = $subDescription;

    $semClass["sousClassesSemantiques"][] = $subSemClass;

    $semClasses["SemanticClasses"] = $semClass;

        return $semClasses;
    
    }

############################### EXAMPLE SOURCE ############################### 

public function docJsonExampleSource(){

    $exemplesSources = [];
    $source = [];
    $source["id"] = ["type" => "Clef primaire (sourceExemple.value)", "required" => true ];    
    $source["descriptionBiblio"] = ["type" => "propriété", "required" => true ];
    //$exemplesSources["sourcesExemples"][] = $source;
    $exemplesSources["source"] = $source;

    return $exemplesSources;
    }
}