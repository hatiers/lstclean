<?php

namespace App\Manager\jsonImportManager;


use App\Entity\Exemple;
use App\Entity\Fonction;
use App\Entity\Lemma;
use App\Entity\LexicalEntry;
use App\Entity\LexicalType;
use App\Entity\Metriques;
use App\Entity\Source;
use App\Entity\SourceExemple;

use App\Manager\LexicalEntryManager;
use App\Manager\PropertyManager;
use Doctrine\ORM\EntityManagerInterface;


class JsonEntriesManager
{
    private $em;
    private $propertyManager;
    private $lexicalEntryManager;

    public function __construct(    EntityManagerInterface $em,
                                    PropertyManager $propertyManager ,
                                    LexicalEntryManager $lexicalEntryManager
    )
    {
        $this->em = $em;
        $this->propertyManager = $propertyManager;
        $this->lexicalEntryManager = $lexicalEntryManager;
    }

    public function importLemmas($lexicalEntries) {

        $lemmas = [];
        foreach($lexicalEntries as $entry){
            $lemmas[] = $entry->lemma;
        }
        $uniqueLemmas =  array_unique($lemmas);
        foreach($uniqueLemmas as $uniqueLemma){
            $lemma = new Lemma();
            $lemma->setValue($uniqueLemma);
            $this->em->persist($lemma);
        }
        $this->em->flush();
        $this->em->clear();
    }

    public function importLexicalTypes($lexicalEntries) {
        $lexicalTypes = [];
        foreach($lexicalEntries as $entry){
            $lexicalTypes[] = $entry->lexicalType;
        }
        $uniqueLexicalTypes =  array_unique($lexicalTypes);
        foreach($uniqueLexicalTypes as $uniqueLexicalType){
            $lexicalType = new LexicalType();
            $lexicalType->setValue($uniqueLexicalType);
            $this->em->persist($lexicalType);
        }
        $this->em->flush();
        $this->em->clear();
    }

    public function importSources($lexicalEntries) {
        $sources = [];
        foreach($lexicalEntries as $entry){
            $sources[] = $entry->source;
        }
        $uniqueSources =  array_unique($sources);
        foreach($uniqueSources as $uniqueSource){
            $source = new Source();
            $source->setValue($uniqueSource);
            $this->em->persist($source);
        }
        $this->em->flush();
        $this->em->clear();
    }


    public function importLexicalEntries($lexicalEntries)
    {
        echo "import LexicalEntries, Metrics, Exemples" . "\n". "\n";

        foreach($lexicalEntries as $entry){

            $lemmaValue = $entry->lemma;
            $catValue = $entry->cat;
            $typeValue = $entry->lexicalType;
            $acceptionValue = $entry->acceptionUniq;

            $propMorph = $entry->propMorph;
            $varGraph = $entry->varGraph;
            $propSynt = $entry->propSynt;
            $verbConstruction = $entry->verbConstruction;
            $meaning = $entry->meaning;
            $sourceValue = $entry->source;
            $semanticClassValue = $entry->semanticClass;
            $semanticSubClassValue = $entry->semanticSubClass;
            
            $lemma = $this->propertyManager->getLemmaOrCreate($lemmaValue);
            $cat = $this->propertyManager->getCatOrCreate($catValue);
            $lexicalType = $this->propertyManager->getLexicalTypeOrCreate($typeValue);
            $source = $this->propertyManager->getSourceOrCreate($sourceValue);

            $semanticClass = $this->propertyManager->getSClassOrCreate($semanticClassValue);
            $semanticSubClass = $this->propertyManager->getSubClassOrCreate($semanticSubClassValue);

            $currentEntry = $this->lexicalEntryManager->create($cat, $lexicalType, $lemma, $acceptionValue, $propMorph, $varGraph, $propSynt, $meaning, $source, $semanticClass, $semanticSubClass, $verbConstruction);

            if(isset($entry->fonctionId)){
                $fonction = $this->em->getRepository(Fonction::class)->findOneByLabel($entry->fonctionId);
                $currentEntry->setFonction($fonction);
            }
                        
            if (isset($entry->metrics)){
                $currentMetrique = $this->createEntryMetrics($entry->metrics);
                $currentEntry->setMetriques($currentMetrique); 
            }
            else {
                // echo $entry->acceptionUniq . "  without metrics" . "\n";
            }

            if(isset($entry->exemples)){
                foreach($entry->exemples as $ex){
                    $exemple = new Exemple();
        
                    $exemple->setContexteGauche($ex->contexteGauche);
                    $exemple->setPivot($ex->pivot);
                    $exemple->setContexteDroit($ex->contexteDroit);
        
                    $sourceExemple= $this->em->getRepository(SourceExemple::class)->findOneByvalue($ex->source);
                    $exemple->setSourceExemple($sourceExemple);
                    $exemple->setPartieTextuelle($ex->partieTextuelle);

                    $exemple->setLexicalEntry($currentEntry);
                    $this->em->persist($exemple);
                }
            }
            else {
                // echo $entry->acceptionUniq . "  without exemples" . "\n";
            }
        }
        $this->em->flush();
        $this->em->clear();
    }

    public function createEntryMetrics($metric) {

        // dd($metric->ratio);

        $metrique = New Metriques();
        $metrique->setRatio($metric->ratio);
        $metrique->setCcbym($metric->ccbym);
        $metrique->setCabym($metric->cabym);
        $metrique->setCafreq($metric->cafreq);
        $metrique->setNgrammes($metric->ngrammes);
        $metrique->setRepartitionDisciplines($metric->repartitionDisciplines);
        $metrique->setRepartitionTranches($metric->repartitionTranches);
        
        $metrique->setDisciplinesSpec($metric->disciplinesSpec);
        $metrique->setFreqAnthropo($metric->freqParDisciplines->anthropo);
        $metrique->setFreqEco($metric->freqParDisciplines->eco);
        $metrique->setFreqGeo($metric->freqParDisciplines->geo);
        $metrique->setFreqHistoire($metric->freqParDisciplines->histoire);
        $metrique->setFreqLing($metric->freqParDisciplines->ling);
        $metrique->setFreqPsycho($metric->freqParDisciplines->psycho);
        $metrique->setFreqSciedu($metric->freqParDisciplines->sciedu);
        $metrique->setFreqSciepo($metric->freqParDisciplines->sciepo);
        $metrique->setFreqScinfo($metric->freqParDisciplines->scinfo);
        $metrique->setFreqSocio($metric->freqParDisciplines->socio);

        $this->em->persist($metrique);
    
        return $metrique;    
    }


    // public function emptydb()
    // {
    //     $semanticClassSubClasses= $this->em->getRepository(SemanticClassSubClass::class)->findAll();
    //     foreach ($semanticClassSubClasses as $semanticClassSubClass) {
    //         // code...
    //         $this->em->remove($semanticClassSubClass);
    //     }
    //     $this->em->flush();
    // }
}
