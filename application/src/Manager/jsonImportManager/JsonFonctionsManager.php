<?php
namespace App\Manager\jsonImportManager;

use App\Entity\Fonction;
use Doctrine\ORM\EntityManagerInterface;

class JsonFonctionsManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function importFonctions($fonctions)
    {
        echo "import Fonctions" . "\n";

        foreach($fonctions as $f){
            $fonctionGen = $f->id;
            // $f->definition et $f->fonctionsSpe[i]->description sont ignorées : 
            // les définitions des fonctions sont gérées comme des chaînes de traduction dans /src/translations/messages.fr.yml
            // éventuellemment ... on pourrait les extraire et les inscrire dans messages.fr.yml  
            //
            foreach($f->fonctionsSpe as $fonctionSpe){
                $fonction = new Fonction();
                $fonction->setGeneralf($fonctionGen);
                $fonction->setLabel($fonctionSpe->id); // remnommer en LABEL
                $fonction->setExemple($fonctionSpe->exemple); // 
                $this->em->persist($fonction);
            }
        }
        $this->em->flush();
        $this->em->clear();
    return;
    }
    
    // public function emptydb()
    // {
    //     $semanticClassSubClasses= $this->em->getRepository(SemanticClassSubClass::class)->findAll();
    //     foreach ($semanticClassSubClasses as $semanticClassSubClass) {
    //         // code...
    //         $this->em->remove($semanticClassSubClass);
    //     }
    //     $this->em->flush();
    // }
}
