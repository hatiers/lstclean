<?php

namespace App\Manager\jsonImportManager;

use App\Entity\ExempleRoutine;
use App\Entity\Fonction;
use App\Entity\Routine;
use App\Entity\SourceExemple;

use Doctrine\ORM\EntityManagerInterface;

class JsonRoutinesManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function importRoutines($routines)
    {
        echo "import routines, Exemples" . "\n". "\n";

        foreach($routines as $rout){

            $routine = New Routine();

            if(! isset($rout->code)){
                dd($rout);
            } 

            $routine->setCode($rout->code);
            $routine->setType($rout->type);
            $routine->setSubType($rout->subType);
            $routine->setSubSubType($rout->subSubType);

            $routine->setModele($rout->modele);
            $routine->setRealisation($rout->realisation);
            $routine->setOccurrence($rout->occurrences);
            $routine->setPatron($rout->patron);


            if(isset($rout->exemples)){
                foreach($rout->exemples as $ex){
                    $exemple = new ExempleRoutine();
        
                    $exemple->setType($ex->type);
                    $exemple->setContent($ex->content);
                    $exemple->setSimplicity($ex->simplicity);
                            
                    $sourceExemple= $this->em->getRepository(SourceExemple::class)->findOneByvalue($ex->source);
                    $exemple->setSource($sourceExemple);

                    $exemple->setRoutine($routine);
                    $this->em->persist($exemple);
                }
            }
            if(isset($rout->fonctionId)){            
                $fonction = $this->em->getRepository(Fonction::class)->findOneByLabel($rout->fonctionId);
                $routine->setFonction($fonction);
            }
            $this->em->persist($routine);
        }
        $this->em->flush();
        $this->em->clear();
    }

}  
