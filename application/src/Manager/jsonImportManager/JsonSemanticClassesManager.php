<?php

namespace App\Manager\jsonImportManager;

use App\Entity\LexicalEntry;
use App\Entity\SemanticClass;
use App\Entity\SemanticClassSubClass;
use App\Entity\SemanticSubClass;
use App\Entity\SyntacCat;

use App\Manager\PropertyManager;
use Doctrine\ORM\EntityManagerInterface;

class JsonSemanticClassesManager
{
    private $em;
    private $propertyManager;


    public function __construct(    EntityManagerInterface $em,
                                    PropertyManager $propertyManager
    )
    {
        $this->em = $em;
        $this->propertyManager = $propertyManager;
    }

    public function importSemanticClasses($classes)
    {
        // $this->createSyntacCat($classes);
        echo "import SemanticClasses, SemanticSubClasses" . "\n";

        $syntacCategories = [];
        foreach($classes as $class){
            $semanticClass = new SemanticClass();
            $semanticClass->setValue($class->id);
            $this->em->persist($semanticClass);

            foreach($class->sousClassesSemantiques as $subclass){
                $semanticSubClass = new SemanticSubClass();
                $semanticSubClass->setValue($subclass->id);
                $this->em->persist($semanticSubClass);
            }
            // CARELESS ?
            foreach($class->definition as $def){
                $syntacCategories[] = $def->cat;
            }
        }
        $this->em->flush();
        $this->em->clear();

        echo "import distinct SyntacCat" . "\n";

        $uniqueCategories =  array_unique($syntacCategories);
        foreach($uniqueCategories as $category){
            $syntacCat = new SyntacCat();
            $syntacCat->setValue($category);
            $this->em->persist($syntacCat);
        }
        $this->em->flush();
        $this->em->clear();

        echo "semanticClassSubClass" . "\n";

        foreach($classes as $class){

            foreach($class->definition as $def){
                $classCat = $def->cat;

                foreach($class->sousClassesSemantiques as $subclass){
                    
                    foreach($subclass->definition as $subclassDef){

                        if($subclassDef->cat == $classCat){
                            // IL FAUT ABSOLUMENT QUE JE LES AI CREES AVANT, SINON EN CREE DE NOUVAUX .... 
                            $catEntity = $this->propertyManager->getCatOrCreate($classCat);

                            $semanticClassSubClass = new SemanticClassSubClass();
                            $semanticClassSubClass->setDescriptionClass($def->description);
                            $semanticClassSubClass->setTestAppartClass($def->critereTest);
                            $semanticClassSubClass->setDescriptionSubClass($subclassDef->description);
                            $semanticClassSubClass->setTestAppartSubClass($subclassDef->critereTest);
                            $semanticClassSubClass->setSyntacCatClassSubClass($catEntity);

                            $parentClass = $this->em->getRepository(SemanticClass::class)->findOneByValue($class->id);
                            $semanticClassSubClass->setSemanticClass($parentClass);
                            $parentSubClass = $this->em->getRepository(SemanticSubClass::class)->findOneByValue($subclass->id);
                            $semanticClassSubClass->setSemanticSubClass($parentSubClass);
                            
                            $this->em->persist($semanticClassSubClass);
                        }
                    }
                }
            }
        }

        $this->em->flush();
        $this->em->clear();
        return;
    }


    public function linkClassSubClassesToEntries(){

        $semanticClassSubclasses = $this->em->getRepository(SemanticClassSubClass::class)->findAll();

        foreach($semanticClassSubclasses as $semanticClassSubclass){
        
            $currentSemanticClass = $semanticClassSubclass->getSemanticClass();
            $currentSemanticSubClass = $semanticClassSubclass->getSemanticSubClass();
            $currentSyntacCat = $semanticClassSubclass->getSyntacCatClassSubClass();

            $lexicalEntries = $this->em->getRepository(LexicalEntry::class)->findLexicalEntryByClassSubClassSyntactCat($currentSemanticClass, $currentSemanticSubClass, $currentSyntacCat);
            
            foreach ($lexicalEntries as $lexicalEntry) {
                //echo("Passage REPO LE \n");
                $lexicalEntry->setSemanticClassSubClass($semanticClassSubclass);
                $this->em->persist($lexicalEntry);
                $semanticClassSubclass->addLexicalEntry($lexicalEntry);                
            }
            // besoin de vérifier que la classe et la sous-classe sont effectivement présentes en base
            if (!$currentSemanticSubClass && !$currentSemanticClass && !$currentSyntacCat) {
                echo("BLEME Classe inexistante : C: " . $semanticClassSubclass->getSemanticClass()->getValue() . " SC: ".$semanticClassSubclass->getSemanticSubClass()->getValue()." CAT: ".$semanticClassSubclass->getSyntacCatClassSubClass()->getValue()."\n" );
            }
        }
        $this->em->flush();
    }

    public function emptydb()
    {
        $semanticClassSubClasses= $this->em->getRepository(SemanticClassSubClass::class)->findAll();
        foreach ($semanticClassSubClasses as $semanticClassSubClass) {
            // code...
            $this->em->remove($semanticClassSubClass);
        }
        $this->em->flush();
    }
}
