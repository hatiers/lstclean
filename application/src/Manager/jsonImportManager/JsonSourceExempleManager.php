<?php

namespace App\Manager\jsonImportManager;

use App\Entity\SourceExemple;
use Doctrine\ORM\EntityManagerInterface;

class JsonSourceExempleManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function importSourceExemple($exampleSources)
    {
        echo "import Sources des Exemples" . "\n";

        foreach($exampleSources as $source){

            $sourceExemple = new SourceExemple();
            $sourceExemple->setValue($source->id);
            $sourceExemple->setDescriptionBiblio($source->descriptionBiblio);
            $this->em->persist($sourceExemple);
        }
        $this->em->flush();
        $this->em->clear();
        return;
    }
    
    // public function emptydb()
    // {
    //     $semanticClassSubClasses= $this->em->getRepository(SemanticClassSubClass::class)->findAll();
    //     foreach ($semanticClassSubClasses as $semanticClassSubClass) {
    //         // code...
    //         $this->em->remove($semanticClassSubClass);
    //     }
    //     $this->em->flush();
    // }
}
