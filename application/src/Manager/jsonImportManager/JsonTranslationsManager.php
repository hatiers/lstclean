<?php

namespace App\Manager\jsonImportManager;

use App\Entity\Collocation;
use App\Entity\Language;
use App\Entity\LemmaTrad;
use App\Entity\LexicalEntryTrad;
use App\Entity\LexicalEntry;
use Doctrine\ORM\EntityManagerInterface;

class JsonTranslationsManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function importLanguages($translations) {

        echo "import Languages". "\n". "\n";

        foreach($translations as $langCode => $trads){
            $language = new Language();
            $language->setShortName($langCode);
            $language->setName($langCode);
            $language->setTranslatedName($langCode); // peut-être à repasser en null ?

            $this->em->persist($language);
        }
        $this->em->flush();
        $this->em->clear();
    }
    

    public function importLemmaTrads($translations) {

        echo "import LemmaTrads". "\n". "\n";

        $languageList = [];
        foreach($translations as $langCode => $trads){
            $languageList[$langCode] = [];

            foreach($trads as $trad){
                $languageList[$langCode][] = $trad->lemmaTrad;
            }
        }
        foreach($languageList as $langCode => $languageLemmas){
            
            $uniqueLemmas =  array_unique($languageLemmas);
            foreach($uniqueLemmas as $uniqueLemma){
                $lemmaTrad = new LemmaTrad();
                $lemmaTrad->setValue($uniqueLemma);

                $langEntity = $this->em->getRepository(Language::class)->findOneByShortName($langCode);

                $lemmaTrad->setLanguage($langEntity);
                $this->em->persist($lemmaTrad);
            }
        }
        $this->em->flush();
        $this->em->clear();
    }

    public function importLexicalEntryTrads($translations)
    {
        echo "import LexicalEntryTrads". "\n". "\n";

        foreach($translations as $langCode => $trads){
        
            echo "language : " . $langCode . "\n";

            foreach($trads as $trad){

                $lexicalTranslation = New LexicalEntryTrad();
                $lexicalTranslation->setNbOcc($trad->nbOcc);
                if($trad->exempleSource){
                    $lexicalTranslation->setExampleSource($trad->exempleSource);
                }
                if($trad->exempleCible){
                    $lexicalTranslation->setExampleCible($trad->exempleCible);

                }
                $lexicalTranslation->setSynonyms($trad->synonyms);
                $lexicalTranslation->setComment($trad->comment);
                $lexicalTranslation->setUsageGeneral($trad->usageGeneral);
                $lexicalTranslation->setUsageSpecific($trad->usageSpecifique);
                $lexicalTranslation->setContext($trad->contexte);

                if($trad->lemmaTrad){
                    $lemmaTrad = $this->em->getRepository(LemmaTrad::class)->findOneByValue($trad->lemmaTrad);
                    $lexicalTranslation->setLemmaTrad($lemmaTrad);
                }
                if($trad->lexicalEntryId){
                    $lexicalEntry = $this->em->getRepository(LexicalEntry::class)->findOneByAcceptionUniq($trad->lexicalEntryId);
                    $lexicalTranslation->setLexicalEntry($lexicalEntry);
                }
                if($trad->collocationId){
                    $collocation = $this->em->getRepository(Collocation::class)->findOneById($trad->collocationId);
                    $lexicalTranslation->setCollocation($collocation);
                }
                $this->em->persist($lexicalTranslation);
            }
        }
        $this->em->flush();
        $this->em->clear();
    }
    
    // public function emptydb()
    // {
    //     $semanticClassSubClasses= $this->em->getRepository(SemanticClassSubClass::class)->findAll();
    //     foreach ($semanticClassSubClasses as $semanticClassSubClass) {
    //         // code...
    //         $this->em->remove($semanticClassSubClass);
    //     }
    //     $this->em->flush();
    // }
}
