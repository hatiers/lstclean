<?php

namespace App\Repository;

use App\Entity\Collocation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Collocation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Collocation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Collocation[]    findAll()
 * @method Collocation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CollocationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Collocation::class);
    }

    public function findByContaining($str)
    {
        $query = $this->createQueryBuilder('collocation')
          ->andWhere('collocation.value LIKE :stringSearch')
          ->orderBy('collocation.value', 'ASC')
          ->setParameter('stringSearch', '%'.$str.'%')
          ->getQuery();

        return $query->getResult();
    }

    public function findMissingExamples()
    {
        return $this->createQueryBuilder('c')
          ->andWhere('c.exempleCollocations IS EMPTY')
          ->orderBy('c.value', 'ASC')
          ->getQuery()
          ->getResult()
      ;
    }

    public function findMissingSource()
    {
        return $this->createQueryBuilder('c')
          ->join('c.exempleCollocations', 'collocs')
          ->andWhere('collocs.sourceExemple IS NULL')
          ->orderBy('c.value', 'ASC')
          ->getQuery()
          ->getResult()
      ;
    }

    public function findCycle()
    {
        return $this->createQueryBuilder('c')
          ->andWhere('c.lexicalEntryBase = c.lexicalEntryCollocatif')
          ->orderBy('c.value', 'ASC')
          ->getQuery()
          ->getResult()
      ;
    }

}
