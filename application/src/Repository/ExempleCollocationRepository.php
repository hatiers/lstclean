<?php

namespace App\Repository;

use App\Entity\ExempleCollocation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExempleCollocation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExempleCollocation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExempleCollocation[]    findAll()
 * @method ExempleCollocation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExempleCollocationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExempleCollocation::class);
    }

    // /**
    //  * @return ExempleCollocation[] Returns an array of ExempleCollocation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExempleCollocation
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
