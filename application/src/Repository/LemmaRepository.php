<?php

namespace App\Repository;

use App\Entity\Lemma;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Lemma|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lemma|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lemma[]    findAll()
 * @method Lemma[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LemmaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lemma::class);
    }


    // /**
    //  * @return Lemma[] Returns an array of Lemma objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Lemma
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
