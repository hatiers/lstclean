<?php

namespace App\Repository;

use App\Entity\LemmaTrad;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<LemmaTrad>
 *
 * @method LemmaTrad|null find($id, $lockMode = null, $lockVersion = null)
 * @method LemmaTrad|null findOneBy(array $criteria, array $orderBy = null)
 * @method LemmaTrad[]    findAll()
 * @method LemmaTrad[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LemmaTradRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LemmaTrad::class);
    }

    public function findByContaining($str)
    {
        $query = $this->createQueryBuilder('lemmatrad')
          ->join('lemmatrad.lexicalEntryTrads', 'entrytrad')
          ->andWhere('lemmatrad.value LIKE :stringSearch')
          ->orWhere('entrytrad.synonyms LIKE :stringSearch')
          ->setParameter('stringSearch', '%'.$str.'%')
          ->getQuery();

        return $query->getResult();
    }


    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(LemmaTrad $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(LemmaTrad $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return LemmaTrad[] Returns an array of LemmaTrad objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LemmaTrad
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
