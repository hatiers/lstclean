<?php

namespace App\Repository;

use App\Entity\LexicalEntry;
use App\Entity\SemanticClass;
use App\Entity\SemanticSubClass;
use App\Entity\SyntacCat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LexicalEntry|null find($id, $lockMode = null, $lockVersion = null)
 * @method LexicalEntry|null findOneBy(array $criteria, array $orderBy = null)
 * @method LexicalEntry[]    findAll()
 * @method LexicalEntry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LexicalEntryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LexicalEntry::class);
    }

    public function findByContaining($str)
    {
        $query = $this->createQueryBuilder('lexicalEntry')
          ->join('lexicalEntry.lemma', 'lemma')
          ->join('lexicalEntry.metriques', 'metriques')
          ->andWhere('lemma.value LIKE :stringSearch')
          ->orWhere('lexicalEntry.meaning LIKE :stringSearch')
          ->orderBy('metriques.cafreq', 'DESC')
          ->setParameter('stringSearch', '%'.$str.'%')
          ->getQuery();

        return $query->getResult();
    }

    public function findByIncludeLemma($lemmavalue)
    {
        return $this->createQueryBuilder('e')
        ->select('e')
        ->join('e.lemma', 'l')
        ->andWhere('l.value LIKE :fuzzyval')
        ->andWhere('l.value != :exactvalue')
        ->setParameter('fuzzyval', '%'.$lemmavalue.'%')
        ->setParameter('exactvalue', $lemmavalue)
        ->orderBy('e.id', 'ASC')
        ->getQuery()
        ->getResult();
    }
    public function findByUnderRatio($ratio)
    {
        return $this->createQueryBuilder('lexicalEntry')
        ->select('count(lexicalEntry)')
        ->join('lexicalEntry.metriques', 'metriques')
        ->andWhere('metriques.ratio < :currentRatio')
        ->setParameter('currentRatio', $ratio)
        ->getQuery()
        ->getSingleScalarResult();
    }

    public function findByUnderCaFreq($frequency)
    {
        return $this->createQueryBuilder('lexicalEntry')
        ->select('count(lexicalEntry)')
        ->join('lexicalEntry.metriques', 'metriques')
        ->where('metriques.cafreq < :frequency')
        ->setParameter('frequency', $frequency)
        ->getQuery()
        ->getSingleScalarResult();
    }

    public function findByNoMetrics()
    {
        return $this->createQueryBuilder('lexicalEntry')
        ->select('count(lexicalEntry)')
        ->where('lexicalEntry.metriques IS NULL')
        ->getQuery()
        ->getSingleScalarResult();
    }

    public function findLexicalEntryByClassSubClassSyntactCat(SemanticClass $sc, SemanticSubClass $ssc, SyntacCat $cat)
    {
        return $this->createQueryBuilder('lexicalEntry')
        ->select('lexicalEntry')
        ->andWhere('lexicalEntry.semanticClass = :semantiClass')
        ->andWhere('lexicalEntry.semanticSubClass = :semanticSubClass')
        ->andWhere('lexicalEntry.cat = :syntacCat')
        ->setParameter('semantiClass', $sc)
        ->setParameter('semanticSubClass', $ssc)
        ->setParameter('syntacCat', $cat)
        ->getQuery()
        ->getResult();
    }

    public function findMissingExamples()
    {
        return $this->createQueryBuilder('l')
          ->andWhere('l.exemples IS EMPTY')
          ->orderBy('l.acceptionUniq', 'ASC')
          ->getQuery()
          ->getResult()
      ;
    }

    public function findMissingSource()
    {
        return $this->createQueryBuilder('l')
          ->join('l.exemples', 'e')
          ->andWhere('e.sourceExemple IS NULL')
          ->orderBy('l.acceptionUniq', 'ASC')
          ->getQuery()
          ->getResult()
      ;
    }

    public function findMissingMetriques()
    {
        return $this->createQueryBuilder('l')
          ->andWhere('l.metriques IS NULL')
          ->orderBy('l.acceptionUniq', 'ASC')
          ->getQuery()
          ->getResult()
      ;
    }

    public function findEntryConnected()
    {
        return $this->createQueryBuilder('l')
          ->leftjoin('l.collocationsBase', 'base')
          ->leftjoin('l.collocationsCollocatif', 'collocatif')
          ->andWhere('base.lexicalEntryCollocatif IS NOT NULL OR collocatif.lexicalEntryBase IS NOT NULL')
          ->orderBy('l.acceptionUniq', 'ASC')
          ->getQuery()
          ->getResult()
      ;
    }

    public function findPolylexicals()
    { 
        return $this->createQueryBuilder('l')
          ->leftjoin('l.lexicalType', 'lexicalType')
          ->andWhere('lexicalType.value = :type')
          ->orderBy('l.acceptionUniq', 'ASC')
          ->setParameter('type', "polylexical")
          ->getQuery()
          ->getResult()
      ;
    }
}
