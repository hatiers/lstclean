<?php

namespace App\Repository;

use App\Entity\LexicalEntryTrad;


//Added... 
use App\Entity\Language;
use App\Entity\LemmaTrad;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<LexicalEntryTrad>
 *
 * @method LexicalEntryTrad|null find($id, $lockMode = null, $lockVersion = null)
 * @method LexicalEntryTrad|null findOneBy(array $criteria, array $orderBy = null)
 * @method LexicalEntryTrad[]    findAll()
 * @method LexicalEntryTrad[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LexicalEntryTradRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LexicalEntryTrad::class);
    }

    public function findMissingBase() {

      return $this->createQueryBuilder('l')
        ->andWhere('l.collocation IS NULL')
        ->andWhere('l.lexicalEntry IS NULL')
        ->getQuery()
        ->getResult();
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(LexicalEntryTrad $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(LexicalEntryTrad $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function findByLanguage($langCode) {

        return $this->createQueryBuilder('l')
        ->innerJoin(
            LemmaTrad::class,
             'lem', 
             'WITH', 
             'lem.id = l.lemmaTrad'
        )
        ->innerJoin(
            Language::class,
            'lang', 
            'WITH', 
            'lang.id = lem.language'
        )
        ->andWhere('lang.shortName = :langCode')
        ->setParameter('langCode', $langCode)
        ->getQuery()
        ->getResult();
      }
  

    // /**
    //  * @return LexicalEntryTrad[] Returns an array of LexicalEntryTrad objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LexicalEntryTrad
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
