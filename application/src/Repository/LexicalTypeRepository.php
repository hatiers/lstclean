<?php

namespace App\Repository;

use App\Entity\LexicalType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LexicalType|null find($id, $lockMode = null, $lockVersion = null)
 * @method LexicalType|null findOneBy(array $criteria, array $orderBy = null)
 * @method LexicalType[]    findAll()
 * @method LexicalType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LexicalTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LexicalType::class);
    }

    // /**
    //  * @return LexicalType[] Returns an array of LexicalType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LexicalType
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
