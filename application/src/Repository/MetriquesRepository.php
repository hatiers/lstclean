<?php

namespace App\Repository;

use App\Entity\Metriques;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Metriques|null find($id, $lockMode = null, $lockVersion = null)
 * @method Metriques|null findOneBy(array $criteria, array $orderBy = null)
 * @method Metriques[]    findAll()
 * @method Metriques[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MetriquesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Metriques::class);
    }

    // /**
    //  * @return Metriques[] Returns an array of Metriques objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Metriques
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
