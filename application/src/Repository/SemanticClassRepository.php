<?php

namespace App\Repository;

use App\Entity\SyntacCat;
use App\Entity\SemanticClass;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SemanticClass|null find($id, $lockMode = null, $lockVersion = null)
 * @method SemanticClass|null findOneBy(array $criteria, array $orderBy = null)
 * @method SemanticClass[]    findAll()
 * @method SemanticClass[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SemanticClassRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SemanticClass::class);
    }

    public function findByCat(SyntacCat $syntacCat)
    {
        return $this->createQueryBuilder("class")
            ->select('class')
            ->join('class.lexicalEntries', 'lexicalEntries')
            ->andWhere('lexicalEntries.cat = :syntacCat')
            ->setParameter('syntacCat', $syntacCat)
            ->distinct(true)
            ->orderBy('class.value', 'ASC')
            ->getQuery()
            ->getResult();
        ;
    }

    // /**
    //  * @return SemanticClass[] Returns an array of SemanticClass objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SemanticClass
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
