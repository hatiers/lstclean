<?php

namespace App\Repository;

use App\Entity\SemanticClassSubClass;
use App\Entity\SyntacCat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\EntityManagerInterface;


/**
 * @method SemanticClassSubClass|null find($id, $lockMode = null, $lockVersion = null)
 * @method SemanticClassSubClass|null findOneBy(array $criteria, array $orderBy = null)
 * @method SemanticClassSubClass[]    findAll()
 * @method SemanticClassSubClass[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SemanticClassSubClassRepository extends ServiceEntityRepository
{
    protected $em;
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        parent::__construct($registry, SemanticClassSubClass::class);
        $this->em = $em;
    }


    public function findAndSort()
    {
        return $this->createQueryBuilder('scsc')
            ->join('scsc.semanticClass', 'class')
            ->join('scsc.semanticSubClass', 'subclass')
            ->addOrderBy('class.value', ' ASC')
            ->addOrderBy('subclass.value', ' ASC')
            ->getQuery()
            ->getResult()
        ;
    }
    public function findAndSortByCat(SyntacCat $cat)
    {
        return $this->createQueryBuilder('scsc')
            ->join('scsc.semanticClass', 'class')
            ->join('scsc.semanticSubClass', 'subclass')
            ->andWhere('scsc.syntacCatClassSubClass= :cat')
            ->setParameter('cat', $cat)
            ->addOrderBy('class.value', ' ASC')
            ->addOrderBy('subclass.value', ' ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function ListAllClasses() 
    {

        $sql = "
        SELECT distinct s.id,
        s.value,
  
         CONCAT('[',
         GROUP_CONCAT(DISTINCT (JSON_OBJECT  ('syntac', synt.value, 
                                             'desc', scs.description_class, 
                                             'test', scs.test_appart_class))) 
         , ']') as classDescription, 
        # CONCAT('[', 
         GROUP_CONCAT(DISTINCT (scs.semantic_sub_class_id)) as subclasses
        # , ']') as subclasses 
         
         FROM semantic_class_sub_class scs
         INNER JOIN semantic_class s ON scs.semantic_class_id = s.id
         INNER JOIN syntac_cat synt ON scs.syntac_cat_class_sub_class_id = synt.id
         
         group by s.id #semantic_class_id 
        ";

        $rsm = new ResultSetMappingBuilder($this->em);
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('value', 'value');
        $rsm->addScalarResult('classDescription', 'classDescription');
        $rsm->addScalarResult('subclasses', 'subclasses');

        $query = $this->em->createNativeQuery($sql, $rsm);

        $classes = $query->getArrayResult();

        return $classes;
    } 


    public function describeSubclass($subclassId)
    {

        $sql = "
        select distinct
        sub.id, 
        sub.value,
        CONCAT('[',
                 GROUP_CONCAT(DISTINCT (JSON_OBJECT  ('syntac', synt.value, 
                                                     'desc', scs.description_sub_class, 
                                                     'test', scs.test_appart_sub_class))) 
                 , ']') as subclassDescription 
        
        from semantic_class_sub_class scs
                 INNER JOIN semantic_sub_class sub ON scs.semantic_sub_class_id = sub.id
                 INNER JOIN syntac_cat synt ON scs.syntac_cat_class_sub_class_id = synt.id
        
         where semantic_sub_class_id = :subclassId
        ";

        $rsm = new ResultSetMappingBuilder($this->em);

        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('value', 'value');
        $rsm->addScalarResult('subclassDescription', 'subclassDescription');

        $query = $this->em->createNativeQuery($sql, $rsm);
        $query->setParameter(':subclassId', $subclassId);

        $subclass = $query->getSingleResult();

        return $subclass;
    } 

    // /**
    //  * @return SemanticClassSubClass[] Returns an array of SemanticClassSubClass objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SemanticClassSubClass
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
