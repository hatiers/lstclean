<?php

namespace App\Repository;

use App\Entity\SemanticSubClass;
use App\Entity\SemanticClass;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SemanticSubClass|null find($id, $lockMode = null, $lockVersion = null)
 * @method SemanticSubClass|null findOneBy(array $criteria, array $orderBy = null)
 * @method SemanticSubClass[]    findAll()
 * @method SemanticSubClass[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SemanticSubClassRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SemanticSubClass::class);
    }

    public function findBySemanticClass(SemanticClass $semanticClass, $syntacCat)
    {
        $query = $this->createQueryBuilder("sub")
            ->select('sub')
            ->join('sub.lexicalEntries', 'lexicalEntries');

        if ($syntacCat) {
            $query->andWhere('lexicalEntries.cat = :syntacCat')
              ->setParameter('syntacCat', $syntacCat);
        }

        return $query->andWhere('lexicalEntries.semanticClass = :semanticClass')
            ->setParameter('semanticClass', $semanticClass)
            ->distinct(true)
            ->orderBy('sub.value', 'ASC')
            ->getQuery()
            ->getResult();
        ;
    }
    /*
    public function findOneBySomeField($value): ?SemanticSubClass
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
