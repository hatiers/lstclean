<?php

namespace App\Repository;

use App\Entity\SourceExemple;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SourceExemple|null find($id, $lockMode = null, $lockVersion = null)
 * @method SourceExemple|null findOneBy(array $criteria, array $orderBy = null)
 * @method SourceExemple[]    findAll()
 * @method SourceExemple[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SourceExempleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SourceExemple::class);
    }

    // /**
    //  * @return SourceExemple[] Returns an array of SourceExemple objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SourceExemple
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
