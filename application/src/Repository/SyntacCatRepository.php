<?php

namespace App\Repository;

use App\Entity\SyntacCat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SyntacCat|null find($id, $lockMode = null, $lockVersion = null)
 * @method SyntacCat|null findOneBy(array $criteria, array $orderBy = null)
 * @method SyntacCat[]    findAll()
 * @method SyntacCat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SyntacCatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SyntacCat::class);
    }

    // /**
    //  * @return SyntacCat[] Returns an array of SyntacCat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SyntacCat
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
