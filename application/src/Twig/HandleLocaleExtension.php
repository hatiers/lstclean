<?php

namespace App\Twig;

use App\Manager\LocaleManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class HandleLocaleExtension extends AbstractExtension
{
    private $lm;

    public function __construct(LocaleManager $lm)
    {
        $this->lm = $lm;
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('normalize', [$this, 'normalizeFilter']),
            new TwigFilter('getFirstLetter', [$this, 'getFirstLetter']),
        ];
    }

    public function normalizeFilter($string)
    {
        return $this->lm->normalize($string);
    }

    public function getFirstLetter($string)
    {
        return $this->lm->getFirstLetter($string);
    }
}
