<?php

namespace App\Twig;

use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use App\Entity\Language;

class LanguageExtension extends AbstractExtension
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('getLanguages', [$this, 'getLanguages'])
        ];
    }

    public function getLanguages()
    {
        $languages = $this->em->getRepository(Language::class)->findAll();

        return $languages;
    }
}
