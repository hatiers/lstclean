<?php

namespace App\Twig;

use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use App\Entity\LexicalEntry;

class LexicalEntryExtension extends AbstractExtension
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('getCoHypoCat', [$this, 'getCoHypoCat']),
            new TwigFilter('getCoHypoAllCat', [$this, 'getCoHypoAllCat']),
            new TwigFilter('idtermithToHuman', [$this, 'idtermithToHuman'])
        ];
    }

    public function getCoHypoCat(LexicalEntry $lexicalEntry)
    {
        $cohypos = $this->em->getRepository(LexicalEntry::class)->findBy([
            'cat' => $lexicalEntry->getCat(),
            'semanticClass' => $lexicalEntry->getSemanticClass(),
            'semanticSubClass' => $lexicalEntry->getSemanticSubClass()
        ]);
        return $cohypos;
    }
    public function getCoHypoAllCat(LexicalEntry $lexicalEntry)
    {
        $cohypos = $this->em->getRepository(LexicalEntry::class)->findBy([
            'semanticSubClass' => $lexicalEntry->getSemanticSubClass()
        ]);
        return $cohypos;
    }
    public function idtermithToHuman(LexicalEntry $lexicalEntry)
    {
        $strToDisplay = $lexicalEntry->getAcceptionUniq();
        if (preg_match('/(.+)_([A-Z_]+)(_(\d))/', $strToDisplay, $matches)) {
            $strToDisplay = $matches[1] . " (" . $matches[4] . ")";
        } elseif (preg_match('/(.+)_([A-Z_]+)/', $strToDisplay, $matches)) {
            $strToDisplay = $matches[1];
        }
        return $strToDisplay;
    }
}
