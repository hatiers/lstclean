<?php

namespace App\Twig;

use App\Entity\SemanticClass;
use App\Entity\SemanticSubClass;
use App\Manager\CacheManager;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class SemanticClassExtension extends AbstractExtension
{
    private $em;
    private $cm;

    public function __construct(EntityManagerInterface $em, CacheManager $cm)
    {
        $this->em = $em;
        $this->cm = $cm;
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('subClasses', [$this, 'getSubClasses']),
            new TwigFilter('getClassProperties', [$this, 'getClassProperties']),
        ];
    }

    public function getSubClasses(SemanticClass $semanticClass, $cat)
    {
        $catId = ($cat) ? $cat->getId() : 0;
        $subclassesCache = $this->cm->get("semantic" . $semanticClass->getId() . "cat" . $catId);

        if ($subclassesCache->isHit()) {
            $subclasses = $subclassesCache->get();
        } else {
            $subclasses = $this->em->getRepository(SemanticSubClass::class)->findBySemanticClass($semanticClass, $cat);
            $this->cm->store($subclassesCache, $subclasses);
        }

        return $subclasses;
    }


    public function getClassProperties(SemanticClass $semanticClass)
    // en entrée une classe sémantique, en sortie un tableau de description et de test
    {
        $semanticClassSubClasses = $semanticClass->getSemanticClassSubClasses();
        $descriptionsAndTests = [];
        foreach ($semanticClassSubClasses as $semanticClassSubClass) {
            // code...
            $propertiesString = $semanticClassSubClass->getSyntacCatClassSubClass()->getValue();
            $propertiesString .= " : " . $semanticClassSubClass->getDescriptionClass();
            if (!empty($semanticClassSubClass->getTestAppartClass())) {
                $propertiesString .= " / Test " . $semanticClassSubClass->getTestAppartClass();
            }

            if ((!in_array($propertiesString, $descriptionsAndTests)) && (!empty($semanticClassSubClass->getDescriptionClass()))) {
                //On affiche seulement si il y a une description
                $descriptionsAndTests[] = $propertiesString;
            }
        }
        return $descriptionsAndTests;
    }
}
