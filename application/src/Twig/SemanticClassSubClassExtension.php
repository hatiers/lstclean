<?php

namespace App\Twig;

use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use App\Entity\SemanticSubClass;
use App\Entity\SemanticClassSubClass;
use App\Entity\SemanticClass;
use App\Manager\CacheManager;

class SemanticClassSubClassExtension extends AbstractExtension
{
    private $em;
    private $cm;

    public function __construct(EntityManagerInterface $em, CacheManager $cm)
    {
        $this->em = $em;
        $this->cm = $cm;
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('getClassSubClass', [$this, 'getClassSubClass']),
        ];
    }

    public function getClassSubClass(SemanticClass $semanticClass, SemanticSubClass $semanticSubClass)
    {
        $classSubclassCache = $this->cm->get("semantic" . $semanticClass->getId() . "semanticSubClass" . $semanticSubClass->getId());

        if ($classSubclassCache->isHit()) {
            $classSubclass = $classSubclassCache->get();
        } else {
            $classSubclass = $this->em->getRepository(SemanticClassSubClass::class)->findOneBy(['semanticClass' => $semanticClass, 'semanticSubClass' => $semanticSubClass]);
            $this->cm->store($classSubclassCache, $classSubclass);
        }

        return $classSubclass;
    }
}
