<?php

namespace App\Twig;

use App\Entity\SemanticSubClass;
use App\Manager\CacheManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class SemanticSubClassExtension extends AbstractExtension
{
    private $cm;

    public function __construct(CacheManager $cm)
    {
        $this->cm = $cm;
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('getSubClassProperties', [$this, 'getSubClassProperties']),
        ];
    }

    public function getSubClassProperties(SemanticSubClass $semanticSubClass)
    // en entrée une classe sémantique, en sortie un tableau de description et de test
    {
        $semanticClassSubClasses = $semanticSubClass->getSemanticClassSubClasses();

        $descriptionsAndTestsCache = $this->cm->get("subClassDescription".$semanticSubClass->getId());
        if ($descriptionsAndTestsCache->isHit()) {
            $descriptionsAndTests = $descriptionsAndTestsCache->get();
        } else {
            $descriptionsAndTests=[];
            foreach ($semanticClassSubClasses as $semanticClassSubClass) {
                $propertiesString = $semanticClassSubClass->getSyntacCatClassSubClass()->getValue();
                $propertiesString .= " : ".$semanticClassSubClass->getDescriptionSubClass();
                if (!empty($semanticClassSubClass->getTestAppartSubClass())) {
                    $propertiesString .= " / Test -> ".$semanticClassSubClass->getTestAppartSubClass();
                }

                if ((! in_array($propertiesString, $descriptionsAndTests)) && (!empty($semanticClassSubClass->getDescriptionSubClass()))) {
                    //On affiche seulement si il y a une description
                    $descriptionsAndTests[] = $propertiesString;
                }
            }
            $this->cm->store($descriptionsAndTestsCache, $descriptionsAndTests);
        }
        return $descriptionsAndTests;
    }
}
