var Encore = require('@symfony/webpack-encore');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .addEntry('js/main', './assets/js/main.js')
    .addEntry('js/article', './assets/js/article.js')
    .addEntry('js/test', './assets/js/test.js')
    .addEntry('js/testCharts', './assets/js/testCharts.js')
    .addEntry('js/testCytoscape', './assets/js/cytoscape-entry.js')
    .addEntry('js/cytoscape-entries', './assets/js/cytoscape-entries.js')
    .addEntry('js/lexicalEntryChartImport', './assets/js/lexicalEntryChartImport.js')
    .addEntry('js/filterEntryTab', './assets/js/filterEntryTab.js')
    .addEntry('js/sortLemmas', './assets/js/sortLemmas.js')
    .addEntry('js/search', './assets/js/search.js')
    .addEntry('js/sortCollocations', './assets/js/sortCollocations.js')
    .addEntry('js/fonctions', './assets/js/fonctions.js')
    .addStyleEntry('css/style', './assets/css/style.css')
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning()
    .createSharedEntry('js/vendor', './assets/js/vendor.js')
    .autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
