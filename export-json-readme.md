# Resources du LST au format JSON  

## Import JSON 

- Suppression de toutes les données (sauf users et articles) et réimport via la commande : `php bin/console app:import-json`
- Le fichier d'import json doit être placé dans `/application/data/json/` et nommé `lstdata.json`
- Chaque Entité ou groupe d'entités décrit ici peut être exporté séparément. 
- L'import d'un export partiel de la base n'échouera pas, mais nécessite d'adapter la fonction vidant la base (appellée dans `/application/src/Command/ImportJsonCommand.php`, définie dans `/application/src/Manager/jsonImportManager/EmptyDbManager.php`) (pur ne la faire vider que les entités détectées dans l'import). Ce n'est pas l'utilisation prévue pour l'instant.  


## Entrée Lexicale 

```json
    "entreesLexicales": {
      "LexicalEntry": {
        "acceptionUniq": {
          "type": "Clef primaire",
          "required": true,
          "description": "lexicalEntry.AcceptionUniq sert d'identifiant unique. Construit sur le modèle : lemme_categorie_numeroOptionnel'"
        },
        "cat": {
          "type": "Clef étrangère de SyntacCat (SyntacCat.value)",
          "required": true,
          "description": "Identifiant d'une catégorie syntaxique (A : adjectif, ADV : adverbe, N : nom ou V : verbe)",
          "comment": "Lors de l'import, un premier passage sur le JSON génère les entités CatégorieSyntaxique (qui n'ont qu'un id et une value) à partir d'un tri des valeurs distincte de cette propriété)."
        },
        "lemma": {
          "type": "Clef étrangère de Lemma (Lemma.value)",
          "required": true,
          "description": "A la fois Valeur (string) et identifiant du Lemma associé à la LexicalEntry",
          "comment": "Lors de l'import, un premier passage sur le JSON génère les entités Lemma (qui n'ont qu'un id et une value) à partir d'un tri des valeurs distincte de cette propriété)."
        },
        "lexicalType": {
          "type": "Clef étrangère de LexicalType (LexicalType.value)",
          "required": true,
          "description": "A la fois Valeur (string) et identifiant du LexicalType associé à la LexicalEntry (2 valeurs : Monolexical / Polylexical)",
          "comment": "Lors de l'import, un premier passage sur le JSON génère les entités lexicalType (qui n'ont qu'un id et une value) à partir d'un tri des valeurs distincte de cette propriété)."
        },
        "semanticClass": {
          "type": "Clef étrangère de SemanticClass (SemanticClass.value)",
          "required": true,
          "description": "associe l'entrée à une classe sémantique",
          "comment": "SemanticClass.value sert de clef. Les lexicalEntry sont aussi associées à une instance de SemanticSubClassSubClass (dont une instance est générée lors de l'import), en croisant cat + semanticClass + SemanticSubClass"
        },
        "semanticSubClass": {
          "type": "Clef étrangère de SemanticSubClass (SemanticSubClass.value)",
          "required": true,
          "description": "associe l'entrée à une sous classe sémantique",
          "comment": "SemanticClass.value sert de clef. Les lexicalEntry sont aussi associées à une instance de SemanticSubClassSubClass (dont une instance est générée lors de l'import), en croisant cat + semanticClass + SemanticSubClass"
        },
        "fonctionId": {
          "type": "Clef étrangère de Fonction (fonction.label)",
          "description": "associe l'entrée à une fonction rhétorique",
          "required": false,
          "comment": "fonction.label sert de clef vers une instance de 'fonctionSpe' dans ce JSON"
        },
        "varGraph": {
          "type": "propriété",
          "required": false,
          "description": "?????"
        },
        "propMorph": {
          "type": "propriété",
          "required": false,
          "description": "?????"
        },
        "propSynt": {
          "type": "propriété",
          "required": false,
          "description": "?????"
        },
        "verbConstruction": {
          "type": "propriété",
          "required": false,
          "description": "?????"
        },
        "sujetComplement": {
          "type": "propriété",
          "required": false,
          "description": "?????"
        },
        "meaning": {
          "type": "propriété",
          "required": false,
          "description": "?????"
        },
        "source": {
          "type": "propriété",
          "required": false,
          "description": "?????"
        },
        "comment": {
          "type": "propriété",
          "required": false,
          "description": "?????"
        },
        "metrics": {
          "DESCRIPTION": {
            "type": "Entité liée à LexicalEntry (One to One)",
            "description": "statisiques de la lexicalEntry",
            "required": false,
            "comment": "l'entité Metrique est générée et reliée à la LexicalEntry qui la contient lors de l'import"
          },
          "ratio": {
            "type": "propriété",
            "required": true,
            "description": "résultat du ratio de la fréquence relative (par million de mots) dans le corpus d'analyse (CA) sur le corpus de constraste (CC)"
          },
          "ccbym": {
            "type": "propriété",
            "required": true,
            "description": "fréquence relative (par million de mot) dans le corpus de contraste"
          },
          "cabym": {
            "type": "propriété",
            "required": true,
            "description": "fréquence relative (par million de mot) dans le corpus d'analyse"
          },
          "cafreq": {
            "type": "propriété",
            "required": true,
            "description": "fréquence absolue (nombre d'occurrences) dans le corpus d'analyse"
          },
          "ngrammes": {
            "type": "propriété",
            "required": true,
            "description": "nombre d'apparition du lemme dans un n-gramme validé manuellement comme expression polylexicale contigüe (ex: 'point' dans 'point de vue') afin de soustraire ce nombre d'occurrences à la fréquence totale."
          },
          "repartitionDisciplines": {
            "type": "propriété",
            "required": true,
            "description": "nombre de disciplines différentes dans lesquelles le lemme apparaît"
          },
          "repartitionTranches": {
            "type": "propriété",
            "required": true,
            "description": "nombre de tranches différentes du corpus d'analyse (divisé en 100 tranches) dans lesquelles le lemme apparaît}</distribTranches"
          },
          "disciplinesSpec": {
            "type": "propriété",
            "required": true,
            "description": "nombre de disciplines pour lesquelles la fréquence relative du lemme est supérieure par rapport au corpus de contraste"
          },
          "freqParDisciplines": {
            "description": "fréquence absolue du lemme dans le sous-corpus disciplinaire",
            "anthropo": {
              "type": "propriété",
              "required": true,
              "description": "fréquence absolue dans le sous-corpus d'anthropologie"
            },
            "eco": {
              "type": "propriété",
              "required": true,
              "description": "etc"
            },
            "geo": {
              "type": "propriété",
              "required": true,
              "description": "etc"
            },
            "histoire": {
              "type": "propriété",
              "required": true,
              "description": "etc"
            },
            "ling": {
              "type": "propriété",
              "required": true,
              "description": "etc"
            },
            "psycho": {
              "type": "propriété",
              "required": true,
              "description": "etc"
            },
            "sciedu": {
              "type": "propriété",
              "required": true,
              "description": "etc"
            },
            "sciepo": {
              "type": "propriété",
              "required": true,
              "description": "etc"
            },
            "scinfo": {
              "type": "propriété",
              "required": true,
              "description": "etc"
            },
            "socio": {
              "type": "propriété",
              "required": true,
              "description": "etc"
            }
          }
        },
        "exemples": [
          {
            "DESRIPTION": {
              "type": "Entité liée à LexicalEntry (One LexicalEntry to Many Exemples )",
              "description": "exemples de la lexicalEntry",
              "required": false,
              "comment": "chaque entité Exemple est générée et reliée à la LexicalEntry qui la contient lors de l'import"
            },
            "source": {
              "type": "Clef étrangère (string) de SourceExemple",
              "required": false,
              "description": "sourceExemple.code sert de clef"
            },
            "contexteGauche": {
              "type": "propriété",
              "required": true
            },
            "pivot": {
              "type": "propriété",
              "required": true
            },
            "contexteDroit": {
              "type": "propriété",
              "required": true
            },
            "partieTextuelle": {
              "type": "propriété",
              "required": false,
              "description": "?????"
            }
          }
        ]
      }
    }, 
```

### Exemple Entrée Lexicale 

```json
 {
      "cat": "A",
      "acceptionUniq": "abondant_A",
      "lemma": "abondant",
      "varGraph": null,
      "lexicalType": "monolexical",
      "fonctionId": null,
      "propMorph": null,
      "propSynt": null,
      "verbConstruction": null,
      "sujetComplement": null,
      "semanticClass": "quantité",
      "semanticSubClass": "quantité_grande",
      "meaning": "En grande quantité.",
      "source": "DEM",
      "comment": null,
      "metrics": {
        "ratio": 3.529,
        "ccbym": 4.045,
        "cabym": 14.273,
        "cafreq": 69,
        "ngrammes": 0,
        "repartitionDisciplines": 9,
        "repartitionTranches": 41,
        "disciplinesSpec": 7,
        "freqParDisciplines": {
          "anthropo": 5,
          "eco": 5,
          "geo": 12,
          "histoire": 21,
          "ling": 0,
          "psycho": 3,
          "sciedu": 3,
          "sciepo": 5,
          "scinfo": 4,
          "socio": 11
        }
      },
      "exemples": [
        {
          "source": "[eco-art-456]",
          "contexteGauche": "L'objet de cet article n'est pas d'offrir un nouveau survol de cette",
          "pivot": "abondante",
          "contexteDroit": "littérature, nous ne pourrions qu'échouer à vouloir rivaliser avec les travaux de TIROLE [1988], VARIAN [1988] […]",
          "partieTextuelle": "introduction"
        },
        {
          "source": "[his-art-152]",
          "contexteGauche": "Les données, quand elles sont",
          "pivot": "abondantes",
          "contexteDroit": ", comme en période de guerre et de conflit, peuvent aussi être utilisées pour la reconstitution détaillée des conditions météorologiques sur des intervalles plus courts. ",
          "partieTextuelle": "développement"
        },
        {
          "source": "[eco-art-95]",
          "contexteGauche": "L'enjeu de cette démarche, au-delà du simple test de la théorie (pour laquelle les",
          "pivot": "abondants",
          "contexteDroit": "résultats empiriques ne permettent pas de dégager des conclusions non ambiguës), porte sur la capacité des taux courts futurs anticipés […]",
          "partieTextuelle": "introduction"
        }
      ]
    },
```

## Collocation

```json
    "collocations": {
      "collocation": {
        "value": {
          "type": "Clef primaire",
          "required": true,
          "description": "forme typique de la collocation (identique à structureGlobale) + éventuel numéro si plusieurs collocations partagent la même forme typique :  ex 'non négligeable 2' "
        },
        "structureGlobale": {
          "type": "propriété",
          "required": true,
          "description": "?????"
        },
        "structureColloc": {
          "type": "propriété",
          "required": true,
          "description": "????? structure syntaxique ?"
        },
        "propSyntax": {
          "type": "propriété",
          "required": false,
          "description": "?????"
        },
        "fonctionLexicale": {
          "type": "propriété",
          "required": false,
          "description": "?????"
        },
        "lexicalEntryBaseId": {
          "type": "Clef étrangère de LexicalEntry (LexicalEntry.acceptionUniq)",
          "required": false,
          "description": "Lien vers la lexicalEntry formant la base de la Collocation",
          "comment": "Si pas de LexicalEntry associée, le lemmaBase existe néanmoins (mais n'est pas décrit outre mesure dans les données)"
        },
        "lexicalEntryCollocatifId": {
          "type": "Clef étrangère de LexicalEntry (LexicalEntry.acceptionUniq)",
          "required": false,
          "description": "Lien vers la lexicalEntry formant le Collocatif",
          "comment": "Si pas de LexicalEntry associée, le lemmaCollocatif existe néanmoins (mais n'est pas décrit outre mesure dans les données)"
        },
        "lemmaBase": {
          "type": "propriété",
          "required": true,
          "description": "Lemme formant la base de la collocation"
        },
        "lemmaCollocatif": {
          "type": "propriété",
          "required": true,
          "description": "Lemme formant le collocatif de la collocation"
        },
        "fonctions": {
          "DESCRIPTION": {
            "type": "Liste de clefs étrangères de Fonction (fonction.label)",
            "description": "associe la collocation à une ou plusieurs fonction rhétorique",
            "required": false,
            "comment": "fonction.label sert de clef vers une instance de 'fonctionSpe' dans ce JSON"
          },
          "Exemple de valeurs": [
            "methodes",
            "analyses",
            "miseenevidence"
          ]
        },
        "complement": {
          "type": "propriété",
          "required": false,
          "description": "?????"
        },
        "patronParadigme": {
          "type": "propriété",
          "required": false,
          "description": "?????"
        },
        "patronFixe": {
          "type": "propriété",
          "required": false,
          "description": "?????"
        },
        "partieDiscours": {
          "type": "propriété",
          "required": false,
          "description": "?????"
        },
        "metrics": {
          "DESCRIPTION": {
            "comment": "propriétés directes de Collocations (ne forment pas une entité indépendante, mais mises à part pour clarté du JSON)"
          },
          "nbOccurrences": "fréquence absolue (nombre d'occurrences) dans le corpus d'analyse",
          "logLike": "Log-likelihood : probabilité que les deux mots (base et collocatif) sont utilisés ensemble plus souvent que le hasard.",
          "frequence": "fréquence d'appariton de la collocation dans le corpus d'analyse (divisé en 100 tranches). Exprimée par les valeurs : MF, F, TF. ",
          "nbDisciplines": "nombre de disciplines différentes dans lesquelles la collocation apparaît"
        },
        "exemples": [
          {
            "DESRIPTION": {
              "type": "Entité liée à Collocation (One Collocation to Many Exemples )",
              "description": "exemples de la Collocation",
              "required": false,
              "comment": "chaque entité Exemple est générée et reliée à la Collocation qui la contient lors de l'import"
            },
            "source": {
              "type": "Clef étrangère (string) de SourceExemple",
              "required": false,
              "description": "sourceExemple.code sert de clef"
            },
            "structure": {
              "type": "propriété",
              "required": true,
              "comment": "identique à Collocation.structureColloc "
            },
            "contexteGauche": {
              "type": "propriété",
              "required": false
            },
            "pivot": {
              "type": "propriété",
              "required": true
            },
            "contexteDroit": {
              "type": "propriété",
              "required": true
            },
            "partieTextuelle": {
              "type": "propriété",
              "required": false,
              "description": "?????"
            }
          }
        ]
      }
    },
```

### Exemple Collocation

```json
 {
      "structureGlobale": "à la suite des travaux",
      "value": "à la suite des travaux",
      "structureColloc": "prep-N",
      "propSyntax": null,
      "fonctionLexicale": null,
      "lexicalEntryBaseId": "travail_N",
      "lexicalEntryCollocatifId": null,
      "fonctions": null,
      "lemmaBase": "travail",
      "lemmaCollocatif": "à la suite de",
      "complement": "<b>A la suite des travaux</b> de qn(Nom d'auteur, Année), nous considérons que...",
      "patronParadigme": null,
      "patronFixe": null,
      "partieDiscours": null,
      "metrics": {
        "nbOccurrences": 7,
        "logLike": 13,
        "frequence": "MF",
        "nbDisciplines": 4
      },
      "exemples": [
        {
          "source": "[lin-art-306]",
          "structure": "prep-N",
          "contexteGauche": "1 Précisons que nous considérerons,",
          "pivot": "à la suite des travaux",
          "contexteDroit": "de M. - J. Reichler Beguelin, que la relation d' anaphore ne consiste pas à dupliquer les antécédents, mais à garder la trace de leur interprétation antérieure qui évolue en fonction des nouvelles propositions ou propriétés qui leur sont attribuées.",
          "partieTextuelle": "notes"
        },
        {
          "source": "[spo-art-433]",
          "structure": "prep-N",
          "contexteGauche": "Quelques jours plus tard, les ouvriers rouges votent en masse pour leur directeur [ 42 ]. \" Cette image \" apolitique \" a été affirmée",
          "pivot": "à la suite des travaux",
          "contexteDroit": "de l' équipe de Louis Girard sur les conseillers généraux de 1870",
          "partieTextuelle": "développement"
        }
      ]
    },
```

## Traduction

```json
    "traductions": {
      "LangCode": {
        "DESCRIPTION": "Toutes les traductions sont regroupées dans un array sous un ID correspondant au code ISO de la langue",
        "LexicalEntryTrad": {
          "nbOcc": {
            "type": "propriété",
            "required": true,
            "comment": "jamais null, mais peut-être de 0"
          },
          "lemmaTrad": {
            "type": "Clef étrangère de LemmaTrad (LemmaTrad.value)",
            "required": true,
            "description": "A la fois Valeur (string) et identifiant du Lemma associé à la LexicalEntryTRad",
            "comment": "Relation One lemmaTrad to Many LexicalEntryTrad Lors de l'import, un premier passage sur le JSON génère les entités LemmaTrad (qui n'ont qu'un id et une value, et une langue qui est celle de l'objet parent) à partir d'un tri des valeurs distincte de cette propriété)."
          },
          "lexicalEntryId": {
            "type": "Clef étrangère de la LexicalEntry traduite (LexicalEntry.acceptionUniq)",
            "required": false,
            "comment": "relation One LexicalEntry to One LexicalEntryTrad"
          },
          "collocationId": {
            "type": "Clef étrangère de la Collocation traduite (Collocation.value)",
            "required": false,
            "comment": "relation One Collocation to One LexicalEntryTrad. Janv 2024 : aucune utilisation"
          },
          "synonyms": {
            "type": "propriété",
            "required": false,
            "comment": "peut contenir des valeurs mutliples séparées par des ','"
          },
          "exempleSource": {
            "type": "propriété",
            "required": false
          },
          "exempleCible": {
            "type": "propriété",
            "required": false
          },
          "comment": {
            "type": "propriété",
            "required": false
          },
          "usageGeneral": {
            "type": "propriété",
            "required": false
          },
          "usageSpecifique": {
            "type": "propriété",
            "required": false
          },
          "contexte": {
            "type": "propriété",
            "required": false
          }
        }
      }
    },
```

### Exemple Traduction

```json
"zh": [
      {
        "nbOcc": 70173,
        "lemmaTrad": "状态",
        "lexicalEntryId": "état_N_1",
        "collocationId": null,
        "synonyms": "状况",
        "exempleSource": "chaque état de conscience",
        "exempleCible": "意识状态",
        "comment": "validé dans BCC",
        "usageGeneral": null,
        "usageSpecifique": "l' état de la conjoncture经济状况",
        "contexte": null
      },
      {...},
      ...
]
```

## Classes sémantiques 

```json
    "classesSemantiques": {
      "SemanticClasses": {
        "id": {
          "type": "Clef primaire (semanticClass.value)",
          "required": true
        },
        "definition": {
          "0": {
            "cat": {
              "type": "Clef secondaire de SyntacCat (SyntacCat.value)",
              "required": true,
              "comment": "Provient de la table semanticClassSubClass"
            },
            "description": {
              "type": "propriété",
              "required": true,
              "comment": "Provient de la table semanticClassSubClass"
            },
            "critereTest": {
              "type": "propriété",
              "required": true,
              "comment": "Provient de la table semanticClassSubClass"
            }
          },
          "EXPLICATION": "Une même classe sémantique peut avoir plusieurs définitions : une par Catégorie Syntaxique associée (SyntacCat). Dans la BDD dy LST,  semanticClass et semanticSubClass n'ont qu'une .value et un .id : les defintions, catégories synaxiques et critere de test, qui vont de pair, sont tous stockés dans la table semanticClassSubClass."
        },
        "sousClassesSemantiques": [
          {
            "id": {
              "type": "Clef primaire (semanticSubClass.value)",
              "required": true
            },
            "definition": {
              "0": {
                "cat": {
                  "type": "Clef secondaire de SyntacCat (SyntacCat.value)",
                  "required": true,
                  "comment": "Provient de la table semanticClassSubClass"
                },
                "description": {
                  "type": "propriété",
                  "required": true,
                  "comment": "Provient de la table semanticClassSubClass"
                },
                "critereTest": {
                  "type": "propriété",
                  "required": true,
                  "comment": "Provient de la table semanticClassSubClass"
                }
              },
              "EXPLICATION": "Une même sous-classe sémantique peut avoir plusieurs définitions : une par Catégorie Syntaxique associée (SyntacCat). Dans la BDD dy LST,  semanticClass et semanticSubClass n'ont qu'une .value et un .id : les defintions, catégories synaxiques et critere de test, qui vont de pair, sont tous stockés dans la table semanticClassSubClass."
            }
          }
        ]
      }
    },
```

### Exemple Classes sémantiques

```json
    {
    "id": "composition",
    "definition": [
        {
        "cat": "A",
        "description": "Dénote la nature d’une entité en tant qu’unité (complexe ou non : assemblage ou combinaison de plusieurs éléments ou parties).",
        "critereTest": null
        }
    ],
    "sousClassesSemantiques": [
        {
        "id": "composition",
        "definition": [
            {
            "cat": "A",
            "description": null,
            "critereTest": null
            },
            {
            "cat": "N",
            "description": "Nature d’une entité en tant qu’unité.",
            "critereTest": " une entité est caractérisée par DétPoss N"
            },
            {
            "cat": "V",
            "description": "État d’une entité qui constitue un assemblage ou une combinaison de plusieurs éléments ou parties et qui est dotée d’une certaine structure.",
            "critereTest": null
            }
        ]
        }
    ]
    },
```

##	Fonction Rhétoriques  

```json
    "fonctionsRhetoriques": {
      "fonctions": {
        "id": {
          "type": "Clef primaire (fonction.generalF)",
          "required": true,
          "comment": "Dans la BDD du LST, fonctionGénérale et fonctionSpécifique sont réunies sur une même ligne : la clef de fonction.generalF est répétée pour chaque fonctionSpe (fonction.generalF , fonction.label). Les définitions de fonctionGénérale et fonctionSpécifique ne sont pas stockées en base mais comme des messages du système de traduction, appelées à partir des ID de fonctionGénérale et fonctionSpécifique"
        },
        "definition": {
          "type": "propriété",
          "required": true,
          "comment": "Attention, n'est pas stocké en base, est stocké comme la chaîne de traduction associée à fonction.generalF"
        },
        "fonctionsSpe": []
      }
    },
```
### Exemple 

```json
    {
    "id": "definir",
    "definition": "Pour définir les termes",
    "fonctionsSpe": [
        {
        "id": "definition",
        "definition": "Définir ou expliquer qc (un terme, une notion, un concept)",
        "exemple": "Nous pouvons définir X comme..."
        },
        {
        "id": "emprunt",
        "definition": "Emprunter des définitions (termes, mots, expressions...) déjà établies à d'autres auteurs",
        "exemple": "On reprend la définition de qn"
        }
    ]
    },
```

### Routines

```json
    "routines": {
      "routines": {
        "code": {
          "type": "Clef primaire (routine.code)",
          "required": true
        },
        "type": {
          "type": "propriété",
          "required": true
        },
        "subType": {
          "type": "propriété",
          "required": false
        },
        "subSubType": {
          "type": "propriété",
          "required": false
        },
        "fonctionId": {
          "type": "Clef étrangère de Fonction (fonction.label)",
          "description": "associe la routine à une fonction rhétorique",
          "required": true,
          "comment": "fonction.label sert de clef vers une instance de 'fonctionSpe' dans ce JSON"
        },
        "modele": {
          "type": "propriété",
          "required": true
        },
        "realisation": {
          "type": "propriété",
          "required": true,
          "comment": "plusieurs realisations sont concaténées avec & comme séparateur."
        },
        "occurrences": {
          "type": "propriété",
          "required": true,
          "comment": "fréquence absolue (nombre d'occurrences) dans le corpus d'analyse"
        },
        "patron": {
          "type": "propriété",
          "required": false
        },
        "exemples": [
          {
            "DESRIPTION": {
              "type": "Entité liée à Routine (One Routine to Many Exemples )",
              "description": "exemples de la Routine",
              "required": false,
              "comment": "chaque entité Exemple est générée et reliée à la Routine qui la contient lors de l'import"
            },
            "source": {
              "type": "Clef étrangère (string) de SourceExemple",
              "required": false,
              "description": "sourceExemple.code sert de clef"
            },
            "type": {
              "type": "propriété",
              "required": true
            },
            "content": {
              "type": "propriété",
              "required": true
            },
            "simplicity": {
              "type": "propriété",
              "required": false
            }
          }
        ]
      }
    },
```
### Exemple 

```json
{
    "code": "SUR_1",
    "type": "Marqueurs d’évaluation",
    "subType": "Surprise",
    "subSubType": null,
    "fonctionId": "surprise",
    "modele": "Il (+ nous/me) + Vétat + ADJsurprise (+ de + Vanalyse + que)",
    "realisation": "Il est frappant de constater que...&Il peut paraître surprenant que...",
    "occurrences": 25,
    "patron": "Il&Il----(nous/me)&(nous/me)----Vétat&être, sembler, paraître, apparaître...----ADJsurprise&frappant, surprenant, étonnant----(de)&(de)----(Vanalyse)&(constater, observer, voir...)----(que)&(que)",
    "exemples": [
    {
        "source": "[geo-art-348]",
        "type": "Forme de base",
        "content": "<ROUT TYPE=\"SUR_1\"><information>Quel que soit le type d’impact attribué aux évolutions démographiques,</information> il est frappant de constater <objet>l’importance de ce facteur dans le débat allemand (Kabisch <hi rend=\"italic\">et al.</hi>, 2006).</objet></ROUT>",
        "simplicity": "'++"
    },
    {
        "source": "[his-art-17]",
        "type": "Forme de base",
        "content": "<ROUT TYPE=\"SUR_1\"><information>Dans ces dessins hebdomadaires, intitulés <hi rend=\"italic\">Uncle Habner Says</hi>,</information> il est frappant de constater <objet>la récurrence d’un discours antiétatique :l’intervention de l’État dans le champ social perturbe l’ordre industriel.</objet></ROUT>",
        "simplicity": "'++"
    },
    ]
}
```

## Sources des exemples

```json
    "sourcesExemples": {
      "source": {
        "id": {
          "type": "Clef primaire (sourceExemple.value)",
          "required": true
        },
        "descriptionBiblio": {
          "type": "propriété",
          "required": true
        }
      }
    }
```

```json
 {
      "id": "[spo-art-637]",
      "descriptionBiblio": "Denord, F., & Schwartz, A. (2010). L'économie (très) politique du traité de Rome. Politix, 89, 35-56. Consulté le 20 octobre 2014  : www.cairn.info/revue-politix-2010-1-page-35.htm."
    },

```