# Resources du LST en xml 

## Utilisation du standard LMF 

Les ressources du LST sont représentées en XML dans un modèle partiellement conforme à LMF (Lexical Markup Framework). 

- Les entrées Lexicales (mono ou polylexicales), les collocations et leurs traductions sont représentées avec des éléments tirés de LMF. Chacune est une entrée de lexique (<LexicalEntry>) associant un ou plusieurs lemmes (<Lemma>), à un sens (<Sense>). Ces entrées sont dotées de définitions et d'exemples(<Context><TextRepresentation/></Context>) représentés eux-aussi avec LMF. Plusieurs traits (<feat>) et attributs (@source, @language, @text) sont également tirés des divers modules de LMF, de même que la manière de représenter les relations entres les entrées (collocatifs, traductions ...). Une portion significative et cohérente des données du lexique est ainsi standardisée. 
- Cependant, au delà de cette structure fondamentale, le LST contient des éléments d'analyse de ces entrées et des relations avec d'autres ressources (routines, fonctions, classes sémantiques), qui ne sont pas exprimées avec un schéma XML reprennant LMF mais selon un modèle ad-hoc. Un certain nobre d'éléments et d'attributs originaux sont donc présents au sein de cet export. 

## Entrée Lexicale 

```xml
<LexicalEntry   id="{Identifiant unique : contruit sur le modèle :lemme_categorie_numeroOptionnel }" 
                propSynt="{????? - OPTIONNEL}"
                propMorph="{????? - OPTIONNEL}"
                fonctionRhetGen="{Identifiant d'une fonction rhétorique générale - OPTIONNEL}" 
                fonctionRhetSpe="{Identifiant d'une fonction rhétorique spécifique - OPTIONNEL}"
                verbConstruction="{????? - OPTIONNEL}"
                >
        <feat att="partOfSpeech" val="{catégorie syntaxique (adjectif, adverbe, nom ou verbe)}"/>
        <feat att="lexicalType" val="{polylexical ou monolexical (selon que segmenté ou non par l'analyseur utilisé)}"/>
        <Lemma>
          <feat att="writtenForm" val="{forme lemmatisée}"/>
        </Lemma>
        <Sense id="{Identifiant unique construit sur le modèle :  'id_LexicalEntry + #sense' }" 
               classe_semantique="{Identifiant de la classe sémantique à laquelle appartient l'acception en question}" 
               sous_classe_semantique="{Identifiant de la sous-classe sémantique à laquelle appartient l'acception en question}"
               >
        <!-- L'identifiant du Sense d'une LexicalEntry permet de faire le lien avec sa/ses traductions -->
          <Definition language="{langue de la définition : code ISO 639-1 (ex : fr)}">
            <feat att="text" val="{texte de la définition}"/>
            <feat att="source" val="{identifiant de la source de la définition} - OPTIONNEL"/>
          </Definition>
          <!-- Context + TextRepresentation = un exemple. Leur nombre varie -->
          <Context> 
            <TextRepresentation source="{identifiant de la source de l'exemple}  - OPTIONNEL">
                {Texte de l'exemple. L'entrée est mise en évidence en étant encadrée par un élément <pivot>entrée</pivot>}
            </TextRepresentation>
          </Context>
          ...
        </Sense>
        <statistiques>...</statistiques>
</LexicalEntry>
```

### Exemple Entrée Lexicale 

```xml
<LexicalEntry   id="aborder_V" 
                propSynt="sujet métonymique" 
                verbConstruction="qn aborde qc "
                >
        <feat att="partOfSpeech" val="V"/>
        <feat att="lexicalType" val="monolexical"/>
        <Lemma>
          <feat att="writtenForm" val="aborder"/>
        </Lemma>
        <Sense id="aborder_V#sense" classe_semantique="analyse_info" sous_classe_semantique="#examen">
          <Definition language="fr">
            <feat att="text" val="Commencer à parler de, à discuter de, en parlant d'un sujet. "/>
            <feat att="source" val="Wiktionnaire"/>
          </Definition>
          <Context>
            <TextRepresentation source="[infcom-art-473]">
                Ces fiches descriptives<pivot>abordaient</pivot>divers sujets et s'inscrivaient dans la perspective monographique prônée par Mauss.
            </TextRepresentation>
          </Context>
        </Sense>
        <statistiques> ... </statistiques>
</LexicalEntry>
```

## Collocation

```xml
<LexicalEntry   id="la forme typique de la collocation sert d'identifiant"               

                fonctionRhetGen="{Identifiant d'une fonction rhétorique générale  - OPTIONNEL}" 
                fonctionRhetSpe="{Identifiant d'une fonction rhétorique spécifique - OPTIONNEL} "

                structure="{?????}"
                fonctionLexicale="{????? - OPTIONNEL}"  
                proprietesSyntaxique="{????? - OPTIONNEL}"
                partieDiscours="{????? - OPTIONNEL}"
                >
            
    <Lemma><feat att="writtenForm" val="{forme typique de la collocation}"/></Lemma> 

    <ListOfComponents>
        <!-- Décompose la collocation en ses différents éléments (base et collocatif). 
        Si ces éléments sont déjà présents comme une lexicalEntry (monolexicale ou polylexicale), le lien est fait avec son identifiant unique:  -->
        <Component entry="{{collocation.lexicalEntryBase.acceptionUniq}}" type="base"/>
        <Component entry="{{collocation.lexicalEntryCollocatif.acceptionUniq}}" type="collocatif"/>

        <!-- Si l'un des deux ou les deux éléments ne sont pas représentés ailleurs, la valeur de leur lemme est indiquée directement dans un attribut value -->
        <Component value="{{collocation.lemmaBase}}" type="base"/>
        <Component value="{{collocation.lemmaCollocatif}}" type="collocatif"/>
    </ListOfComponents>

    <Sense id="{identifiant du sens de la collocation construit sur le modèle : 'forme typique de la collocation + #sense'}" >

    <!-- L'identifiant du Sense d'une collocation permet de faire le lien avec sa/ses traductions. -->
    <!-- Conformément à LMF, les exemples sont rattachés au sens, et représentés par Context/TextRepresentation.   -->
    <!-- Il peut y avoir plusieurs exemples. -->
        <Context> 
            <TextRepresentation structure="{????? - OPTIONNEL (identique à la structure de la collocation)}"
                                partieTexte="{?????}"
                                >
                        {Texte de l'exemple. La collocation est mise en évidence en étant encadrée par un élément <pivot>entrée</pivot>}
            </TextRepresentation>        
        </Context>
        ...
    </Sense>

    <!--  JE NE SAIS PAS COMMENT DÉCRIRE CES 3 ÉLÉMENTS -->
    <complement><![CDATA[{????? - OPTIONNEL}]]></complement> 
    <patronParadigme>{????? - OPTIONNEL}</patronParadigme>
    <patronFixe>{????? - OPTIONNEL}</patronFixe>

    <statistiques> ... </statistiques>
</LexicalEntry>
```

### Exemple Collocation

```xml
    <LexicalEntry id="à la suite des travaux" structure="prep-N">
    <Lemma>
        <feat att="writtenForm" val="à la suite des travaux"/>
    </Lemma>
    <ListOfComponents>
        <Component entry="travail_N" type="base"/>
        <Component value="à la suite de" type="collocatif"/>
    </ListOfComponents>
    <Sense id="à la suite des travaux#sense">
        <Context>
        <TextRepresentation source="[lin-art-306]" structure="prep-N" partieTexte="notes">1 Précisons que nous considérerons,<pivot>à la suite des travaux</pivot>de M. - J. Reichler Beguelin, que la relation d' anaphore ne consiste pas à dupliquer les antécédents, mais à garder la trace de leur interprétation antérieure qui évolue en fonction des nouvelles propositions ou propriétés qui leur sont attribuées.</TextRepresentation>
        </Context>
        <Context>
        <TextRepresentation source="[spo-art-433]" structure="prep-N" partieTexte="développement">Quelques jours plus tard, les ouvriers rouges votent en masse pour leur directeur [ 42 ]. " Cette image " apolitique " a été affirmée<pivot>à la suite des travaux</pivot>de l' équipe de Louis Girard sur les conseillers généraux de 1870</TextRepresentation>
        </Context>
    </Sense>
    <complement><![CDATA[<b>A la suite des travaux</b> de qn(Nom d'auteur, Année), nous considérons que...]]></complement>
    <statistiques>
        <nbOccurrences>7</nbOccurrences>
        <frequence>MF</frequence>
        <distribDisciplines>4</distribDisciplines>
        <forceAssociation>13</forceAssociation>
    </statistiques>
    </LexicalEntry>
```

## Traduction

```xml
<LexicalEntry id="{Le lemme sert d'identifiant unique'}" lang="{ISO 639-1 (ex : zh)}">
    <Lemma>
        <feat att="writtenForm" val="{forme lemmatisée}"/>
    </Lemma>
    <Sense id="{Identifiant unique construit sur le modèle :  'lemme + #sense' }">
        <!-- Pour une traduction, les deux "exemples" -->
        <Context>
            <TextRepresentation lang="{ISO 639-1 (ex : zh)}"> {texte de l'exemple (cible) dans la langue indiquée}</TextRepresentation>
        </Context>
        <Context>
            <TextRepresentation lang="{ISO 639-1 (ex : fr)}">{texte de l'exemple (source) dans la langue indiquée}</TextRepresentation>
        </Context>
    </Sense>

    <!-- OPTIONNEL et répétable : il peut y avoir plusieurs RelatedForm pour plusieurs synonymes  -->
    <RelatedForm type="synonym"> 
        <feat att="writtenForm" val="{lemme des synonymes, dans la même langue}" />
    </RelatedForm>
    
    <usageSpe>{????? - OPTIONNEL}</usageSpe>
    <comment>{commentaire - OPTIONNEL}</comment>

    <statistiques>
        <nbOccurrences>{Nombre total d'occurrence dans le corpus}</nbOccurrences>
    </statistiques>
</LexicalEntry>
<!-- SenseAxis permet de lier une entrée à sa traduction -->
<SenseAxis senses="{Identifiant de l'expression cible (traduction) séparé par un espace de l'identifiant de l'expression source (traduite)}" type="translation{type pour indiquer que la relation entre sens est celle de traduction}" />
```

### Exemple Traduction

```xml
<LexicalEntry id="状态" lang="zh">
    <Lemma><feat att="writtenForm" val="状态"/></Lemma>
    <Sense id="状态#sense">
        <Context>
            <TextRepresentation lang="zh">意识状态</TextRepresentation>
        </Context>
        <Context>
            <TextRepresentation lang="fr">chaque état de conscience</TextRepresentation>
        </Context>
    </Sense>
    <RelatedForm type="synonym">
        <feat att="writtenForm" val="状况" lang="zh"/>
    </RelatedForm>

    <usageSpe>l' état de la conjoncture经济状况</usageSpe>
    <comment>validé dans BCC</comment>

    <statistiques>
        <nbOccurrences>70173</nbOccurrences>
    </statistiques>
</LexicalEntry>
<SenseAxis senses="状态#sense état_N_1#sense" type="translation"/>
```

## Classes sémantiques 

```xml
<ClasseSemantique id="{identifiant unique (varchar)}">
    <definition categorieSyntaxique="{catégorie syntaxique (adjectif, adverbe, nom ou verbe)}">
        {Texte de la définition de la classe en lien avec une catégorie Syntaxique précise}
        <critereTest>{????? - OPTIONNEL}</critereTest>
    </definition>
    <!-- Une ClasseSemantique contient plusieurs SousClasseSemantique -->
    <SousClasseSemantique id="{identifiant unique (varchar)}">
        <definition categorieSyntaxique="{catégorie syntaxique (adjectif, adverbe, nom ou verbe)}">
            {Texte de la définition de la classe en lien avec une catégorie Syntaxique précise}
            <critereTest>{????? - OPTIONNEL}</critereTest>
        </definition>
    </SousClasseSemantique>
    ... 
</ClasseSemantique>
```

### Exemple Classes sémantiques

```xml
<ClasseSemantique id="quantité">
    <definition categorieSyntaxique="A"> Regroupe les adjectifs décrivant le dénombrement ou la masse. </definition>
    <definition categorieSyntaxique="ADV"> Indique le dénombrement, la mesure d’une unité. </definition>
    <definition categorieSyntaxique="N"> renvoie à la notion de quantité, de mesure.
         <critereTest>calculer un n / un n s’élève à</critereTest>
    </definition>
    <SousClasseSemantique id="quantité_grande">
         <definition categorieSyntaxique="A"> </definition>
    </SousClasseSemantique>
    <SousClasseSemantique id="mesure">
         <definition categorieSyntaxique="ADV"> </definition>
    <definition categorieSyntaxique="N">
            Grandeur résultant du rapport entre plusieurs valeurs.
            <critereTest>calculer/mesurer un N entre X et Y</critereTest>
    </definition>
    </SousClasseSemantique>
    ... 
</ClasseSemantique>
```

##	Fonction Rhétoriques  

### Exemple 

```xml
<FonctionRhetGen id="definirthemeessentiel">
    <definition>Pour définir le thème essentiel de la recherche (sujet d’étude, objectif, hypothèse, problématique)</definition>
    <exemple>L’objet/sujet d’étude concerne qc</exemple>
    
    <FonctionRhetSpe id="sujet">
        <definition>Exposer au lecteur le thème que l’auteur souhaite aborder dans son article</definition>
    <exemple>L’objet/sujet d’étude concerne qc</exemple>
    </FonctionRhetSpe>

    <FonctionRhetSpe id="objectif">
        <definition>Exposer au lecteur le but que l’auteur souhaite atteindre</definition>
    <exemple>Le premier objectif est de + Vinf</exemple>
    </FonctionRhetSpe>
    ...
</FonctionRhetGen>
```
### Routines

- Nombre de `<realisation>` variable. 
- Nombre d'`<exemple>` variable.
- `<simplicity>` optionnel sur les exemples. 
- contenu des exemples est balisé, de manière irrégulière, donc encadré par  `<![CDATA[ ... ]]>`

### Exemple 

```xml
<Routine    id="SUR_1" 
            type="Marqueurs d’évaluation" 
            subtype="Surprise" 
            fonctionRhetGen="evaluer" 
            fonctionRhetSpe="surprise"
            >
    <modele>Il (+ nous/me) + Vétat + ADJsurprise (+ de + Vanalyse + que)</modele>
    <realisation>Il est frappant de constater que...</realisation>
    <realisation>Il peut paraître surprenant que...</realisation>

    <patron>
        <element label="Il">Il </element>
        <element label="(nous/me)">(nous/me) </element>
        <element label="Vétat">être, sembler, paraître, apparaître... </element>
        <element label="ADJsurprise">frappant, surprenant, étonnant </element>
        <element label="(de)">(de) </element>
        <element label="(Vanalyse)">(constater, observer, voir...) </element>
        <element label="(que)">(que) </element>
    </patron>

    <exemple type="Forme de base" simplicity="">
    <![CDATA[<ROUT TYPE="SUR_1"><facultatif>Par ailleurs,</facultatif> il est frappant de constater <objet>de quelle manière l’influence des deux hommes s’est exprimée différemment</objet></ROUT>]]>
    </exemple>
    ...
    <exemple source="[geo-art-348]" type="Forme de base" simplicity="'++">
    <![CDATA[<ROUT TYPE="SUR_1"><information>Quel que soit le type d’impact attribué aux évolutions démographiques,</information> il est frappant de constater <objet>l’importance de ce facteur dans le débat allemand (Kabisch <hi rend="italic">et al.</hi>, 2006).</objet></ROUT>]]>
    </exemple>
    ...
</Routine>
```

## Sources des exemples

- Toutes les sources des exemples sont représentées dans un élément `<sourcesExemples>`
- Le lien se fait par la correspondance entre l'attribut `id=''` d'une `<source>`et l'attribut `source=''` d'un élément `<exemple>` ou d'un élément `<TextRepresentation>` (représentation d'un exemple d'après LMF pour les Entrées Lexicales et les Collocations).

```xml
    <source  id="{identifiant unique utilisé pour établir un lien vers les exemples}">
        {Citation bibliographique}
    </source>
```

### Exemple

```xml
<sourcesExemples>
    <source id="[ant-art-121]">
    Lassègue, J., Rosenthal, V., & Visetti, Y-M. (2009). Économie symbolique et phylogenèse du langage. L'Homme, 192, 67-100. Consulté le 20 octobre 2014 : www.cairn.info/revue-l-homme-2009-4-page-67.htm.
    </source>
    <source id="[ant-art-133]">
    Macdonald (2008). L'anthropologie sociale en France, dans quel état ?. Ethnologie française, Vol. 38, 617-625. Consulté le 20 octobre 2014 : www.cairn.info/revue-ethnologie-francaise-2008-4-page-617.htm.
    </source>
    ...
</sourcesExemples>
```


## Statistiques

### Statisiques Entrée Lexicale

```xml
<statistiques>
    <caFreq>{fréquence absolue (nombre d'occurrences) dans le corpus d'analyse}</caFreq>
    <caByM>{fréquence relative (par million de mot) dans le corpus d'analyse}</caByM>
    <ccByM>{fréquence relative (par million de mot) dans le corpus de contraste}</ccByM>
    <ngrammes>{nombre d'apparition du lemme dans un n-gramme validé manuellement comme expression polylexicale contigüe (ex: 'point' dans 'point de vue') afin de soustraire ce nombre d'occurrences à la fréquence totale.}</ngrammes>
    <specificite>{résultat du ratio de la fréquence relative (par million de mots) dans le corpus d'analyse (CA) sur le corpus de constraste (CC)}</specificite>
    <distribTranches>{nombre de tranches différentes du corpus d'analyse (divisé en 100 tranches) dans lesquelles le lemme apparaît}</distribTranches>
    <distribDisciplines>{nombre de disciplines différentes dans lesquelles le lemme apparaît}</distribDisciplines>
    <specifDisciplines>{nombre de disciplines pour lesquelles la fréquence relative du lemme est supérieure par rapport au corpus de contraste}</specifDisciplines>
    <!-- freqDisciplines : fréquence absolue du lemme dans le sous-corpus disciplinaire -->
    <freqDisciplines  anthropo="{fréquence absolue dans le sous-corpus d'anthropologie}" 
                    eco="{fréquence absolue dans le sous-corpus d'économie}" 
                    geo="{fréquence absolue dans le sous-corpus de géographie}" 
                    histoire="{fréquence absolue dans le sous-corpus d'histoire}" 
                    ling="{fréquence absolue dans le sous-corpus de linguistique}" 
                    psycho="{fréquence absolue dans le sous-corpus de psychologie}" 
                    sciedu="{fréquence absolue dans le sous-corpus de sciences de l'éducation}" 
                    sciepo="{fréquence absolue dans le sous-corpus de sciences politiques}" 
                    scinfo="{fréquence absolue dans le sous-corpus de sciences de l'information}" 
                    socio="{fréquence absolue dans le sous-corpus de sociologie}"/>
</statistiques>
```

#### Exemple Statisiques Entrée Lexicale

```xml
<statistiques> 
    <caFreq>433</caFreq>
    <caByM>89.567</caByM>
    <ccByM>30.722</ccByM>
    <ngrammes>0</ngrammes>
    <specificite>2.915</specificite>
    <distribTranches>94</distribTranches>
    <distribDisciplines>10</distribDisciplines>
    <specifDisciplines>6</specifDisciplines>
    <freqDisciplines  anthropo="53" eco="19" geo="43" histoire="38" ling="51" psycho="27" sciedu="33" sciepo="24" scinfo="104" socio="41"/>
</statistiques>
```

### Statisiques Collocation

```xml
<statistiques>
    <nbOccurrences>{fréquence absolue (nombre d'occurrences) dans le corpus d'analyse}</nbOccurrences>
    <frequence>{fréquence d'appariton de la collocation dans le corpus d'analyse (divisé en 100 tranches). Exprimée par les valeurs : MF, F, TF. }</frequence>
    <distribDisciplines>{nombre de disciplines différentes dans lesquelles la collocation apparaît}</distribDisciplines>
    <forceAssociation>{Log-likelihood : probabilité que les deux mots (base et collocatif) sont utilisés ensemble plus souvent que le hasard. }</forceAssociation>
</statistiques>
```

#### Exemple Statisiques Collocation

```xml
<statistiques>
    <nbOccurrences>7</nbOccurrences>
    <frequence>MF</frequence>
    <distribDisciplines>4</distribDisciplines>
    <forceAssociation>13</forceAssociation>
</statistiques>
```